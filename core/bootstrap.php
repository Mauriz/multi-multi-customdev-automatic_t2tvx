<?php
set_include_path(__DIR__);
spl_autoload_register();
date_default_timezone_set('Europe/Paris');

require 'constants.php';
require 'config.php';
