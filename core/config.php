<?php
$config = array(

	// Default api configuration
	'api' => array(
        'partner_users'	=> array('A' => 'bsima_valley_api'),
        'partner_key1' 	=> 'GKBkZGerDgFq-He41OYGkeCfZl6fAaikEwJ6S9xg0GhLhA_xs08QeVYOKTOjH5Itj8BOsXqTiVs-VPFt2Nzkb1zZqBg',
        'partner_key2' 	=> 'He41OYGkeCfZl6fAaikEwJ6S9xg0GhLhA_xs08QeVYOKTOjH5Itj8BOsXqTiVs-VPFt2Nzkb1zZqBg1',
	),

    // Members import statuses
	'importStatuses' => array(
		'success' 	=> array('DONE', 'DONE WITH ERROR(S)', 'DONE WITH ERRORS'),
		'error' 	=> array('ERROR', 'FAILURE'),
		'pending' 	=> array('STORAGE', 'VALIDATED', 'QUEUED', 'IMPORTING'),
	),
	
	'importStatusesSummary' => array(
		'finished'		=> array('complete', 'error', 'validated',),
		'processing'	=> array('processing',),
	),	

    // Default log folder
	'logFolder' => 'log',

    // Default nmp configuration
	'nmp' => array(
		'wsdl' 			=> 'http://api.notificationmessaging.com/NMSOAP/NotificationService?wsdl',
		'id' 			=> '1544713',
		'random'		=> '1ABD84DA2180801A',
		'encrypt'		=> 'EdX7CqkmmqPm8SA9MOPv1E3WWT4OFKm3izrcc6k-WMGoK7U',
		'recepients'	=> array('monitoring_valley@smartfocus.com', 'rahel.chowdhury@smartfocus.com'),
		'subjects'		=> array(
        	'NOTIFY'		=> 'Notification',
			'SFTP_ERROR'	=> 'SFTP error',
			'FTP_ERROR'		=> 'FTP error',
			'API_ERROR'		=> 'API error',
			'ERROR'			=> 'Error',
			'WARNING'		=> 'Warning',
			'NOTICE'		=> 'Notice',
			'SUCCESS'		=> 'Success',
			'ABORD'			=> 'Abord',
		),
	),

    // Default timezone
	'timeZone' => 'Europe/Paris',

    // Default upload folder
	'uploadFolder' => 'upload',

);
