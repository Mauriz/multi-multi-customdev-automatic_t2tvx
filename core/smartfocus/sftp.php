<?php namespace SmartFocus;

require_once CORE_PATH . '/libraries/phpseclib/Net/SSH2.php';
require_once CORE_PATH . '/libraries/phpseclib/Net/SFTP.php';
require_once CORE_PATH . '/libraries/phpseclib/Crypt/Hash.php';
require_once CORE_PATH . '/libraries/phpseclib/Crypt/RSA.php';
require_once CORE_PATH . '/libraries/phpseclib/Crypt/RC4.php';
require_once CORE_PATH . '/libraries/phpseclib/Math/BigInteger.php';

/**
 * Sftp class
 *
 * @dependencies: phpseclib
 */
class Sftp
{

    const DEFAULT_PORT 				= '22';
	const DEFAULT_TIMEOUT 			= 90; // seconds
	const DEFAULT_MAX_ATTEMPTS		= 3;
	const DEFAULT_OPERATION_DELAY	= 1; // seconds
	
	const ITEM_TYPE_FILE	= 1;
	const ITEM_TYPE_FOLDER	= 2;

	/**
	 * Authenticated
	 *
	 * @var bool
	 */
	protected $authenticated;

	/**
	 * Connection
	 *
	 * @var resource
	 */
	public $connection;

	/**
	 * Max attempts
	 *
	 * @var integer
	 */
	protected $maxAttempts;

	/**
	 * Operation delay
	 *
	 * @var integer
	 */
	protected $operationDelay;

	/**
	 * Port
	 *
	 * @var string
	 */
	protected $port;

	/**
	 * Rsa key
	 *
	 * @var Crypt_RSA
	 */
	protected $rsa;

	/**
	 * Server
	 *
	 * @var string
	 */
	protected $server;

	/**
	 * Username
	 *
	 * @var string
	 */
	protected $username;
	
	/**
	 * Root
	 *
	 * @var string
	 */
	protected $root;	

	/**
	 * Class constructor
	 *
	 * @param string $server
	 * @param string $username
	 * @param string $key
	 */
	public function __construct($server, $username, $key, $maxAttempts = NULL, $port = NULL, $operationDelay = NULL, $root = NULL)
	{

        $this->authenticated	= FALSE;
		$this->connection 		= NULL;
		$this->maxAttempts 		= is_null($maxAttempts) ? self::DEFAULT_MAX_ATTEMPTS : max(1, $maxAttempts);
		$this->operationDelay	= is_null($operationDelay) ? self::DEFAULT_OPERATION_DELAY : $operationDelay;

		$this->server			= $server;
        $this->port				= empty($port) ? self::DEFAULT_PORT : $port;
		$this->username 		= $username;
        $this->rsa 				= new \Crypt_RSA();

        $this->rsa->loadKey($key);
        
        $this->root 			= empty($root) ? '/' : $root;

	}

	/**
	 * Close ftp connection
	 *
	 * @param $reason [optional]
	 * @return bool
	 */
	public function close($reason = NULL)
	{

        \SmartFocus\Util::log('---->Disconnect from the sftp server: ' . $this->server . '.');

		if (empty($this->connection) || !$this->authenticated) {
            \SmartFocus\Util::log('<----Already disconnected from the sftp server: ' . $this->server . '.');
			return TRUE;
		}

        $this->connection->_disconnect($reason);

        $this->connection 		= NULL;
        $this->authenticated 	= FALSE;

        \SmartFocus\Util::log('<----Successfully disconnected from the sftp server: ' . $this->server . '.');

		return TRUE;

	}

	/**
	 * Connect method
	 *
	 * @param bool $reconnect [optional][default=FALSE]
	 * @return bool or throw \Exception
	 */
	public function connect($reconnect = FALSE)
	{

		if (!empty($this->connection) && $this->authenticated) {
            \SmartFocus\Util::log('<----Already authenticated with the sftp server ' . $this->server . '.');
			return TRUE;
		}

		// -----===== START Connect to the sftp server =====-----
        \SmartFocus\Util::log('---->' . ($reconnect ? 'Reconnect' : 'Connect') . ' to the sftp server ' . $this->server . '.');

        $connectionAttempts = 0;

		while (empty($this->connection) && $connectionAttempts < $this->maxAttempts) {

        	$connectionAttempts++;

			if ($connectionAttempts > 1) {
				\SmartFocus\Util::log('---->Retry to connect to the sftp server (attempt: ' . $connectionAttempts . ').');
	            sleep($this->operationDelay);
			}

            $this->connection = new \Net_SFTP($this->server, $this->port, self::DEFAULT_TIMEOUT);

			if (empty($this->connection)) {
	            \SmartFocus\Util::log('<----Failed to connect to the sftp server (attempt: ' . $connectionAttempts . ').');
			}

		}

		// Failed to connect to the sftp server. Exiting script with error.
		if (empty($this->connection)) {
	        throw new \Exception('Failed to connect to sftp server (attempts: ' . $connectionAttempts . ').', EXCEPTION_CODE_SFTP_ERROR);
		}

        \SmartFocus\Util::log('<----Successfully connected to the sftp server (attempts: ' . $connectionAttempts . ').');
        // -----===== END Connect to the sftp server =====-----

		// -----===== START Authenticare with the sftp server =====-----
        \SmartFocus\Util::log('---->Authenticate with the sftp server: ' . $this->server . '.');

        $authenticationAttempts	= 0;

		while (!$this->authenticated && $authenticationAttempts < $this->maxAttempts) {

	        $authenticationAttempts++;

			if ($authenticationAttempts > 1) {
				\SmartFocus\Util::log('---->Retry to authenticate to the sftp server (attempt: ' . $authenticationAttempts . ').');
				sleep($this->operationDelay);
			}

	        $this->authenticated = $this->connection->login($this->username, $this->rsa);

			if (!$this->authenticated) {
	            \SmartFocus\Util::log('<----Failed to authenticate to the sftp server (attempt: ' . $authenticationAttempts . ').');
			}

		}

        // Failed to authenticate with the sftp server. Exiting script with error.
		if (!$this->authenticated) {
			$exceptionMessage = 'Failed to authenticate to sftp server (attempts: ' . $authenticationAttempts . ').';
   			$this->connection->_disconnect($exceptionMessage);
            $this->connection = NULL;
	        throw new \Exception($exceptionMessage, EXCEPTION_CODE_SFTP_ERROR);
		}

        \SmartFocus\Util::log('<----Successfully authenticated to the sftp server (attempts: ' . $authenticationAttempts . ').');
        // -----===== END Authenticare with the sftp server =====-----

		return TRUE;

	}

	/**
	 * Download file method
	 *
	 * @param string $sourceFile
	 * @param string $destinationFile
	 * @return boolean or string or throw \Exception
	 */
    public function downloadFile($sourceFile, $destinationFile)
	{

		$fileContent		= NULL;
		$downloaded			= FALSE;
		$downloadAttempts	= 0;

		// Reconnect if necessary.
		if (empty($this->connection) || !$this->authenticated) {
			$this->connect(true);
		}

		\SmartFocus\Util::log('---->Download the file ' . $sourceFile . ' to the destination file ' . $destinationFile . '.');

		// Download
		while (!$downloaded && $downloadAttempts < $this->maxAttempts) {

	        $downloadAttempts++;

			if ($downloadAttempts > 1) {
				\SmartFocus\Util::log('---->Retrying to donwload the file ' . $sourceFile . ' (attempt: ' . $downloadAttempts . ').');
              	sleep($this->operationDelay);
			}

			if (!empty($destinationFile)) {
            	$downloaded = $this->connection->get($sourceFile, $destinationFile);
			}
			else {
				$downloaded = (bool) ($fileContent = $this->connection->get($sourceFile));
			}

			if (!$downloaded) {
	            \SmartFocus\Util::log('<----Failed to donwload the file ' . $sourceFile . ' (attempt: ' . $downloadAttempts . ').');
			}

		}

		if (!$downloaded) {
            throw new \Exception('Failed to donwload the file: ' . $sourceFile . ' (attempts: ' . $downloadAttempts . ').', EXCEPTION_CODE_WARNING);
		}

        \SmartFocus\Util::log('<----Successfully downloaded the file: ' . $destinationFile . '.');

		return empty($downloadFile) ? $fileContent : TRUE;

	}

	/**
	 * Rename file method
	 *
	 * @param string $sourceFile
	 * @param string $destinationFile
	 * @return boolean or throw \Exception
	 */
    public function renameFile($sourceFile, $destinationFile)
	{

		$renamed		= FALSE;
		$renameAttempts	= 0;

		// Reconnect if necessary.
		if (empty($this->connection) || !$this->authenticated) {
			$this->connect(true);
		}

		\SmartFocus\Util::log('---->Rename the file  ' . $sourceFile . ' to ' . $destinationFile . '.');

		while (!$renamed && $renameAttempts < $this->maxAttempts) {

	        $renameAttempts++;

			if ($renameAttempts > 1) {
				\SmartFocus\Util::log('---->Retrying to rename the file ' . $sourceFile . ' to ' . $destinationFile . ' (attempt: ' . $renameAttempts . ').');
				sleep($this->operationDelay);
			}
	        
	        $renamed = $this->connection->rename($sourceFile, $destinationFile);	        

			if (!$renamed) {
	            \SmartFocus\Util::log('<----Failed to rename the file ' . $sourceFile . ' to ' . $destinationFile . ' (attempt: ' . $renameAttempts . ').');
			}

		}

		if (!$renamed) {
            throw new \Exception('Failed to rename the file: ' . $sourceFile . ' to ' . $destinationFile . ' (attempts: ' . $renameAttempts . ').', EXCEPTION_CODE_FTP_ERROR);
		}

        \SmartFocus\Util::log('<----Successfully renamed the file  ' . $sourceFile . ' to ' . $destinationFile . '.');

		return TRUE;

	}	
	
	/**
	 * Searches files
	 *
	 * @param string $fileNamePattern [optional]
	 * @param string $folder [optional] 	 
	 * @return array or throw \Exception
	 */	
	public function searchFiles($fileNamePattern = NULL, $folder = NULL)
	{
	
		// Reconnect if necessary.
		if (empty($this->connection) || !$this->authenticated) {
			$this->connect(TRUE);
		}
		
		if (empty($folder)) {
			\SmartFocus\Util::log('---->Scan the ftp server for files within the root folder ' . $this->root . '.');
			$folder = $this->root;			
		}
		else {

			\SmartFocus\Util::log('---->Scan the ftp server for files within the folder ' . $folder . '.');
			 			
			// Set working folder.
			if (!@$this->connection->chdir($folder)) {			
	            $this->throwException('Failed to set the working folder ' . $folder . '.', EXCEPTION_CODE_FTP_ERROR, TRUE);
			}
			
			$folder = rtrim($this->root, '/') . '/' . $folder;
			
		}
		
		// Scan for items.
		$itemList = (array) $this->connection->rawlist($folder);
		if (empty($itemList)) {
            //$this->throwException('Empty folder ' . $folder . '.', EXCEPTION_CODE_WARNING);
		}
		
		$rawList = array();
		
		// Scan for daily file.
		foreach ($itemList as $itemName => $item) {
			
			if (
				$item['type'] == self::ITEM_TYPE_FILE
				&&
				(
					empty($fileNamePattern)
					||
					strpos(strtolower($itemName), strtolower($fileNamePattern)) !== FALSE
				)
			) {
				$rawList[] = array('name' => $itemName, 'folder' => $folder, 'path' => $folder . '/' . $itemName,);
			}
		
		}
		
		if (empty($rawList)) {
			if (empty($fileNamePattern)) {
				$this->throwException('Could not find any file.', EXCEPTION_CODE_ABORD);
			}			
			$this->throwException('Could not find any file like ' . $fileNamePattern . '.', EXCEPTION_CODE_ABORD);							
		}
		
		return $rawList;
	
	}
	
	/**
	 * Throws an exception
	 *
	 * @param string $message
	 * @param int $code [optional]
	 * @param bool $disconnect [optional][default=TRUE]
	 * @return throw \Exception
	 */
	public function throwException($message, $code = NULL, $disconnect = TRUE) {

		\SmartFocus\Util::log('<----' . $message);
		if ($disconnect) {
        	$this->close();
		}
		throw new \Exception($message, $code);

	}		

	/**
	 * Upload file method
	 *
	 * @param string $sourceFileContent
	 * @param string $destinationFile
	 * @return boolean or throw \Exception
	 */
    public function uploadFile($sourceFileContent, $destinationFile)
	{

		$uploaded		= FALSE;
		$uploadAttempts	= 0;

		// Reconnect if necessary.
		if (empty($this->connection) || !$this->authenticated) {
			$this->connect(true);
		}

		\SmartFocus\Util::log('---->Upload file contents to the destination file ' . $destinationFile . '.');

		while (!$uploaded && $uploadAttempts < $this->maxAttempts) {

	        $uploadAttempts++;

			if ($uploadAttempts > 1) {
				\SmartFocus\Util::log('---->Retrying to upload the file contents (attempt: ' . $uploadAttempts . ').');
              	sleep($this->operationDelay);
			}

			$uploaded = $this->connection->put($destinationFile, $sourceFileContent);

			if (!$uploaded) {
	            \SmartFocus\Util::log('<----Failed to upload the file contents (attempt: ' . $uploadAttempts . ').');
			}

		}

		if (!$uploaded) {
            throw new \Exception('Failed to upload the file: contents (attempts: ' . $uploadAttempts . ').', EXCEPTION_CODE_FTP_ERROR);
		}

        \SmartFocus\Util::log('<----Successfully uploaded the file  ' . $destinationFile . '.');

		return TRUE;

	}

}
?>