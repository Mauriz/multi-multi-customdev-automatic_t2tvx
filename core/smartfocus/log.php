<?php namespace SmartFocus;

class Log
{

 	const DEFAULT_TIMEZONE = 'UTC';
 	const DEFAULT_LOG_NAME = 'log';

 	const FREQUENCY_ONCE		= 1;
 	const FREQUENCY_MINUTE 		= 2;
 	const FREQUENCY_HOUR 		= 3;
 	const FREQUENCY_DAY 		= 4;
 	const FREQUENCY_WEEK		= 5;
 	const FREQUENCY_MONTH		= 6;

	const ERROR_FOLDER_CREATE	= 'Could not create folder %s.';

    protected $frequency;
    protected $frequencyRate;

    protected $buffer 	= array();
	protected $history 	= array();
	protected $rootFolder;
	protected $timeZome;

	/**
	 * Class constructor method
	 *
	 * @param string $rootFolder
	 * @param string $timeZone [optional]
	 */
	public function __construct($rootFolder, $timeZone = NULL, $frequency = NULL, $frequencyRate = NULL)
	{
        $this->frequency		= is_null($frequency) ? self::FREQUENCY_DAY : $frequency;
        $this->frequencyRate	= is_null($frequencyRate) ? 1 : $frequencyRate;
        $this->rootFolder		= $rootFolder;
		$this->timeZone 		= empty($timeZone) ? self::DEFAULT_TIMEZONE : $timeZone;
	}

	/**
	 * Set root folder method
	 *
	 * @parama string $bufferLine
	 * @return bool
	 */
	public function setRootFolder($rootFolder)
	{
		$this->rootFolder = $rootFolder;
		return true;
	}

	/**
	 * Get buffer method
	 *
	 * @return array
	 */
	public function getBuffer()
	{
		return $this->buffer;
	}

	/**
	 * Reset buffer method
	 *
	 * @return bool
	 */
	public function resetBuffer()
	{
        $this->buffer = array();
		return true;
	}

	/**
	 * Set buffer method
	 *
	 * @parama string $bufferLine
	 * @return bool
	 */
	public function setBuffer($bufferLine)
	{
		$this->buffer[] = $bufferLine;
		return true;
	}

	/**
	 * Get history method
	 *
	 * @return array
	 */
	public function getHistory()
	{
        return $this->history;
	}

	/**
	 * Reset history method
	 *
	 * @return bool
	 */
	public function resetHistory()
	{
        $this->history = array();
		return true;
	}

	/**
	 * Set history method
	 *
	 * @param string $historyLine
	 * @return bool
	 */
	public function setHistory($historyLine)
	{
		$this->history[] = $historyLine;
		return true;
	}

	/**
	 * Wrap message method
	 *
	 * @param string $message
	 * @param bool $newLine [optional][default=false]
	 * @return string
	 */
	public function wrapMessage($message, $newLine = false)
	{
        $dateTime = new \DateTime('now', new \DateTimeZone($this->timeZone));

        $message = '[' . $dateTime->format('d-m-Y H:i:s') . ']' . trim($message);

        return $newLine ? (PHP_EOL . $message) : $message;
	}

	/**
	 * Get folder path (tries to create the folder if it does not exist)
	 *
	 * @param int $frequency
	 * @return string
	 */
	public function getFolderPath($frequency)
	{
		$folderPath = $this->rootFolder;

		if (in_array($frequency, array(self::FREQUENCY_HOUR, self::FREQUENCY_MINUTE))) {

			$dateTime 	= new \DateTime('now', new \DateTimeZone($this->timeZone));

			$folderPath .= '/' . $dateTime->format('Ymd');

			if (!is_dir($folderPath) && !mkdir($folderPath)) {
				trigger_error(E_USER_WARNING, sprintf(self::ERROR_FOLDER_CREATE, $folderPath));
			}

		}

		return $folderPath;
	}

	/**
	 * Get file path method
	 *
	 * @param string $folderPath
	 * @param int $frequency
	 * @param int $frequencyRate [optional][default=1]
	 * @param string $namePrefix [optional]
	 */
	public function getFilePath($folderPath, $frequency, $frequencyRate = 1, $namePrefix = NULL)
	{

		$dateTime 	= new \DateTime('now', new \DateTimeZone($this->timeZone));
  		$nameSufix	= NULL;

		if ($frequency == self::FREQUENCY_ONCE && empty($namePrefix)) {
            $namePrefix = self::DEFAULT_LOG_NAME;
		}
		elseif (!empty($namePrefix)) {
            $namePrefix .= '_';
		}

		switch ($frequency) {

			case self::FREQUENCY_MONTH:
                $nameSufix = $dateTime->format('Ym');
				break;
			case self::FREQUENCY_WEEK:
								$nameSufix = $dateTime->format('Ym')
									.date('d', strtotime('this week last monday', $dateTime->getTimestamp()))
									.date('d', strtotime('this week next sunday', $dateTime->getTimestamp()));
				break;				
			case self::FREQUENCY_DAY:
                $nameSufix = $dateTime->format('Ymd');
				break;
			case self::FREQUENCY_HOUR:
                $frequencyRate 	= max(min($frequencyRate, 24), 1);
                $nameSufix 		= $dateTime->format('Ymd') . str_pad(strval((int) floor((int) $dateTime->format('G') / $frequencyRate) * $frequencyRate), 2, '0', STR_PAD_LEFT) . '00';
				break;
			case self::FREQUENCY_MINUTE:
                $frequencyRate 	= max(min($frequencyRate, 60), 1);
                $nameSufix 		= $dateTime->format('YmdH') . str_pad(strval((int) floor((int) $dateTime->format('i') / $frequencyRate) * $frequencyRate), 2, '0', STR_PAD_LEFT);
				break;

			case self::FREQUENCY_ONCE:
   			default:
				break;

		}

		return $folderPath . '/' . $namePrefix . $nameSufix . '.log';

	}

	/**
	 * Write method
	 *
	 * @param string $message
	 * @param int $frequency [optional]
  	 * @param int $frequencyRate [optional]
	 * @param string $fileNamePrefix [optional]
	 * @param bool $wrapMessage [optional][default=true]
	 * @param bool $resetBuffer [optional][default=false]
	 */
	public function write($message, $frequency = NULL, $frequencyRate = NULL, $fileNamePrefix = NULL, $wrapMessage = true, $resetBuffer = false)
	{

		if (is_null($frequency)) {
            $frequency = $this->frequency;
		}
		if (is_null($frequencyRate)) {
            $frequencyRate = $this->frequencyRate;
		}

		if ($resetBuffer) {
			$this->resetBuffer();
		}

        $folderPath = $this->getFolderPath($frequency);
		if (!is_dir($folderPath) || !is_writable($folderPath)) {
			return FALSE;
		}

		$filePath 	= $this->getFilePath($folderPath, $frequency, $frequencyRate, $fileNamePrefix);

        $fileLine = $wrapMessage ? $this->wrapMessage($message) : $message;

        $this->setBuffer($fileLine);
        $this->setHistory($fileLine);

		if (file_exists($filePath) && filesize($filePath) > 0 && $frequency != self::FREQUENCY_ONCE) {
            $fileLine = PHP_EOL . $fileLine;
		}

		$fileWriteType = $frequency == self::FREQUENCY_ONCE ? NULL : FILE_APPEND;

		return file_put_contents($filePath, $fileLine, $fileWriteType);

	}

}
?>