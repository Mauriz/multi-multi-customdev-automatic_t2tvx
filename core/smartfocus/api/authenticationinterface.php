<?php

namespace SmartFocus\API;

/**
 * AuthenticationInterface
 *
 * Interface for all authentication variants
 *
 * @author Christian Tandler <ctandler@emailvision.com>
 *
 */

interface AuthenticationInterface
{

	public function getAuthenticationInfo($api_name);

}
