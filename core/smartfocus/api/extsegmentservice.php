<?php

namespace SmartFocus\API;

use \SoapClient;
use \SoapFault;
use \SoapVar;

/**
 * ExtSegmentService class
 *
 * Wrapper class for low level interaction with the SOAP version of the
 * SmartFocus External Segment API.
 *
 * @version 10.12 (2013-10-18)
 * @author Christian Tandler <ctandler@emailvision.com>
 *
 */

class ExtSegmentService extends BaseService
{

	/**
	 * Name of the API
	 * @var string
	 */
	protected $api_name = 'API_ADVANCED_INTEGRATION';

	/**
	 * URL to the WSDL
	 * @var string
	 */
	protected $wsdl = 'http://{server}/apiadvancedintegration/services/ExtSegmentService?wsdl';

	/**
	 * --------------------------------------------
	 * !!!           HELPER FUNCTIONS           !!!
	 * --------------------------------------------
	 *
	 * These functions are for simpler internal handling.
	 */

	/**
	 * ---------------------------------------------
	 * !!! API METHOD IMPLEMENTATION STARTS HERE !!!
	 * ---------------------------------------------
	 *
	 * If one is missing, please implement it below.
	 */

	/**
	 * Creates external segment
	 * @param  array   $mapping          File mapping
	 * @param  string  $file             File content
	 * @param  string  $name             Segment name
	 * @param  string  $partnerName      Partner name
	 * @param  string  $endOfValidity    End date
	 * @param  string  $desc             Description
	 * @param  string  $urlCallBack      Callback URL if ok
	 * @param  string  $urlCallBackError Callback URL if error
	 * @return integer                   Segment id
	 */
	public function extSegmentationCreate(
		$mapping,
		$file,
		$name,
		$partnerName,
		$endOfValidity = null,
		$desc = null,
		$urlCallBack = null,
		$urlCallBackError = null
	)
 	{
		$parameters['token'] = $this->token;
		$parameters['file'] = $file;
		$parameters['mapping'] = $mapping;

		//this value is required but actually doesn't mean anything
		$parameters['apiExtSegment']['id'] = 1;

		if ($endOfValidity) {
			$parameters['expire'] = true;
			$parameters['apiExtSegment']['endOfValidity'] = $endOfValidity;
		} else {
			$parameters['expire'] = false;
		}

		$parameters['apiExtSegment']['name'] = $name;
		$parameters['apiExtSegment']['partnerName'] = $partnerName;

		if ($desc) {
			$parameters['apiExtSegment']['desc'] = $desc;
		}
		if ($urlCallBack) {
			$parameters['urlCallBack'] = $urlCallBack;
		}
		if ($urlCallBackError) {
			$parameters['urlCallBackError'] = $urlCallBackError;
		}

		return $this->call('extSegmentationCreate', $parameters)->return;
	}

    /**
	 * Adds members to an existing external segment
	 * @param  string  $id	             External segment ID
	 * @param  array   $mapping          File mapping
	 * @param  string  $file             File content
	 * @param  string  $urlCallBack      Callback URL if ok
	 * @param  string  $urlCallBackError Callback URL if error
	 * @return boolean                   True/False
	 */
	public function extSegmentationAddLines($id, $mapping, $file, $urlCallBack = null, $urlCallBackError = null) {
        $parameters['token'] = $this->token;
		$parameters['file'] = $file;
		$parameters['mapping'] = $mapping;
        $parameters['id'] = $id;

		if ($urlCallBack) {
			$parameters['urlCallBack'] = $urlCallBack;
		}
		if ($urlCallBackError) {
			$parameters['urlCallBackError'] = $urlCallBackError;
		}

		return $this->call('extSegmentationAddLines', $parameters)->return;
	}

	/**
	 * Removes members from an existing external segment
	 * @param  string  $id	             External segment ID
	 * @param  array   $mapping          File mapping
	 * @param  string  $file             File content
	 * @param  string  $urlCallBack      Callback URL if ok
	 * @param  string  $urlCallBackError Callback URL if error
	 * @return boolean                   True/False
	 */
	public function extSegmentationRemoveLines($id, $mapping, $file, $urlCallBack = null, $urlCallBackError = null) {
        $parameters['token'] = $this->token;
		$parameters['file'] = $file;
		$parameters['mapping'] = $mapping;
        $parameters['id'] = $id;

		if ($urlCallBack) {
			$parameters['urlCallBack'] = $urlCallBack;
		}
		if ($urlCallBackError) {
			$parameters['urlCallBackError'] = $urlCallBackError;
		}

		return $this->call('extSegmentationRemoveLines', $parameters)->return;
	}

	/**
	 * Replaces the member list in an external segment with new members provided in a file
	 * @param  string  $id	             External segment ID
	 * @param  array   $mapping          File mapping
	 * @param  string  $file             File content
	 * @param  string  $urlCallBack      Callback URL if ok
	 * @param  string  $urlCallBackError Callback URL if error
	 * @return boolean                   True/False
	 */
	public function extSegmentationReloadSegment($id, $mapping, $file, $urlCallBack = null, $urlCallBackError = null) {
        $parameters['token'] = $this->token;
		$parameters['file'] = $file;
		$parameters['mapping'] = $mapping;
        $parameters['id'] = $id;

		if ($urlCallBack) {
			$parameters['urlCallBack'] = $urlCallBack;
		}
		if ($urlCallBackError) {
			$parameters['urlCallBackError'] = $urlCallBackError;
		}

		return $this->call('extSegmentationReloadSegment', $parameters)->return;
	}

    /**
	 * Updates an external segment without changing the list of members
	 * @param  string  $id	             External segment ID
	 * @param  string  $endOfValidity    End date
	 * @param  string  $name             Segment name
	 * @param  string  $partnerName      Partner name
	 * @param  string  $desc             Description
	 * @return boolean                   True/False
	 */
	public function  extSegmentationUpdate($id, $endOfValidity, $name, $partnerName, $desc = null) {
		$parameters['token'] = $this->token;
  		$parameters['apiExtSegment']['id'] = $id;
        $parameters['apiExtSegment']['name'] = $name;
        $parameters['apiExtSegment']['partnerName'] = $partnerName;

		if ($endOfValidity) {
			$parameters['expire'] = true;
			$parameters['apiExtSegment']['endOfValidity'] = $endOfValidity;
		}
		else {
			$parameters['expire'] = false;
		}
		if ($desc) {
			$parameters['apiExtSegment']['desc'] = $desc;
		}

		return $this->call('extSegmentationUpdate', $parameters)->return;
	}

    /**
	 * (What it should do) - Deletes an external segment
	 * (What it really does) - Hides an external from the interface.  Still retrievable with APIs
	 * @param  string  $id	             External segment ID
	 * @return boolean                   True/False
	 */
	public function extSegmentationDelete($id) {
    	$parameters['token'] = $this->token;
		$parameters['id'] = $id;
        return $this->call('extSegmentationDelete', $parameters)->return;
	}

    /**
	 * retrieves the attributes of an external segment excluding the list of members
	 * @param  string  $id	             External segment ID
	 * @return object
	 */
	public function extSegmentationGet($id) {
        $parameters['token'] = $this->token;
		$parameters['id'] = $id;
        return $this->call('extSegmentationGet', $parameters)->return;
	}

    /**
	 * Get paginated list of segments.
	 * @return object
	 */
	public function extSegmentationGetSegments($page = 1, $items = 30, $source = null, $type = null)
	{
		$options = array(
			'token' => $this->token,
			'page' => $page,
			'noItemsPerPage' => $items
		);
		if ($source) {
			$options['source'] = $source;
		}
		if ($type) {
			$options['type'] = $type;
		}
		return $this->call('extSegmentationGetSegments', $options)->return;
	}

    /**
	 * Retrieves the lines that generated errors when an external segment was loaded (created or reloaded)
	 * @param  string  $id	             External segment ID
	 * @return object
	 */
	public function extSegmentationGetBadLines($id) {
        $parameters['token'] = $this->token;
		$parameters['id'] = $id;
        return $this->call('extSegmentationGetBadLines', $parameters)->return;
	}

	/**
     * Cannot find documentation on this function

	 */
	public function createCallBackUrl() {}
}
