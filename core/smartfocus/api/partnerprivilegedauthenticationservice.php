<?php

namespace SmartFocus\API;

use \SoapClient;
use \SoapFault;

/**
 * PartnerPrivilegedAuthenticationService class
 *
 * Authentication provider for the SmartFocus
 * internal partner privileged authentification
 *
 * @author Christian Tandler <ctandler@emailvision.com>
 *
 */

class PartnerPrivilegedAuthenticationService implements AuthenticationInterface
{

	/**
	 * WSDL for SoapClient
	 * @var string
	 */
	protected $wsdl = 'http://internal-apipartner.emv2.com/apiconnection/services/ConnectionService?wsdl';

	/**
	 * SmartFocus User
	 * @var string
	 */
	protected $user = null;

	/**
	 * Partner API key1
	 * @var string
	 */
	protected $key1 = null;

	/**
	 * Partner API key2
	 * @var string
	 */
	protected $key2 = null;

	/**
	 * SoapClient instance
	 * @var SoapClient
	 */
	protected $client = null;

	/**
	 * Constructor
	 * @param  string $user SmartFocus Username
	 * @param  string $key1 Partner API key1
	 * @param  string $key2 Partner API key2
	 */
	public function __construct($user, $key1, $key2)
	{
		$this->user = $user;
		$this->key1 = $key1;
		$this->key2 = $key2;

		$this->client = new SoapClient($this->wsdl, array('trace' => 1));
	}

	/**
	 * Open partner privileged connection
	 * @param  string $api_name API name
	 * @return object           Connection info (manager_type, platform, token)
	 */
	public function openPartnerPrivileged($api_name)
	{
		try {
			$response = $this->client->openPartnerPrivileged(array(
				'apiName' => $api_name,
				'login'   => $this->user,
				'key1'    => $this->key1,
				'key2'    => $this->key2
			));
		} catch (SoapFault $e) {
			error_log($this->client->__getLastRequest());
			error_log($this->client->__getLastResponse());
			throw $e;
		}

		return $response->return;
	}

	/**
	 * Open connection from interface
	 * @param  string $api_name API name
	 * @return object           Connection info (manager_type, platform, token)
	 */
	public function getAuthenticationInfo($api_name)
	{
		return $this->openPartnerPrivileged($api_name);
	}

}
