<?php

namespace SmartFocus\API;

use \SoapClient;
use \SoapFault;
use \SoapVar;

/**
 * CampaignManagementService class
 *
 * Wrapper class for low level interaction with the SOAP version of the
 * SmartFocus Campaign Management API.
 *
 * @version 10.16 (2014-06-25)
 * @author Christian Tandler <ctandler@emailvision.com>, Bogdan Sima <bogdan.sima@smartfocus.com>
 *
 */
class CampaignManagementService extends BaseService
{

	const OPERATOR_DEFAULT = "IS_EMPTY";

	/**
	 * Name of the API
	 * @var string
	 */
	protected $api_name = 'API_CCMD';

	/**
	 * URL to the WSDL
	 * @var string
	 */
	protected $wsdl = 'http://{server}/apiccmd/services/CcmdService?wsdl';

	/**
	 * Generic accepted operators
	 * @var array
	 */
	private $operators = array('EQUALS', 'CONTAINS', 'DOES_NOT_CONTAINS', 'BEGINS_WITH', 'ENDS_WITH', 'IS_EMPTY', 'IS_NOT_EMPTY');

	/**
	 * Case sensitive accepted operators
	 * @var array
	 */
    private $case_sensitive_operators = array('EQUALS_CASE_SENSITIVE', 'CONTAINS_CASE_SENSITIVE', 'DOES_NOT_CONTAIN_CASE_SENSITIVES', 'BEGINS_WITH_CASE_SENSITIVE', 'ENDS_WITH_CASE_SENSITIVE');

	/**
	 * ---------------------------------------------
	 * !!! API METHOD IMPLEMENTATION STARTS HERE !!!
	 * ---------------------------------------------
	 *
	 * If one is missing, please implement it below.
	 */

	/**
	 * Get default sender
	 * @return string Default sender
	 */
	public function getDefaultSender()
	{
		return $this->call('getDefaultSender', array(
			'token' => $this->token
		))->return;
	}

	/**
	 * Clones an email message
	 * @param  integer	$id 		Message id
	 * @param  string   $newName	New message name
	 * @return integer				New message id
	 */
	public function cloneMessage($id, $newName)
	{
		return $this->call('cloneMessage', array(
			'token'   	=> $this->token,
			'id' 		=> $id,
			'newName' 	=> $newName,
		))->return;
	}

	/**
	 * Creates an email message
	 * @param  array   $message Message array
	 * @return integer          Message id
	 */
	public function createEmailMessagebyObj(array $message)
	{
		$defaults = array(
			'id'              => 0,
			'isBounceback'    => false,
			'encoding'        => 'UTF-8',
			'hotmailUnsubFlg' => false
		);

		$message = $message + $defaults;

		$message['name'] = mb_substr($message['name'], 0, 50);
		$message['subject'] = mb_substr($message['subject'], 0, 2000);
		if (!($message['body'] instanceof SoapVar)) $message['body'] = new SoapVar("<body><![CDATA[{$message['body']}]]></body>", XSD_ANYXML);

		return $this->call('createEmailMessagebyObj', array(
			'token'   => $this->token,
			'message' => $message
		))->return;
	}

	/**
	 * Creates an sms message
	 * @param  string  $name Message name
	 * @param  string  $from Sender name
	 * @param  string  $body Message content
	 * @param  string  $desc Message description
	 * @return integer       Message id
	 */
	public function createSmsMessage($name, $from, $body, $desc)
	{
		$name = mb_substr($name, 0, 50);
		$body = "[EMV SMSPART]{$body}";

		return $this->call('createSmsMessage', array(
			'token' => $this->token,
			'name'  => $name,
			'desc'  => $desc,
			'from'  => $from,
			'body'  => $body
		))->return;
	}

	/**
	 * Get message
	 * @param  integer $message_id 	Message id
	 * @return stdClass				Message
	 */
	public function getMessage($message_id)
	{
		$result = $this->call('getMessage', array(
			'token'	=> $this->token,
			'id'	=> $message_id
		));
		return empty($result->return) ? NULL : $result->return;
	}

	/**
	 * Gets a list of id's of email messages that match the given value in the specified field
	 * @param  string	$field	Field name
	 * @param  string	$value	Field value
	 * @param  integer	$limit	Maximum number of email messages to retrieve
	 * @return array			List of email messages id's
	 */
	public function getEmailMessagesByField($field, $value = '', $limit = 1)
	{
		$result = $this->call('getEmailMessagesByField', array(
			'token'	=> $this->token,
			'field'	=> $field,
			'value'	=> $value,
			'limit'	=> $limit
		));

		return empty($result->return) ? array() : $result->return;

	}

	/**
	 * Delete message
	 * @param  integer $message_id Message id
	 * @return boolean             Success
	 */
	public function deleteMessage($message_id)
	{
		return $this->call('deleteMessage', array(
			'token' => $this->token,
			'id'    => $message_id
		))->return;
	}

	/**
	 * Tracks all links
	 * @param  integer $message_id Message id
	 * @return integer             Last link id
	 */
	public function trackAllLinks($message_id = 0)
	{
		return $this->call('trackAllLinks', array(
			'token' => $this->token,
			'id'    => $message_id
		))->return;
	}

	/**
	 * Creates mirror link
	 * @param  integer $message_id Message id
	 * @param  string  $name       Name of the link
	 * @return integer             Link id
	 */
	public function createMirrorUrl($message_id = 0, $name = 'Mirror')
	{
		return $this->call('createMirrorUrl', array(
			'token'     => $this->token,
			'messageId' => $message_id,
			'name'      => $name
		))->return;
	}

	/**
	 * Creates and adds mirror link, replacing the first occurrence of "&&&" in the message
	 * @param  integer $message_id Message id
	 * @param  string  $name       Name of the link
	 * @return integer             Link id
	 */
	public function createAndAddMirrorUrl($message_id = 0, $name = 'Mirror')
	{
		return $this->call('createAndAddMirrorUrl', array(
			'token'     => $this->token,
			'messageId' => $message_id,
			'name'      => $name
		))->return;
	}

	/**
	 * Creates unjoin link
	 * @param  integer $message_id    Message id
	 * @param  string  $name          Name of the link
	 * @param  string  $page_ok       URL to the OK page
	 * @param  string  $message_ok    Id of the OK bounceback message
	 * @param  string  $page_error    URL to the ERROR page
	 * @param  string  $message_error Id of the ERROR bounceback message
	 * @return integer                Link id
	 */
	public function createUnsubscribeUrl(
		$message_id    = 0,
		$name          = 'CC Unjoin',
		$page_ok       = NULL,
		$message_ok    = '',
		$page_error    = NULL,
		$message_error = ''
	)
	{
		return $this->call('createUnsubscribeUrl', array(
			'token'        => $this->token,
			'messageId'    => $message_id,
			'name'         => $name,
			'pageOK'       => $page_ok,
			'messageOK'    => $message_ok,
			'pageError'    => $page_error,
			'messageError' => $message_error
		))->return;
	}

	/**
	 * Creates Link with personalised URL
	 * @param  integer $message_id Message id
	 * @param  string  $name       Name of the link
	 * @param  string  $url        URL
	 * @return integer             Link order id
	 */
	public function createPersonalisedUrl($message_id = 0, $name = '', $url = '')
	{
		return $this->call('createPersonalisedUrl', array(
			'token'     => $this->token,
			'messageId' => $message_id,
			'name'      => $name,
			'url'       => $url
		))->return;
	}

	/**
	 * Creates a campaign
	 * @param  array   $campaign Campaign array
	 * @return integer           Campaign id
	 */
	public function createCampaignByObj(array $campaign)
	{
		$defaults = array(
			'id'                => 0,
			'analytics'         => false,
			'deliverySpeed'     => 0,
			'emaildedupflg'     => true,
			'notification'      => true,
			'postClickTracking' => true
		);
		$campaign = $campaign + $defaults;

		$campaign['name'] = mb_substr($campaign['name'], 0, 128);

		return $this->call('createCampaignByObj', array(
			'token'    => $this->token,
			'campaign' => $campaign
		))->return;
	}

	/**
	 * Posts a campaign
	 * @param  integer $id Campaign id
	 * @return bolean      Success
	 */
	public function postCampaign($id = 0)
	{
		return $this->call('postCampaign', array(
			'token' => $this->token,
			'id'    => $id
		))->return;
	}

	/**
	 * Gets a campaign
	 * @param  integer $id Campaign id
	 * @return bolean      Success
	 */
	public function getCampaign($id)
	{
		return $this->call('getCampaign', array(
			'token' => $this->token,
			'id'    => $id
		))->return;
	}

	/**
	 * Gets a list of id's of campaigns that match the given value in the specified field
	 * @param  string	$field	Field name
	 * @param  string	$value	Field value
	 * @param  integer	$limit	Maximum number of campaigns to retrieve
	 * @return array			List of campaign id's
	 */
	public function getCampaignsByField($field, $value = '', $limit = 1)
	{
		return $this->call('getCampaignsByField', array(
			'token'	=> $this->token,
			'field'	=> $field,
			'value'	=> $value,
			'limit'	=> $limit
		))->return;
	}

	/**
	 * Gets a list of id's of campaigns with a specified status
	 * @param  string	$status	Status to match
	 * @return array			List of campaign id's
	 */
	public function getCampaignsByStatus($status = '')
	{
		return $this->call('getCampaignsByStatus', array(
			'token'	=> $this->token,
			'status'=> mb_strtoupper($status)
		))->return;
	}

	/**
	 * Gets a list of id's of campaigns from a specified period
	 * @param  string	$dateBegin	The start date of the period to retrieve (yyyy-MM-dd HH:mm:ss)
	 * @param  string	$dateEnd	The end date of the period to retrieve (yyyy-MM-dd HH:mm:ss)
	 * @return array				List of campaign id's
	 */
	public function getCampaignsByPeriod($dateBegin = '', $dateEnd = '')
	{
		return $this->call('getCampaignsByPeriod', array(
			'token'		=> $this->token,
			'dateBegin'	=> $dateBegin,
			'dateEnd'	=> $dateEnd
		))->return;
	}

	/**
	 * Retrieves a campaign status
	 * @param  integer	$id	Campaign id
	 * @return string		Status of the campaign
	 */
	public function getCampaignStatus($id = 0)
	{
		return $this->call('getCampaignStatus', array(
			'token'	=> $this->token,
			'id'	=> $id
		))->return;
	}

	/**
	 * Retrieves the most recent campaigns
	 * @param  integer	$limit	Maximum number of campaigns to retrieve
	 * @return array			List of most recent campaign id's
	 */
	public function getLastCampaigns($limit = 0)
	{
		return $this->call('getLastCampaigns', array(
			'token'	=> $this->token,
			'limit'	=> $limit
		))->return;
	}

	/**
	 * Retrieves the most recent email messages created
	 * @param  integer	$limit	Maximum number of email messages to retrieve
	 * @return array			List of most recent email messages id's
	 */
	public function getLastEmailMessages($limit = 0)
	{
		return $this->call('getLastEmailMessages', array(
			'token'	=> $this->token,
			'limit'	=> $limit
		))->return;
	}

	/**
	 * Gets a list of id's of email messages from a specified period
	 * @param  string	$dateBegin	The start date of the period to retrieve (yyyy-MM-dd HH:mm:ss)
	 * @param  string	$dateEnd	The end date of the period to retrieve (yyyy-MM-dd HH:mm:ss)
	 * @return array				List of email messages id's
	 */
	public function getMessagesByPeriod($dateBegin = '', $dateEnd = '')
	{
		return $this->call('getMessagesByPeriod', array(
			'token'		=> $this->token,
			'dateBegin'	=> $dateBegin,
			'dateEnd'	=> $dateEnd
		))->return;
	}

	/**
	 * Gets a list of id's of email messages and their details
	 * @param  array	$listOptionsParams		Pagination options
	 * @param  array	$searchParams			Search options
	 * @param  array	$sortOptionsParams		Sorting options
	 * @return object							Object messageSummaryList containing Message objects
	 */
	public function getMessageSummaryList(array $paginationParams, array $searchParams = NULL, array $sortOptionsParams = NULL)
	{
		$listOptionsEntity	= array(
			'page'				=> $paginationParams['page'],
			'pageSize'			=> min($paginationParams['pageSize'], 1000),
			'search'			=> empty($searchParams) ? NULL : array(
				'messageId'			=> empty($searchParams['messageId']) ? NULL : $searchParams['messageId'],
				'name'				=> empty($searchParams['name']) ? NULL : $searchParams['name'],
				'minCreatedDate'	=> empty($searchParams['minCreatedDate']) ? NULL : $searchParams['minCreatedDate'],
				'maxCreatedDate'	=> empty($searchParams['maxCreatedDate']) ? NULL : $searchParams['maxCreatedDate'],
				'from'				=> empty($searchParams['from']) ? NULL : $searchParams['from'],
				'messageType'		=> empty($searchParams['messageType']) ? NULL : $searchParams['messageType'],
				'subject'			=> empty($searchParams['subject']) ? NULL : $searchParams['subject'],
				'to'				=> empty($searchParams['to']) ? NULL : $searchParams['to'],
				'isBounceBack'		=> empty($searchParams['isBounceBack']) ? NULL : $searchParams['isBounceBack']
			),
			'sortOptions'		=> empty($sortOptionsParams) ? NULL : array(
				'sortOption'		=> array(
					'column'			=> empty($sortOptionsParams['column']) ? NULL : $searchParams['column'],
					'order'				=> empty($sortOptionsParams['order']) ? NULL : $searchParams['order']
				)
			)
		);
		return $this->call('getMessageSummaryList', array(
			'token'				=> $this->token,
			'listOptionsEntity'	=> $listOptionsEntity,
		))->return;
	}

	/**
	 * Retrieves a list of all the tracked links in an email
	 * @param  integer	$id		Message id
	 * @return array			List of tracked links in the email
	 */
	public function getAllTrackedLinks($id)
	{
		$return = $this->call('getAllTrackedLinks', array(
			'token'		=> $this->token,
			'id'		=> $id
		));
		return empty($return->return) ? false : $return->return;
	}

	/**
	 * Retrieves the parameters of a URL based on its order in the message
	 * @param  integer	$messageId	Message id
	 * @param  integer	$order		Order number of the URL
	 * @return object				URL object
	 */
	public function getUrlByOrder($messageId, $order)
	{
		return $this->call('getUrlByOrder', array(
			'token'		=> $this->token,
			'messageId'	=> $messageId,
			'order'		=> $order
		))->return;
	}

	/**
	 * Creates and adds unjoin link, replacing the first occurrence of "&&&" in the message
	 * @param  integer $message_id    Message id
	 * @param  string  $name          Name of the link
	 * @param  string  $page_ok       URL to the OK page
	 * @param  string  $message_ok    Id of the OK bounceback message
	 * @param  string  $page_error    URL to the ERROR page
	 * @param  string  $message_error Id of the ERROR bounceback message
	 * @return integer                Link id
	 */
	public function createAndAddUnsubscribeUrl(
		$message_id    = 0,
		$name          = 'CC Unjoin',
		$page_ok       = NULL,
		$message_ok    = '',
		$page_error    = NULL,
		$message_error = ''
	)
	{
		return $this->call('createAndAddUnsubscribeUrl', array(
			'token'        => $this->token,
			'messageId'    => $message_id,
			'name'         => $name,
			'pageOK'       => $page_ok,
			'messageOK'    => $message_ok,
			'pageError'    => $page_error,
			'messageError' => $message_error
		))->return;
	}

	/**
	 * Creates and adds an update link in the message
	 * @param  string  $messageId      	 ID of the message
	 * @param  string  $name           	 Name of the message link
	 * @param  string  $params         	 The update parameters to apply to the member table (for a particular member)
	 * @param  string  $pageOK         	 URL to call if the action was successful
	 * @param  string  $messageOK        Message to display if the action was successful
	 * @param  string  $pageError      	 URL to call if the action was unsuccessful
	 * @param  string  $messageError   	 Message to display if the action was unsuccessful
	 * @return integer                   The order number of the URL
	 */
	public function createAndAddUpdateUrl($messageId, $name, $params, $pageOK = NULL, $messageOK = NULL, $pageKO = NULL, $messageKO = NULL)
	{
    	$parameters['token'] = $this->token;
		$parameters['messageId'] = $messageId;
		$parameters['name'] = $name;
		$parameters['parameters'] = $params;
		$parameters['pageOK'] = $pageOK == NULL || $pageOK == false ? '' : $pageOK;
		$parameters['messageOK'] = $messageOK == NULL || $messageOK == false ? '' : $messageOK;
		$parameters['pageError'] = $pageKO == NULL || $pageKO == false ? '' : $pageKO;
		$parameters['messageError'] = $messageKO == NULL || $messageKO == false ? '' : $messageKO;
		return $this->call('createAndAddUpdateUrl', $parameters)->return;
	}

	/**
	 * Creates and adds an action link in the message
	 * @param  string  $messageId        ID of the message
	 * @param  string  $name           	 Name of the message link
	 * @param  string  $action      	 The action to perform
	 * @param  string  $pageOK         	 URL to call if the action was successful
	 * @param  string  $messageOK        Message to display if the action was successful
	 * @param  string  $pageError      	 URL to call if the action was unsuccessful
	 * @param  string  $messageError   	 Message to display if the action was unsuccessful
	 * @return integer                   The order number of the URL
	 */
	public function createAndAddActionUrl($messageId, $name, $action, $pageOK = NULL, $messageOK = NULL, $pageKO = NULL, $messageKO = NULL)
	{
    	$parameters['token'] = $this->token;
		$parameters['messageId'] = $messageId;
		$parameters['name'] = $name;
		$parameters['action'] = $action;
		$parameters['pageOK'] = $pageOK == NULL || $pageOK == false ? '' : $pageOK;
		$parameters['messageOK'] = $messageOK == NULL || $messageOK == false ? '' : $messageOK;
		$parameters['pageError'] = $pageKO == NULL || $pageKO == false ? '' : $pageKO;
		$parameters['messageError'] = $messageKO == NULL || $messageKO == false ? '' : $messageKO;
		return $this->call('createAndAddActionUrl', $parameters)->return;
	}

    /**
	 * Updates a SMS or email message by object
	 * @param  array	$message	Message to update
	 * @return boolean				True/False
	 */
	public function updateMessageByObj(array $message)
	{
		return $this->call('updateMessageByObj', array(
			'token'		=> $this->token,
			'message'	=> $message
		))->return;
	}

	/**
	 * Creates an email message
	 * @param  array   $message SMS Message array
	 * @return integer          SMS Message id
	 */
	public function createSmsMessageByObj(array $message)
	{
		$defaults = array(
			'id'	=> 0
		);

		$message = $message + $defaults;

		$message['name'] = mb_substr($message['name'], 0, 50);
		if (!($message['body'] instanceof SoapVar)) $message['body'] = new SoapVar("<body><![CDATA[{$message['body']}]]></body>", XSD_ANYXML);

		return $this->call('createSmsMessageByObj', array(
			'token'   => $this->token,
			'message' => $message
		))->return;
	}

    /**
	 * Creates a dynamic content block
	 * @param  string  $name             Name of dynamic content block
	 * @param  string  $contentType      TEXT or HTML
	 * @param  string  $content 		 Dynamic content
	 * @param  string  $description      Description
	 * @return Integer                   Banner ID
	 */
	public function createBanner($name, $contentType, $content = NULL, $description = NULL) {
        $parameters['token'] = $this->token;
		$contentType = strtoupper($contentType);
		$parameters['name'] = $name;

		if ($contentType == "TEXT" || $contentType == "HTML") {
			$parameters['contentType'] = $contentType;
		}
		else {
			$parameters['contentType'] = "HTML";
		}
		if($content != NULL) {
			$parameters['content'] = $content;
		}
		if($description != NULL) {
			$parameters['description'] = $description;
		}
		return $this->call('createBanner', $parameters)->return;
	}


    /**
	 * Creates a dynamic content block by object
	 * @param  string  $name             Name of dynamic content block
	 * @param  string  $contentType      TEXT or HTML
	 * @param  string  $content 		 Dynamic content
	 * @param  string  $description      Description
	 * @return Integer                   Banner ID
	 */
	public function createBannerByObj($name, $contentType, $content = NULL, $description = NULL)
	{
        $contentType = strtoupper($contentType);

		$parameters['token'] 					= $this->token;
		$parameters['banner']['name'] 			= $name;
		$parameters['banner']['id']   			= 0;
        $parameters['banner']['contentType']	= in_array($contentType, array('HTML', 'TEXT')) ? $contentType : 'HTML';

		if (!empty($content)) {
            $parameters['banner']['content'] = '<![CDATA[' . utf8_encode($content) . ']]>';
		}
		if (!empty($description)) {
            $parameters['banner']['description'] = $description;
		}

		return $this->call('createBannerByObj', $parameters)->return;
	}

	/**
	 * Creates a dynamic content block by object
	 * @param  string  $id             	 ID of the dynamic content block
  	 * @return boolean                   True/False
	 */
	public function deleteBanner($id)
	{
    	$parameters['token'] = $this->token;
		$parameters['id'] = $id;
		return $this->call('deleteBanner', $parameters)->return;
	}

    /**
	 * Updates a dynamic content block by one field and value
	 * @param  string  $id             	 ID of the dynamic content block
	 * @param  string  $field          	 Field of the dynamic content block
	 * @param  string  $value          	 Value of the field
	 * @return boolean                   True/False
	 */
	public function updateBanner($id, $field, $value)
	{
        $parameters['token'] = $this->token;
		$parameters['id'] 	 = $id;
		$parameters['field'] = $field;
		$parameters['value'] = $value;
		return $this->call('updateBanner', $parameters)->return;
	}

    /**
	 * Updates a dynamic content block by object
	 * @param  string  $id             	 ID of the dynamic content block
	 * @param  string  $name             Name of dynamic content block
	 * @param  string  $contentType      TEXT or HTML
	 * @param  string  $content 		 Dynamic content
	 * @param  string  $description      Description
	 * @return boolean                   True/False
	 */
	public function updateBannerByObj($id, $name, $contentType, $content, $description = NULL)
	{
        $contentType = strtoupper($contentType);

		$parameters['token'] 					= $this->token;
		$parameters['banner']['name'] 			= $name;
		$parameters['banner']['id']   			= $id;
        $parameters['banner']['contentType']	= in_array($contentType, array('HTML', 'TEXT')) ? $contentType : 'HTML';

		if (!empty($content)) {
            $parameters['banner']['content'] = $content;
		}
		if (!empty($description)) {
            $parameters['banner']['description'] = $description;
		}

		return $this->call('updateBannerByObj', $parameters)->return;
	}

    /**
	 * Clone a dynamic content block
	 * @param  string  $id             	 ID of the dynamic content block
	 * @param  string  $newName          New name of the new dynamic content block
	 * @return integer                   Banner ID
	 */
	public function cloneBanner($id, $newName)
	{
		$parameters['token'] = $this->token;
		$parameters['id'] 	 = $id;
		$parameters['newName'] = $newName;
		return $this->call('cloneBanner', $parameters)->return;
	}

    /**
	 * Displays a preview of a dynamic content block
	 * @param  string  $id             	 ID of the dynamic content block
	 * @return string                    html
	 */
	public function getBannerPreview($id)
	{
        $parameters['token'] = $this->token;
		$parameters['id'] = $id;
		return $this->call('getBannerPreview', $parameters)->return;
	}

	/**
	 * Retrieves a dynamic content block using its ID
	 * @param  string  $id             	 ID of the dynamic content block
	 * @return object                    Banner
	 */
	public function getBanner($id)
	{
        $parameters['token'] = $this->token;
		$parameters['id'] = $id;
		return $this->call('getBanner', $parameters)->return;
	}


 	/**
	 * Retrieves a list of dynamic content blocks that contain the same given value in a specific field
	 * @param  string  $field          	 Field of the dynamic content block
	 * @param  string  $value          	 Value of the field
	 * @param  string  $limit          	 The size of the list (between 1 - 1000)
	 * @return array					 Banner ids
	 */
	public function getBannersByField($field, $value = '', $limit = 1)
	{
        $parameters['token'] = $this->token;
		$parameters['field'] = $field;
		$parameters['value'] = $value;
		$parameters['limit'] = min(max(1, $limit), 1000);

		$result = $this->call('getBannersByField', $parameters);

		return empty($result->return) ? NULL : $result->return;
	}

    /**
	 * Retrieves a list of dynamic content blocks from a given period
	 * @param  string  $dateBegin     	 start date of the period to retrieve (yyyy-MM-dd HH:mm:ss)
	 * @param  string  $dateEnd          end date of the period to retrieve (yyyy-MM-dd HH:mm:ss)
	 * @return array					 Banner ids
	 */
	public function getBannersByPeriod($dateBegin, $dateEnd)
	{
        $parameters['token'] = $this->token;
		$parameters['dateBegin'] = $dateBegin;
		$parameters['dateEnd'] = $dateEnd;
		$return = $this->call('getBannersByPeriod', $parameters);
		return empty($return->return) ? array() : $return->return;
	}

 	/**
	 * Retrieves a list of the last dynamic content blocks created
	 * @param  string  $limit          	 The size of the list (between 1 - 1000)
	 * @return array					 Banner ids
	 */
	public function getLastBanners($limit)
	{
        $parameters['token'] = $this->token;
		if (1 <= $limit && $limit >= 1000) {
        	$parameters['limit'] = $limit;
		}
		else {
        	$parameters['limit'] = 10;
		}
		return $this->call('getLastBanners', $parameters)->return;
	}

    /**
	 * Retrieves a list of dynamic content blocks and their details
	 * @param  array   $listOptions 	 Options for filtering
	 * @return object  					 Banner summary list
	 *
	 * Sample $listOptions
	 *
	 * $listOptions['page'] = "1";
	 * $listOptions['pageSize'] = "10";
     * $listOptions['search']['minModifiedDate'] = "2014-05-26 00:00:00";
     * $listOptions['search']['maxModifiedDate'] = "2014-05-30 00:00:00";
     * $listOptions['search']['contentType'] = "HTML";
	 * $listOptions['sortOptions']['sortOption']['column'] = "name";
	 * $listOptions['sortOptions']['sortOption']['order']  = "asc";
	 */
	public function getBannerSummaryList($listOptions) {
        $parameters['token'] = $this->token;
		$parameters['listOptions'] = $listOptions;
		return $this->call('getBannerSummaryList', $parameters);
	}

	/**
	 * Activates tracking for all untracked dynamic content block links and saves the dynamic content blocks
	 * @param  string  $id             	 ID of the dynamic content block
	 * @return integer 			         The last tracked link's order number
	 *
	 * If 'Fault occurred while processing', links could already be tracked
	 */
	public function trackAllBannerLinks($id) {
    	$parameters['token'] = $this->token;
		$parameters['id'] = $id;
		return $this->call('trackAllBannerLinks', $parameters)->return;
	}

	/**
	 * Untracks all the dynamic content block links
	 * @param  string  $id	ID of the dynamic content block
	 * @return boolean		True/False
	 */
	public function untrackAllBannerLinks($id) {
    	$parameters['token'] 	= $this->token;
		$parameters['id'] 		= $id;
		return $this->call('untrackAllBannerLinks', $parameters)->return;
	}

    /**
	 * Tracks the dynamic content block link by it's position in the dynamic content block
	 * @param  string  $id             	 ID of the dynamic content block
	 * @param  string  $position       	 The position of the link in the dynamic content block
	 * @return integer 			         The order number of the URL
	 */
	public function trackBannerLinkByPosition($id, $position) {
        $parameters['token'] = $this->token;
		$parameters['id'] = $id;
		$parameters['position'] = $position;
		return $this->call('trackBannerLinkByPosition', $parameters)->return;
	}

 	/**
	 * Untracks a link in the dynamic content block by its order
	 * @param  string  $id             	 ID of the dynamic content block
	 * @param  string  $order       	 The order number of the URL
	 * @return boolean 			         True/False
	 */
	public function untrackBannerLinkByOrder($id, $order) {
        $parameters['token'] = $this->token;
		$parameters['id'] = $id;
		$parameters['order'] = $order;

		return $this->call('untrackBannerLinkByOrder', $parameters)->return;
	}

    /**
	 * Retrieves a list of all the tracked links in a dynamic content block
	 * @param  string  $id             	 ID of the dynamic content block
	 * @return array                     Order numbers of all tracked links
	 */
	public function getAllBannerTrackedLinks($id) {
        $parameters['token'] = $this->token;
		$parameters['id'] = $id;
		return $this->call('getAllBannerTrackedLinks', $parameters)->return;
	}


 	/**
	 * Retrieves a list of all the unused tracked links in a dynamic content block
	 * @param  string  $id             	 ID of the dynamic content block
	 * @return array                     Order numbers of all unused tracked links
	 */
	public function getAllUnusedBannerTrackedLinks($id) {
        $parameters['token'] = $this->token;
		$parameters['id'] = $id;
		return $this->call('getAllUnusedBannerTrackedLinks', $parameters)->return;
	}

	/**
	 * Retrieves a list of all the trackable links in a dynamic content block
	 * @param  string  $id             	 ID of the dynamic content block
	 * @return array                     Order numbers of all trackable links
	 */
	public function getAllBannerTrackableLinks($id) {
        $parameters['token'] = $this->token;
		$parameters['id'] = $id;
		return $this->call('getAllBannerTrackableLinks', $parameters)->return;
	}

	//*********************************
	// BANNER LINKS
	//*********************************

	/**
	 * Creates a standard link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @param  string  $url            	 URL of the link
	 * @return integer                   The order number of the URL
	 */
	public function createStandardBannerLink($bannerId, $name, $url) {
        $parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		$parameters['url'] = $url;
		return $this->call('createStandardBannerLink', $parameters)->return;
	}

	/**
	 * Creates and adds a standard link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @param  string  $url            	 URL of the link
	 * @return integer                   The order number of the URL
	 */
	public function createAndAddStandardBannerLink($bannerId, $name, $url) {
	  	$parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		$parameters['url'] = $url;
		return $this->call('createAndAddStandardBannerLink', $parameters)->return;
	}

	/**
	 * Creates an unsubscribe link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @param  string  $pageOK         	 URL to call if the action was successful
	 * @param  string  $messageOK        Message to display if the action was successful
	 * @param  string  $pageError      	 URL to call if the action was unsuccessful
	 * @param  string  $messageError   	 Message to display if the action was unsuccessful
	 * @return integer                   The order number of the URL
	 */
	public function createUnsubscribeBannerLink($bannerId, $name, $pageOK=NULL, $messageOK=NULL, $pageError=NULL, $messageError=NULL) {
    	$parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		if($pageOK != NULL)
			$parameters['pageOK'] = $pageOK;
		if($messageOK != NULL)
			$parameters['messageOK'] = $messageOK;
		if($pageError != NULL)
			$parameters['pageError'] = $pageError;
		if($messageError != NULL)
			$parameters['messageError'] = $messageError;
		return $this->call('createUnsubscribeBannerLink', $parameters)->return;
	}

	/**
	 * Creates and adds an unsubscribe link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @param  string  $pageOK         	 URL to call if the action was successful
	 * @param  string  $messageOK        Message to display if the action was successful
	 * @param  string  $pageError      	 URL to call if the action was unsuccessful
	 * @param  string  $messageError   	 Message to display if the action was unsuccessful
	 * @return integer                   The order number of the URL
	 */
	public function createAndAddUnsubscribeBannerLink($bannerId, $name, $pageOK=NULL, $messageOK=NULL, $pageError=NULL, $messageError=NULL) {
    	$parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		if($pageOK != NULL)
			$parameters['pageOK'] = $pageOK;
		if($messageOK != NULL)
			$parameters['messageOK'] = $messageOK;
		if($pageError != NULL)
			$parameters['pageError'] = $pageError;
		if($messageError != NULL)
			$parameters['messageError'] = $messageError;
		return $this->call('createAndAddUnsubscribeBannerLink', $parameters)->return;
	}

	/**
	 * Creates a personalized link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @param  string  $url            	 URL of the link
	 * @return integer                   The order number of the URL
	 */
	public function createPersonalisedBannerLink($bannerId, $name, $url) {
        $parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		$parameters['url'] = $url;
		return $this->call('createPersonalisedBannerLink', $parameters)->return;
	}

	/**
	 * Creates and adds a personalized link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @param  string  $url            	 URL of the link
	 * @return integer                   The order number of the URL
	 */
	public function createAndAddPersonalisedBannerLink($bannerId, $name, $url) {
        $parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		$parameters['url'] = $url;
		return $this->call('createAndAddPersonalisedBannerLink', $parameters)->return;
	}

	/**
	 * Creates an update link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @param  string  $params         	 The update parameters to apply to the member table (for a particular member)
	 * @param  string  $pageOK         	 URL to call if the action was successful
	 * @param  string  $messageOK        Message to display if the action was successful
	 * @param  string  $pageError      	 URL to call if the action was unsuccessful
	 * @param  string  $messageError   	 Message to display if the action was unsuccessful
	 * @return integer                   The order number of the URL
	 *
	 * Note: Not sure what $params is supposed to accept
	 */
	public function createUpdateBannerLink($bannerId, $name, $params, $pageOK=NULL, $messageOK=NULL, $pageError=NULL, $messageError=NULL) {
    	$parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		$parameters['parameters'] = $params;
		$parameters['pageOK'] = $pageOK == NULL || $pageOK == false ? '' : $pageOK;
		$parameters['messageOK'] = $messageOK == NULL || $messageOK == false ? '' : $messageOK;
		$parameters['pageError'] = $pageError == NULL || $pageError == false ? '' : $pageError;
		$parameters['messageError'] = $messageError == NULL || $messageError == false ? '' : $messageError;
		return $this->call('createUpdateBannerLink', $parameters)->return;
	}

	/**
	 * Creates and adds an update link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @param  string  $params         	 The update parameters to apply to the member table (for a particular member)
	 * @param  string  $pageOK         	 URL to call if the action was successful
	 * @param  string  $messageOK        Message to display if the action was successful
	 * @param  string  $pageError      	 URL to call if the action was unsuccessful
	 * @param  string  $messageError   	 Message to display if the action was unsuccessful
	 * @return integer                   The order number of the URL
	 *
	 * Note: Not sure what $params is supposed to accept
	 */
	public function createAndAddUpdateBannerLink($bannerId, $name, $params, $pageOK=NULL, $messageOK=NULL, $pageError=NULL, $messageError=NULL) {
    	$parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		$parameters['parameters'] = $params;
		$parameters['pageOK'] = $pageOK == NULL || $pageOK == false ? '' : $pageOK;
		$parameters['messageOK'] = $messageOK == NULL || $messageOK == false ? '' : $messageOK;
		$parameters['pageError'] = $pageError == NULL || $pageError == false ? '' : $pageError;
		$parameters['messageError'] = $messageError == NULL || $messageError == false ? '' : $messageError;
		return $this->call('createAndAddUpdateBannerLink', $parameters)->return;
	}

	/**
	 * Creates an action link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @param  string  $action         	 The action to perform
	 * @param  string  $pageOK         	 URL to call if the action was successful
	 * @param  string  $messageOK        Message to display if the action was successful
	 * @param  string  $pageError      	 URL to call if the action was unsuccessful
	 * @param  string  $messageError   	 Message to display if the action was unsuccessful
	 * @return integer                   The order number of the URL
	 *
	 * Note: Not sure what $action is supposed to accept
	 */
	public function createActionBannerLink($bannerId, $name, $action, $pageOK=NULL, $messageOK=NULL, $pageError=NULL, $messageError=NULL) {
    	$parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		$parameters['action'] = $action;
		$parameters['pageOK'] = $pageOK == NULL || $pageOK == false ? '' : $pageOK;
		$parameters['messageOK'] = $messageOK == NULL || $messageOK == false ? '' : $messageOK;
		$parameters['pageError'] = $pageError == NULL || $pageError == false ? '' : $pageError;
		$parameters['messageError'] = $messageError == NULL || $messageError == false ? '' : $messageError;
		return $this->call('createActionBannerLink', $parameters)->return;
	}

	/**
	 * Creates and adds an action link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @param  string  $action      	 The action to perform
	 * @param  string  $pageOK         	 URL to call if the action was successful
	 * @param  string  $messageOK        Message to display if the action was successful
	 * @param  string  $pageError      	 URL to call if the action was unsuccessful
	 * @param  string  $messageError   	 Message to display if the action was unsuccessful
	 * @return integer                   The order number of the URL
	 *
	 * Note: Not sure $params is supposed to accept
	 */
	public function createAndAddActionBannerLink($bannerId, $name, $action, $pageOK=NULL, $messageOK=NULL, $pageError=NULL, $messageError=NULL) {
    	$parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		$parameters['action'] = $action;
		$parameters['pageOK'] = $pageOK == NULL || $pageOK == false ? '' : $pageOK;
		$parameters['messageOK'] = $messageOK == NULL || $messageOK == false ? '' : $messageOK;
		$parameters['pageError'] = $pageError == NULL || $pageError == false ? '' : $pageError;
		$parameters['messageError'] = $messageError == NULL || $messageError == false ? '' : $messageError;
		return $this->call('createAndAddActionBannerLink', $parameters)->return;
	}

	/**
	 * Creates a mirror link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @return integer                   The order number of the URL
	 */
	public function createMirrorBannerLink($bannerId, $name) {
    	$parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		return $this->call('createMirrorBannerLink', $parameters)->return;
	}

	/**
	 * Creates and adds a mirror link for the dynamic content block
	 * @param  string  $bannerId       	 ID of the dynamic content block
	 * @param  string  $name           	 Name of the dynamic content block link
	 * @return integer                   The order number of the URL
	 */
	public function createAndAddMirrorBannerLink($bannerId, $name) {
    	$parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['name'] = $name;
		return $this->call('createAndAddMirrorBannerLink', $parameters)->return;
	}

	/**
	 * Updates a dynamic content block link by field
	 * @param  string  $bannerId       	 ID of the dynamic content block
     * @params string  $order            The order number of the URL
	 * @param  string  $field          	 Field of the dynamic content block
	 * @param  string  $value          	 Value of the field
	 * @return boolean                   True/False
	 */
	public function updateBannerLinkByField($bannerId, $order, $field, $value = NULL) {
	 	$parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
        $parameters['order'] = $order;
		$parameters['field'] = $field;
		$parameters['value'] = $value;
		return $this->call('updateBannerLinkByField', $parameters)->return;
	}

	/**
	 * Retrieves a dynamic content block link by its order number
	 * @param  string  $bannerId       	 ID of the dynamic content block
     * @params string  $order            The order number of the URL
	 * @return object                    Banner link
	 */
	public function getBannerLinkByOrder($bannerId, $order) {
        $parameters['token'] = $this->token;
		$parameters['bannerId'] = $bannerId;
		$parameters['order'] = $order;
		return $this->call('getBannerLinkByOrder', $parameters)->return;
	}

	/**
	 * Sends a test mail to a member
	 * @param  integer $message_id    Message id
	 * @param  integer $member_id     Member id
	 * @param  string  $part          Message part
	 * @param  string  $subject       Subject of test campaign
	 * @param  string  $campaign_name Name of test campaign
	 * @return boolean                Success
	 */
	public function testEmailMessageByMember(
		$message_id    = 0,
		$member_id     = 0,
		$part          = 'MULTIPART',
		$subject       = '[TEST]',
		$campaign_name = 'Test'
	)
	{
		return $this->call('testEmailMessageByMember', array(
			'token'        => $this->token,
			'id'           => $message_id,
			'memberId'     => $member_id,
			'part'         => $part,
			'subject'      => $subject,
			'campaignName' => $campaign_name
		))->return;
	}

	/**
	 * Creates a segment inclusion/exclusion criteria
	 * @param  string  $id         Segment id
	 * @param  string  $segment_id Id of segment to include/exclude
	 * @param  string  $operator   Whether to include or exclude
	 * @param  string  $orderFrag  Relation to other rules
	 * @return boolean             Success
	 */
	public function segmentationCreateInclusionExclusionCriteriaByObj(
		$id = 0,
		$segment_id = 0,
		$operator = '',
		$orderFrag = 0,
		$groupNumber = 0
	)
	{
		return $this->call('segmentationCreateInclusionExclusionCriteriaByObj', array(
			'token'                      => $this->token,
			'apiInclusionExclusionCriteria' => array(
				'id'          => $id,
				'difflistId'  => $segment_id,
				'operator'    => $operator,
				'orderFrag'   => $orderFrag,
				'groupNumber' => $groupNumber
			)
		))->return;
	}

	/**
	 * Deletes a segment
	 * @param  string  $id Segment id
	 * @return boolean     Success
	 */
	public function segmentationDeleteSegment($id = 0)
	{
		return $this->call('segmentationDeleteSegment', array(
			'token'      => $this->token,
			'difflistId' => $id
		))->return;
	}

	/**
	 * Gets the distinct members of a segment
	 * @param  integer $id Segment id
	 * @return integer     Segment count
	 */
	public function segmentationDistinctCount($id)
	{
		return $this->call('segmentationDistinctCount', array(
			'token' => $this->token,
			'id'    => $id
		))->return;
	}

	/**
	 * Creates a new segment
	 * @param  string  $name        Segment name
	 * @param  string  $description Segment description
	 * @param  string  $sample_type Segment sample type
	 * @param  string  $sample_rate Segment sample_rate
	 * @return integer              Segment id
	 */
	public function segmentationCreateSegment(
		$name        = '',
		$description = '',
		$sample_type = 'ALL',
		$sample_rate = ''
	)
	{
		return $this->call('segmentationCreateSegment', array(
			'token'           => $this->token,
			'apiSegmentation' => array(
				'id'         => 0,
				'name'       => $name,
				'desc'       => $description,
				'sampleType' => $sample_type,
				'sampleRate' => $sample_rate
			)
		))->return;
	}

	public function segmentationAddStringDemographicCriteriaByObj(
		$segmentId,
		$columnName,
		$operator,
		$values=NULL,
		$groupName=NULL,
		$groupNumber=NULL,
		$orderFrag=NULL
	)
	{
        $parameters = array();

		$parameters['id']			= $segmentId;
		$parameters['columnName'] 	= $columnName;

        $parameters['operator'] 	= self::OPERATOR_DEFAULT;
		if (in_array($operator, $this->operators) || in_array($operator, $this->case_sensitive_operators)) {
            $parameters['operator'] = $operator;
		}

		if (!empty($values)) {
			if (is_array($values)) {
				foreach($values as $key => $value) {
					$parameters['values'][$key] = $value;
				}
			}
			else {
            	$parameters['values'] = $values;
			}
		}

		if (!empty($groupName)) {
        	$parameters['groupName'] = $groupName;
		}

        $parameters['groupNumber'] = $groupNumber;
        $parameters['orderFrag'] = $orderFrag;

		return $this->call('segmentationAddStringDemographicCriteriaByObj', array(
			'token'           			=> $this->token,
			'stringDemographicCriteria' => $parameters
		))->return;

	}

	public function segmentationAddCampaignActionCriteriaByObj($id, $campaignId, $operator, $groupName = null, $groupNumber = null, $orderFrag = null, $messageOrder= null, $serieId = 0)
	{
        $parameters = array();

		$parameters['id']			= $id;
		$parameters['campaignId']	= $campaignId;
		$parameters['operator']		= $operator;

		if (!empty($groupName)) {
        	$parameters['groupName'] = $groupName;
		}

        $parameters['groupNumber'] 	= $groupNumber;
        $parameters['orderFrag'] 	= $orderFrag;
        $parameters['messageOrder']	= $messageOrder;
        $parameters['serieId']		= $serieId;

		return $this->call('segmentationAddCampaignActionCriteriaByObj', array(
			'token'         	=> $this->token,
			'actionCriteria'	=> $parameters
		))->return;

	}
	
	/**
	 * Gets the segment by id
	 * @param  integer $id	Segment id
	 * @return object     	Segment object
	 */
	public function segmentationGetSegmentById($id)
	{
		return $this->call('segmentationGetSegmentById', array(
			'token' 		=> $this->token,
			'difflistId'	=> $id
		))->return;
	}	

}
