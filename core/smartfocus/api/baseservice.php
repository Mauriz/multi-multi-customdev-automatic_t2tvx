<?php

namespace SmartFocus\API;

use \SoapClient;
use \SoapFault;
use \SoapVar;
use \Exception;

/**
 * BaseService class
 *
 * Base class for low level interaction with the SOAP version of the
 * SmartFocus Campaign Management API.
 * All actual implementations have to extend this class.
 *
 * @author Christian Tandler <ctandler@emailvision.com>
 *
 */

class BaseService
{
	const DEFAULT_CONNECTION_ATTEMPTS = 3;

	/**
	 * Name of the API
	 * @var string
	 */
	protected $api_name = null;

	/**
	 * URL to the WSDL
	 * @var string
	 */
	protected $wsdl = null;

	/**
	 * Holds the Authentification instance
	 * @var AuthentificationInterface
	 */
	protected $auth_service = null;

	/**
	 * Holds the PHP SoapClient instance
	 * @var SoapClient
	 */
	protected $client = null;

	/**
	 * Holds the current authentification token for the SOAP connection
	 * @var string
	 */
	protected $token = null;

	/**
	 * Connection attemts
	 * @var string
	 */
	protected $connection_attempts = self::DEFAULT_CONNECTION_ATTEMPTS;

	/**
	 * Constructor
	 * @param AuthentificationInterface Instance of the Authentification
	 */
	public function __construct(AuthenticationInterface $auth_service)
	{
		if (!$this->api_name) {
			throw new Exception('SERVICE EXCEPTION: No api_name has been set.');
		}
		if (!$this->wsdl) {
			throw new Exception('SERVICE EXCEPTION: No wsdl has been set.');
		}
		if (!$auth_service) {
			throw new Exception('SERVICE EXCEPTION: No auth_service has been passed.');
		}

		$this->auth_service = $auth_service;

		$this->connect();
	}

	/**
	 * Builds the WSDL URL
	 * @param string $platform API server
	 * @return string WSDL URL
	 */
	protected function getWSDL($platform = '')
	{
		return preg_replace('/{server}/', $platform, $this->wsdl);
	}

	/**
	 * Connects the SOAP API
	 * @return $this Itself for function chaining
	 */
	protected function connect()
	{
		if (!$this->client) {
			$auth_info = $this->auth_service->getAuthenticationInfo($this->api_name);
			$this->client = new SoapClient($this->getWSDL($auth_info->platform), array('trace' => 1));
			$this->token = $auth_info->token;
		}

		return $this;
	}

	/**
	 * Calls a SOAP method.
	 * If the session died, it will try up to three times to reconnect.
	 * @param  string  $method    Method name to call
	 * @param  array   $arguments Arguments to pass to the method
	 * @param  integer $retry     Internal recursion counter for reconnect tries
	 * @return object             Returned object of the method
	 */
	protected function call($method = '', array $arguments, $retry = 0)
	{
		$return = null;

		if (!$retry && is_null($this->client)) {
            $this->connect();
		}

		try {
			$return = $this->client->$method($arguments);
		} catch (SoapFault $e) {
			if ($retry === $this->connection_attempts) {
				throw $e;
			}
			if (isset($e->detail->ConnectionServiceException) && $e->detail->ConnectionServiceException->status === 'CHECK_SESSION_FAILED') {
				$this->connect();
				$this->call($method, $arguments, $retry + 1);
			} else {
				error_log($this->client->__getLastRequest());
				error_log($this->client->__getLastResponse());
				throw $e;
			}
		}

		return $return;
	}

	/**
	 * Closes the API connection
	 * @return object	Returned object of the method
	 */
	public function disconnect()
	{
		$result = $this->call('closeApiConnection', array(
			'token' => $this->token
		))->return;

		$this->client = null;

		return $result;
	}

	/**
	 * Sets the connection attemts
	 * @return boolean  True
	 */
	public function setConnectionAttemts($connection_attempts = 0)
	{
		$this->connection_attempts = empty($connection_attempts) ? DEFAULT_CONNECTION_ATTEMPTS : $connection_attempts;
		return true;
	}
	
	public function getDebug()
	{
		if (empty($this->client)) {
			return NULL;
		}
		return (object) array(
			'request' 	=> trim($this->client->__getLastRequest()),
			'response' 	=> trim($this->client->__getLastResponse()),
		);
	}

}
