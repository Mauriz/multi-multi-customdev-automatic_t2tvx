<?php namespace SmartFocus\API;

use \SoapClient;
use \SoapFault;
use \SoapVar;

/**
 * BatchMemberService class
 *
 * Wrapper class for low level interaction with the SOAP version of the
 * SmartFocus Data Mass Update API.
 *
 * @version 10.16 (2014-03-16)
 * @author Christian Tandler <ctandler@emailvision.com>
 *
 */
class BatchMemberService extends BaseService
{

	/**
	 * Name of the API
	 * @var string
	 */
	protected $api_name = 'API_BATCH_MEMBER';

	/**
	 * URL to the WSDL
	 * @var string
	 */
	protected $wsdl = 'http://{server}/apibatchmember/services/BatchMemberService?wsdl';

	/**
	 * Override connect from BaseService to use MTOM SoapClient
	 * @return $this Itself for function chaining
	 */
	protected function connect()
	{
		if (!$this->client) {
			$auth_info = $this->auth_service->getAuthenticationInfo($this->api_name);
			$this->client = new MtomSoapClient($this->getWSDL($auth_info->platform), array('trace' => 1));
			$this->token = $auth_info->token;
		}

		return $this;
	}

	/**
	 * --------------------------------------------
	 * !!!           HELPER FUNCTIONS           !!!
	 * --------------------------------------------
	 *
	 * These functions are for simpler internal handling.
	 */

	/**
	 * ---------------------------------------------
	 * !!! API METHOD IMPLEMENTATION STARTS HERE !!!
	 * ---------------------------------------------
	 *
	 * If one is missing, please implement it below.
	 */

	/**
	 * Queues an file insert upload
	 * @param  string  $file          File data
	 * @param  string  $fileName      File name
	 * @param  string  $separator     File separator
	 * @param  array   $mapping       File mapping
	 * @param  string  $criteria      Criteria for merging
	 * @param  string  $fileEncoding  File encoding
	 * @param  boolean $skipFirstLine Whether to skip the first line
	 * @param  string  $dateFormat    Format of dates in the file
	 * @return integer                Upload id
	 */
	public function uploadFileInsert(
		$file,
		$fileName,
		$separator,
		$mapping,
		$criteria = 'LOWER(EMAIL)',
		$fileEncoding = null,
		$skipFirstLine = false,
		$dateFormat = null,
        $autoMapping = false
	)
	{
		$parameters['token'] = $this->token;
		$parameters['file'] = $file;
		$parameters['insertUpload']['fileName'] = $fileName;
		$parameters['insertUpload']['separator'] = $separator;

		if ($fileEncoding) {
			$parameters['insertUpload']['fileEncoding'] = $fileEncoding;
		}

		$parameters['insertUpload']['skipFirstLine'] = $skipFirstLine;

		if (!empty($dateFormat)) {
			$parameters['insertUpload']['dateFormat'] = $dateFormat;
		}

		$parameters['insertUpload']['criteria'] = $criteria;

        $parameters['insertUpload']['autoMapping'] = $autoMapping;

		if (!$autoMapping) {
			$parameters['insertUpload']['mapping'] = $mapping;
		}

		return $this->call('uploadFileInsert', $parameters)->return;
	}

	/**
	 * Queues an file merge upload
	 * @param  string  $file          File data
	 * @param  string  $fileName      File name
	 * @param  string  $separator     File separator
	 * @param  array   $mapping       File mapping
	 * @param  string  $criteria      Criteria for merging
	 * @param  string  $fileEncoding  File encoding
	 * @param  boolean $skipFirstLine Whether to skip the first line
	 * @param  string  $dateFormat    Format of dates in the file
	 * @return integer                Upload id
	 */
	public function uploadFileMerge(
		$file,
		$fileName,
		$separator,
		$mapping,
		$criteria = 'LOWER(EMAIL)',
		$fileEncoding = null,
		$skipFirstLine = null,
		$dateFormat = null
	)
	{
		$parameters['token'] = $this->token;
		$parameters['file'] = $file;
		$parameters['mergeUpload']['fileName'] = $fileName;
		$parameters['mergeUpload']['separator'] = $separator;

		if ($fileEncoding) {
			$parameters['mergeUpload']['fileEncoding'] = $fileEncoding;
		}
		if ($skipFirstLine != null && is_bool($skipFirstLine)) {
			$parameters['mergeUpload']['skipFirstLine'] = $skipFirstLine;
		}

		if ($dateFormat) {
			$parameters['mergeUpload']['dateFormat'] = $dateFormat;
		}

		$parameters['mergeUpload']['criteria'] = $criteria;
		$parameters['mergeUpload']['mapping'] = $mapping;

		return $this->call('uploadFileMerge', $parameters)->return;
	}

	/**
	 * Returns the upload status
	 * @param  integer  $id	CCMD import id
	 * @return string       Status
	 */
	public function getUploadStatus($id)
	{
		$response = $this->call('getUploadStatus', array(
			'token'    => $this->token,
			'uploadId' => $id
		));
		
		return empty($response->return) ? NULL : $response->return;
		
	}
	
	/**
	 * Retrieves a list of uploads and their details
	 * @param  integer	$page			The page to return
	 * @param  integer	$pageSize		Number of elements to return per page
	 * @param  array	$search			Search options : source (API_BATCH_MEMBER or CCMD), minCreatedDate, maxCreatedDate, status (STORAGE, VALIDATED, QUEUED, IMPORTING, ERROR, FAILURE, DONE, DONE WITH ERROR(S))
	 * @param  array	$sortOptions	Sorting options : column, order
	 * @return object	entities
	 */
	public function getUploadSummaryList($page = 1, $pageSize = 20, array $search = NULL, array $sortOptions = NULL)
	{
		$pageSize = min(max(1, $pageSize), 20);
		$listOptions = array (
			'page'		=> $page,
			'pageSize'	=> $pageSize
		);
		if (!empty($search)) {
			$listOptions['search'] = $search;
		}
		if (!empty($sortOptions)) {
			$listOptions['sortOptions']	= $sortOptions;
		}
		
		$response = $this->call('getUploadSummaryList', array(
			'token'    		=> $this->token,
			'listOptions'	=> $listOptions
		));
		return empty($response->return) ? NULL : $response->return;
	}

	/**
	 * Retrieves the log file associated with an upload
	 * @param  integer	$id	CCMD import id
	 * @return string	Log
	 */
	public function getLogFile($id)
	{
		$response = $this->call('getLogFile', array(
			'token'    => $this->token,
			'uploadId' => $id
		));
		
		return empty($response->return) ? NULL : $response->return;
		
	}

}
