<?php

namespace SmartFocus\API;

use \SoapClient;
use \SoapVar;
use \Exception;
use \DateTime;

/**
 * NotificationService class
 *
 * NMP trigger API
 *
 * @author Christian Tandler <ctandler@emailvision.com>
 *
 */

class NotificationService
{

	/**
	 * WSDL for SoapClient
	 * @var string
	 */
	protected $wsdl = 'http://api.notificationmessaging.com/NMSOAP/NotificationService?wsdl';

	/**
	 * SoapClient instance
	 * @var SoapClient
	 */
	protected $client = null;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->client = new SoapClient($this->wsdl, array('trace' => 1));
	}

	/**
	 * --------------------------------------------
	 * !!!           HELPER FUNCTIONS           !!!
	 * --------------------------------------------
	 *
	 * These functions are for simpler internal handling.
	 */

	/**
	 * Builds the send object for use in the api calls
	 * @param  string  $email          Email address
	 * @param  string  $encrypt        NMP security tag
	 * @param  integer $notificationId NMP Message id
	 * @param  string  $random         NMP unique random tag
	 * @param  string  $senddate       When to send the mail (W3C/ATOM)
	 * @param  array   $content        Array of the content blocks ([1 => 'value', 2 => 'value'])
	 * @param  array   $dyn            Array of dyn variables (['firstname' => 'value', 'lastname' => 'value'])
	 * @param  string  $synchrotype    Synchro type
	 * @param  string  $uidkey         UID key for synchro
	 * @return array                   Prepared send object
	 */
	static function buildSendObject(
		$email = '',
		$encrypt = '',
		$notificationId = 0,
		$random = '',
		$senddate = '',
		array $content = array(),
		array $dyn = array(),
		$synchrotype = 'NOTHING',
		$uidkey = 'EMAIL'
	)
	{
		$now = new DateTime();
		$senddate = ($senddate) ? $senddate : $now->format(DateTime::W3C);

		$structured_content['entry'] = array();
		foreach ($content as $key => $value) {
			$structured_content['entry'][] = array(
				'key' => $key,
				'value' => ($value instanceof SoapVar) ? $value : new SoapVar("<value><![CDATA[{$value}]]></value>", XSD_ANYXML)
			);
		}

		$structured_dyn['entry'] = array();
		foreach ($dyn as $key => $value) {
			$structured_dyn['entry'][] = array(
				'key' => $key,
				'value' => ($value instanceof SoapVar) ? $value : new SoapVar("<value><![CDATA[{$value}]]></value>", XSD_ANYXML)
			);
		}

		return array(
			'email' => $email,
			'encrypt' => $encrypt,
			'notificationId' => $notificationId,
			'random' => $random,
			'senddate' => $senddate,
			'content' => $structured_content,
			'dyn' => $structured_dyn,
			'synchrotype' => $synchrotype,
			'uidkey' => $uidkey
		);
	}

	/**
	 * ---------------------------------------------
	 * !!! API METHOD IMPLEMENTATION STARTS HERE !!!
	 * ---------------------------------------------
	 *
	 * If one is missing, please implement it below.
	 */

	/**
	 * Send object
	 * @param  array  $send_object Send object
	 * @return string              Send status
	 */
	public function sendObject(array $send_object)
	{
		$response = $this->client->sendObject(array(
			'arg0' => $send_object
		));

		return $response->return;
	}

	/**
	 * Send objects
	 * @param  array  $send_objects Array of send objects
	 * @return string               Send status
	 */
	public function sendObjects(array $send_objects)
	{
		$response = $this->client->sendObjects(array(
			'arg0' => array(
				'sendrequest' => $send_objects
			)
		));

		return $response->return;
	}

	/**
	 * Send objects
	 * @param  array $send_objects Array of send objects
	 * @return array               List of send status for each request
	 */
	public function sendObjectsWithFullStatus(array $send_objects)
	{
		$response = $this->client->sendObjectsWithFullStatus(array(
			'arg0' => array(
				'sendrequest' => $send_objects
			)
		));

		return $response->return;
	}

}
