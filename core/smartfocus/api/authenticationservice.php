<?php

namespace SmartFocus\API;

use \SoapClient;
use \Exception;

/**
 * AuthenticationService class
 *
 * API user based authentication provider for the SmartFocus API
 *
 * @author Christian Tandler <ctandler@emailvision.com>
 *
 */

class AuthenticationService implements AuthenticationInterface
{

	/**
	 * WSDL for SoapClient
	 * @var string
	 */
	protected $wsdl = array(
		'API_CCMD'   => 'http://{server}/apiccmd/services/CcmdService?wsdl',
		'API_MEMBER' => 'http://{server}/apimember/services/MemberService?wsdl',
	);

	/**
	 * SmartFocus platform
	 * @var string
	 */
	protected $platform = null;

	/**
	 * SmartFocus login
	 * @var string
	 */
	protected $login = null;

	/**
	 * SmartFocus password
	 * @var string
	 */
	protected $password = null;

	/**
	 * SoapClient instance
	 * @var SoapClient
	 */
	protected $client = null;

	/**
	 * Constructor
	 * @param  string $platform SmartFocus platform
	 * @param  string $login    SmartFocus login
	 * @param  string $password SmartFocus password
	 * @param  string $api_name SmartFocus API name
	 */
	public function __construct($platform, $login, $password, $api_name)
	{
		if (!array_key_exists($api_name, $this->wsdl)) {
			throw new Exception('SERVICE_EXCEPTION: '.$api_name.' is not a valid API name.');
		}

		$this->platform = $platform;
		$this->login    = $login;
		$this->password = $password;

		$this->client = new SoapClient(preg_replace('/{server}/', $platform, $this->wsdl[$api_name]), array('trace' => 1));
	}

	/**
	 * Open connection
	 * @return string Token
	 */
	public function openConnection()
	{
		$response = $this->client->openConnection(array(
			'login'    => $this->login,
			'password' => $this->password,
			'key'      => $this->key
		));

		return $response->return;
	}

	/**
	 * Open connection from interface
	 * @param  string $api_name API name
	 * @return object           Connection info (manager_type, platform, token)
	 */
	public function getAuthenticationInfo($api_name)
	{
		$token = $this->openConnection();

		return (object)array(
			'manager_type' => 'API',
			'platform' => $this->platform,
			'token' => $token
		);
	}

}
