<?php
namespace SmartFocus\API;

use \SoapClient;

ini_set('memory_limit', '-1');


/**
 * MtomSoapClient class
 *
 * Remove MTOM overhead in response as the PHP internal SoapClient is incompatible.
 *
 * @author Christian Tandler <ctandler@emailvision.com>
 *
 */

class MtomSoapClient extends SoapClient{

	public function __doRequest($request, $location, $action, $version, $one_way = 0){
		$response = parent::__doRequest($request, $location, $action, $version, $one_way);
		// remove mtom overhead from response

		$response = preg_replace('#^.*(<soap:Envelope.*>)[^>]*$#s', '$1', $response);
		return $response;
	}

}
