<?php
/*
*
*	@author: Rahel <rahel.chowdhury@smartfocus.com>
*	@copyright: 2017
*
*	@package: SmartFocus\HouseKeeping
*/

namespace SmartFocus;

mb_internal_encoding('UTF-8');

class HouseKeeping
{
	protected $configs;

	protected $clean_directory;

	protected $clean_rules;

	protected $log_dir_key = 'LOG_DIRECTORY';
	protected $clean_rules_key = 'CLEAN_RULES';
	//[CLEAN_RULES]
	//	|----------[MATCH_PATTERN] = '*.log'
	protected $clean_rules_pattern_key = 'MATCH_PATTERN';
	//[CLEAN_RULES]
	//	|----------[MATCH_DAYS] = 30
	protected $clean_rules_days_key = 'MATCH_DAYS';
	//[CLEAN_RULES]
	//	|----------[MATCH_DIR] = false
	protected $clean_rules_dir_key = 'MATCH_DIR';
	//[CLEAN_RULES]
	//	|----------[MATCH_FILES] = true
	protected $clean_rules_files_key = 'MATCH_FILES';

	protected $directory_lock_file = '.clean_in_progress';

	public function __construct( $configs )
	{
		$this->__init( $configs );
	}

	/*
	* Load Object by Initializing
	*/
	public function __init( $configs )
	{
		foreach( $configs as $key => $val )
		{
			//Checking for the details that are needed
			if( $key == $this->log_dir_key )
			{
				if( !is_dir( $val ) || !is_readable( $val ) )
				{
					trigger_error( 'Directory cannot be read or does not exist -> ' . $val, E_USER_WARNING );
					continue;
				}
				$this->clean_directory = $val;

			}elseif( $key == $this->clean_rules_key )
			{
				if( empty( $val ) || !is_array( $val ) )
				{
					trigger_error( 'Expecting ARRAY for clean rules', E_USER_WARNING );
					continue;
				}
			}
			

			//Store key => val
			$this->configs[$key] = $val;
		}
	}

	public function set_log_directory( $dir )
	{
		if( !is_dir( $dir ) || !is_readable( $dir ) )
		{
			error_log( 'Directory cannot be read or does not exist -> ' . $dir );
			return false;
		}
		$this->configs[$this->log_dir_key] = $dir;
	}

	public function set_clean_rules( $rules )
	{
		if( empty( $rules ) || !is_array( $rules ) )
		{
			error_log( 'Expecting ARRAY for clean rules' );
			return false;
		}
		$this->configs[] = $rules;
	}

	/*
	* Perform Clean Operation
	* Based on Configurations
	*/
	public function clean()
	{
		if( is_dir( $this->configs[$this->log_dir_key] ) )
		{
			//Let's attempt to clean the f(p?)ile of sh...t			
			chdir( $this->configs[$this->log_dir_key] );

			$locked_status = file_exists() ? true : false; 

			if( $locked_status == false )
			{
				//lock it.
				file_put_contents( $this->directory_lock_file, '1' );

			}

		}else
		{
			error_log( '' . $this->configs[$this->log_dir_key] );
			return false;
		}
	}	


	protected function traverse_clean_files( $directory )
	{
		if( is_dir( $directory ) )
		{
			//GLOB_MARK returns directories with an added slash
			$files_to_delete = glob( $directory . '*', GLOB_MARK );
			foreach( $files_to_delete as $file )
			{
				//remove each of the files/directory
				$this->traveerse_clean_files( $file );
			}
			rmdir( $directory ); //remove the directory now

		}elseif( is_file( $directory) )
		{
			unlink( $directory ); //remove the file
		}else
		{
			error_log(); //an error occurred
		}
	}

	protected function recursive_clean_files( $directory )
	{
		if( is_dir( $directory ) )
		{
			$dfh = opendir( $directory );
			if( !$dfh )
			{
				error_log( "Could not open directory $directory!" );
				return false;
			}

			while( $file = readdir( $dfh ) )
			{
				if( $file == "." || $file == ".." )
				{
					continue;
				}
				if( is_dir( $directory . DIRECTORY_SEPARATOR . $file ) )
				{
					$this->recursive_clean_files( $directory . DIRECTORY_SEPARATOR . $file );

				}elseif( is_file( $file ) )
				{
					unlink( $file );
				}
			}
		
			closedir( $dfh );
			rmdir( $directory );

			return true;

		}elseif( is_file( $directory ) )
		{
			unlink( $directory );
			return true;
		}else
		{
			error_log( "Cannot Cleanup $directory is not a file or folder!" );
			return false;
		}

	}

}

?>
