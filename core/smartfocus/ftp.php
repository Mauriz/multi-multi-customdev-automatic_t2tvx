<?php namespace SmartFocus;

/**
 * Ftp class
 *
 * @dependencies: Constants: EXCEPTION_CODE_ERROR (bootstrap.php)
 */
class Ftp
{
	const DEFAULT_PORT 				= '21';
	const DEFAULT_TIMEOUT 			= 90; // seconds
	const DEFAULT_MAX_ATTEMPTS		= 3;
	const DEFAULT_OPERATION_DELAY	= 1; // seconds	

	/**
	 * Authenticated
	 *
	 * @var bool
	 */
	protected $authenticated;

	/**
	 * Authentication attempts
	 *
	 * @var integer
	 */
	protected $authenticationAttempts;

	/**
	 * Connection
	 *
	 * @var resource
	 */
	public $connection;

	/**
	 * Max attempts
	 *
	 * @var integer
	 */
	protected $maxAttempts;

	/**
	 * Operation delay
	 *
	 * @var integer
	 */
	protected $operationDelay;

	/**
	 * Password
	 *
	 * @var string
	 */
	protected $password;

	/**
	 * Port
	 *
	 * @var string
	 */
	protected $port;

	/**
	 * Root
	 *
	 * @var string
	 */
	protected $root;

	/**
	 * Server
	 *
	 * @var string
	 */
	protected $server;

	/**
	 * SSL flag
	 *
	 * @var boolean
	 */
	protected $ssl;

	/**
	 * Username
	 *
	 * @var string
	 */
	protected $username;

	/**
	 * Class constructor
	 *
	 * @param array $config
	 */
	public function __construct($server, $username, $password, $root = NULL, $maxAttempts = NULL, $ssl = FALSE, $port = NULL, $operationDelay = NULL, $passiveMode = FALSE)
	{
        $this->authenticated	= FALSE;
		$this->connection 		= NULL;
		$this->maxAttempts 		= is_null($maxAttempts) ? self::DEFAULT_MAX_ATTEMPTS : max(1, $maxAttempts);
		$this->operationDelay	= is_null($operationDelay) ? self::DEFAULT_OPERATION_DELAY : $operationDelay;

		$this->server			= $server;
		$this->username 		= $username;
		$this->password 		= $password;
		$this->root 			= empty($root) ? '/' : $root;
		$this->ssl 				= $ssl;
		$this->port				= empty($port) ? self::DEFAULT_PORT : $port;
		$this->passiveMode		= $passiveMode;
	}

	/**
	 * Close ftp connection
	 *
	 * @return bool
	 */
	public function close()
	{

        \SmartFocus\Util::log('---->Disconnect from the ftp server: ' . $this->server . '.');

		if (empty($this->connection) || !$this->authenticated) {
            \SmartFocus\Util::log('<----Already disconnected from the ftp server: ' . $this->server . '.');
			return TRUE;
		}

        @ftp_close($this->connection);

        $this->connection 		= NULL;
        $this->authenticated 	= FALSE;

        \SmartFocus\Util::log('<----Successfully disconnected from the ftp server: ' . $this->server . '.');

		return TRUE;

	}

	/**
	 * Connect method
	 *
	 * @param bool $reconnect [optional][default=FALSE]
	 * @return bool or throw \Exception
	 */
	public function connect($reconnect = FALSE)
	{

		if (!empty($this->connection) && $this->authenticated) {
            \SmartFocus\Util::log('<----Already authenticated with the ftp server ' . $this->server . '.');
			return TRUE;
		}

		// -----===== START Connect to the ftp server =====-----
        \SmartFocus\Util::log('---->' . ($reconnect ? 'Reconnect' : 'Connect') . ' to the ftp server ' . $this->server . '.');

        $connectionAttempts = 0;

		while (empty($this->connection) && $connectionAttempts < $this->maxAttempts) {

        	$connectionAttempts++;

			if ($connectionAttempts > 1) {
				\SmartFocus\Util::log('---->Retry to connect to the ftp server (attempt: ' . $connectionAttempts . ').');
	            sleep($this->operationDelay);
			}

			if ($this->ssl) {
                $this->connection = @\ftp_ssl_connect($this->server, $this->port);
			}
			else {
    			$this->connection = @\ftp_connect($this->server, $this->port);
			}

			if (empty($this->connection)) {
	            \SmartFocus\Util::log('<----Failed to connect to the ftp server (attempt: ' . $connectionAttempts . ').');
			}

		}

		// Failed to connect to the ftp server. Exiting script with error.
		if (empty($this->connection)) {
	        throw new \Exception('Failed to connect to ftp server (attempts: ' . $connectionAttempts . ').', EXCEPTION_CODE_FTP_ERROR);
		}

        \SmartFocus\Util::log('<----Successfully connected to the ftp server (attempts: ' . $connectionAttempts . ').');
        // -----===== END Connect to the ftp server =====-----

		// -----===== START Authenticare with the ftp server =====-----
        \SmartFocus\Util::log('---->Authenticate with the ftp server: ' . $this->server . '.');

        $authenticationAttempts	= 0;

		while (!$this->authenticated && $authenticationAttempts < $this->maxAttempts) {

	        $authenticationAttempts++;

			if ($authenticationAttempts > 1) {
				\SmartFocus\Util::log('---->Retry to authenticate to the ftp server (attempt: ' . $authenticationAttempts . ').');
				sleep($this->operationDelay);
			}

	        $this->authenticated = @ftp_login($this->connection, $this->username, $this->password);
	        
	        if ($this->passiveMode) {
	        	@ftp_pasv($this->connection, TRUE);
			}	        

			if (!$this->authenticated) {
	            \SmartFocus\Util::log('<----Failed to authenticate to the ftp server (attempt: ' . $authenticationAttempts . ').');
			}

		}

        // Failed to authenticate with the ftp server. Exiting script with error.
		if (!$this->authenticated) {
			ftp_close($this->connection);
            $this->connection = NULL;
	        throw new \Exception('Failed to authenticate to ftp server (attempts: ' . $authenticationAttempts . ').', EXCEPTION_CODE_FTP_ERROR);
		}

        \SmartFocus\Util::log('<----Successfully authenticated to the ftp server (attempts: ' . $authenticationAttempts . ').');
        // -----===== END Authenticare with the ftp server =====-----

		return TRUE;

	}

	/**
	 * Delete file method
	 *
	 * @param string $file
	 * @return boolean or throw \Exception
	 */
    public function deleteFile($file)
	{

		$deleted		= FALSE;
		$deleteAttempts	= 0;

		// Reconnect if necessary.
		if (empty($this->connection) || !$this->authenticated) {
			$this->connect(true);
		}

		\SmartFocus\Util::log('---->Delete the file ' . $file . '.');

		while (!$deleted && $deleteAttempts < $this->maxAttempts) {

	        $deleteAttempts++;

			if ($deleteAttempts > 1) {
				\SmartFocus\Util::log('---->Retrying to delete the file ' . $file . ' (attempt: ' . $deleteAttempts . ').');
				sleep($this->operationDelay);
			}

	        $deleted = @ftp_delete($this->connection, $file);

			if (!$deleted) {
	            \SmartFocus\Util::log('<----Failed to delete the file ' . $file . ' (attempt: ' . $deleteAttempts . ').');
			}

		}

		if (!$deleted) {
            throw new \Exception('Failed to delete the file: ' . $file . ' (attempts: ' . $deleteAttempts . ').', EXCEPTION_CODE_FTP_ERROR);
		}

        \SmartFocus\Util::log('<----Successfully deleted the file  ' . $file . '.');

		return TRUE;

	}

	/**
	 * Download file method
	 *
	 * @param string $sourceFile
	 * @param string $destinationFile
	 * @return boolean or throw \Exception
	 */
    public function downloadFile($sourceFile, $destinationFile)
	{

		$downloaded			= FALSE;
		$downloadAttempts	= 0;

		// Reconnect if necessary.
		if (empty($this->connection) || !$this->authenticated) {
			$this->connect(true);
		}

		\SmartFocus\Util::log('---->Download the file  ' . $sourceFile . ' to the destination file ' . $destinationFile . '.');

		while (!$downloaded && $downloadAttempts < $this->maxAttempts) {

	        $downloadAttempts++;

			if ($downloadAttempts > 1) {
				\SmartFocus\Util::log('---->Retrying to download the file ' . $sourceFile . ' (attempt: ' . $downloadAttempts . ').');
              	sleep($this->operationDelay);
			}

	        $downloaded = @ftp_get($this->connection, $destinationFile, $sourceFile, FTP_BINARY);

			if (!$downloaded) {
	            \SmartFocus\Util::log('<----Failed to download the file ' . $sourceFile . ' (attempt: ' . $downloadAttempts . ').');
			}

		}

		if (!$downloaded) {
            throw new \Exception('Failed to download the file: ' . $sourceFile . ' (attempts: ' . $downloadAttempts . ').', EXCEPTION_CODE_FTP_ERROR);
		}

        \SmartFocus\Util::log('<----Successfully downloaded the file: ' . $destinationFile . '.');

		return TRUE;

	}

	/**
	 * Rename file method
	 *
	 * @param string $sourceFile
	 * @param string $destinationFile
	 * @return boolean or throw \Exception
	 */
    public function renameFile($sourceFile, $destinationFile)
	{

		$renamed		= FALSE;
		$renameAttempts	= 0;

		// Reconnect if necessary.
		if (empty($this->connection) || !$this->authenticated) {
			$this->connect(true);
		}

		\SmartFocus\Util::log('---->Rename the file  ' . $sourceFile . ' to ' . $destinationFile . '.');

		while (!$renamed && $renameAttempts < $this->maxAttempts) {

	        $renameAttempts++;

			if ($renameAttempts > 1) {
				\SmartFocus\Util::log('---->Retrying to rename the file ' . $sourceFile . ' to ' . $destinationFile . ' (attempt: ' . $renameAttempts . ').');
				sleep($this->operationDelay);
			}

	        $renamed = @ftp_rename($this->connection, $sourceFile, $destinationFile);

			if (!$renamed) {
	            \SmartFocus\Util::log('<----Failed to rename the file ' . $sourceFile . ' to ' . $destinationFile . ' (attempt: ' . $renameAttempts . ').');
			}

		}

		if (!$renamed) {
            throw new \Exception('Failed to rename the file: ' . $sourceFile . ' to ' . $destinationFile . ' (attempts: ' . $renameAttempts . ').', EXCEPTION_CODE_FTP_ERROR);
		}

        \SmartFocus\Util::log('<----Successfully renamed the file  ' . $sourceFile . ' to ' . $destinationFile . '.');

		return TRUE;

	}
	
	/**
	 * Searches file
	 *
	 * @param string $fileNamePattern
	 * @param string $folder [optional] 	 
	 * @return array or throw \Exception
	 */	
	public function searchFile($fileNamePattern, $folder = NULL)
	{
	
		// Reconnect if necessary.
		if (empty($this->connection) || !$this->authenticated) {
			$this->connect(TRUE);
		}
		
		if (empty($folder)) {
			\SmartFocus\Util::log('---->Scan the ftp server for new campaign folders within the root folder ' . $this->root . '.');
			$folder = $this->root;			
		}
		else {

			\SmartFocus\Util::log('---->Scan the ftp server for new campaign folders within the folder ' . $folder . '.');
			
			// Check if folder exists.
			if (!in_array($folder, ftp_nlist($this->connection, $this->root))) {
				$this->throwException('Could not find the folder ' . $folder . '.', EXCEPTION_CODE_WARNING);
			}
	
			// Set working folder.
			if (!@ftp_chdir($this->connection, $folder)) {
	            $this->throwException('Failed to set the working folder ' . $folder . '.', EXCEPTION_CODE_FTP_ERROR, TRUE);
			}
			
		}
		
		// Scan for items.
		$itemList = (array) ftp_nlist($this->connection, $folder);
		if (empty($itemList)) {
            $this->throwException('Empty folder ' . $folder . '.', EXCEPTION_CODE_WARNING);
		}
		
		$rawList = array();
		
		// Scan for daily file.
		foreach ($itemList as $itemName) {		
			if (strpos(strtolower($itemName), strtolower($fileNamePattern)) !== FALSE) {
				$rawList[] = array('name' => $itemName, 'path' => $folder . '/' . $itemName,); 
			}		
		}
		
		if (empty($rawList)) {
			$this->throwException('Could not find daily file ' . $fileNamePattern . '.', EXCEPTION_CODE_WARNING);
		}
		
		return $rawList;
	
	}
	
	/**
	 * Throws an exception
	 *
	 * @param string $message
	 * @param int $code [optional]
	 * @param bool $disconnect [optional][default=TRUE]
	 * @return throw \Exception
	 */
	public function throwException($message, $code = NULL, $disconnect = TRUE) {

		\SmartFocus\Util::log('<----' . $message);
		if ($disconnect) {
        	$this->close();
		}
		throw new \Exception($message, $code);

	}	 

	/**
	 * Upload file method
	 *
	 * @param string $sourceFile
	 * @param string $destinationFile
	 * @return boolean or throw \Exception
	 */
    public function uploadFile($sourceFile, $destinationFile)
	{

		$uploaded		= FALSE;
		$uploadAttempts	= 0;

		// Reconnect if necessary.
		if (empty($this->connection) || !$this->authenticated) {
			$this->connect(true);
		}

		\SmartFocus\Util::log('---->Upload the file  ' . $sourceFile . ' to the destination file ' . $destinationFile . '.');

		while (!$uploaded && $uploadAttempts < $this->maxAttempts) {

	        $uploadAttempts++;

			if ($uploadAttempts > 1) {
				\SmartFocus\Util::log('---->Retrying to upload the file ' . $sourceFile . ' (attempt: ' . $uploadAttempts . ').');
              	sleep($this->operationDelay);
			}

	        $uploaded = @ftp_put($this->connection, $destinationFile, $sourceFile, FTP_BINARY);

			if (!$uploaded) {
	            \SmartFocus\Util::log('<----Failed to upload the file ' . $sourceFile . ' (attempt: ' . $uploadAttempts . ').');
			}

		}

		if (!$uploaded) {
            throw new \Exception('Failed to upload the file: ' . $sourceFile . ' (attempts: ' . $uploadAttempts . ').', EXCEPTION_CODE_FTP_ERROR);
		}

        \SmartFocus\Util::log('<----Successfully uploaded the file  ' . $destinationFile . '.');

		return TRUE;

	}

}

?>