<?php namespace SmartFocus;

/**
 * Utility class
 */
class Util {

	/**
	 * Config array
	 *
	 * @var array
	 */
	protected static $config;

	/**
	 * Log object
	 *
	 * @var \SmartFocus\Log
	 */
	public static $log;

	/**
	 * Class constructor
	 *
	 * @param array $config
	 */
	public function __construct($config)
	{
		self::$config = $config;
	}

	/**
	 * Exception code to log code transformation
	 *
	 * @param int $exceptionCode
	 * @return string
	 */
	public static function exceptionCodeToLogCode($exceptionCode)
	{
		switch ($exceptionCode) {

			case EXCEPTION_CODE_FTP_ERROR:
	        	$logCode = LOG_CODE_FTP_ERROR;
				break;
			case EXCEPTION_CODE_SFTP_ERROR:
	        	$logCode = LOG_CODE_SFTP_ERROR;
				break;
			case EXCEPTION_CODE_API_ERROR:
	        	$logCode = LOG_CODE_API_ERROR;
				break;
			case EXCEPTION_CODE_NMP_ERROR:
	        	$logCode = LOG_CODE_NMP_ERROR;
				break;
			case EXCEPTION_CODE_ERROR:
		    	$logCode = LOG_CODE_ERROR;
				break;
			case EXCEPTION_CODE_ABORD:
		    	$logCode = LOG_CODE_ABORD;
				break;
			case EXCEPTION_CODE_SUCCESS:
		    	$logCode = LOG_CODE_SUCCESS;
				break;
			case EXCEPTION_CODE_WARNING:
		    	$logCode = LOG_CODE_WARNING;
				break;
			case EXCEPTION_CODE_NOTICE:
		    	$logCode = LOG_CODE_NOTICE;
				break;				
			default:
		    	$logCode = LOG_CODE_ERROR;
				break;

		}

		return $logCode;
	}

	/**
	 * Modify nmp subject in the nmp parameters array
	 *
	 * @param array $nmpParameters
	 * @param integer $exceptionCode
	 * @return array
	 */
	public static function exceptionCodeToNmpSubject($nmpParameters, $exceptionCode)
	{

        $logCode = self::exceptionCodeToLogCode($exceptionCode);

		if (array_key_exists($logCode, self::$config['nmp']['subjects'])) {
	        $nmpParameters['subject'] = self::$config['nmp']['subjects'][$logCode];
		}

		return $nmpParameters;

	}

	/**
	 * Exception to log message method
	 *
	 * @param \Exception $exception
	 * @return string
	 */
	public static function exceptionToLogMessage($exception)
	{

  		if (!($exception instanceof \Exception)) {
			return NULL;
		}

  		if (!($exception instanceof \SoapFault)) {
			return $exception->getMessage();
		}

        $message = $exception->getMessage();

		if (!property_exists($exception, 'detail')) {
			return $message;
		}

		if ( // CcmdServiceException
			property_exists($exception->detail, 'CcmdServiceException')
			&& property_exists($exception->detail->CcmdServiceException, 'description')
			&& property_exists($exception->detail->CcmdServiceException, 'status')            
		) {
			if (property_exists($exception->detail->CcmdServiceException, 'fields')) {
				$message .= ' ' . $exception->detail->CcmdServiceException->description . ' (value: ' . $exception->detail->CcmdServiceException->fields . ', status: ' . $exception->detail->CcmdServiceException->status . ')';
			}
			else {
				$message .= ' ' . $exception->detail->CcmdServiceException->description . ' (status: ' . $exception->detail->CcmdServiceException->status . ')';
			}		        
		}
		elseif ( // ConnectionServiceException
			property_exists($exception->detail, 'ConnectionServiceException')
			&& property_exists($exception->detail->ConnectionServiceException, 'description')
			&& property_exists($exception->detail->ConnectionServiceException, 'status')
		) {
        	$message .= ' ' . $exception->detail->ConnectionServiceException->description . ' (status: ' . $exception->detail->ConnectionServiceException->status . ')';
		}

		return $message;

	}

	/**
	 * Checks if a string is UTF8 enconded
	 *
	 * @param string $string
	 * @return string
	 */
    public static function isUTF8($string)
	{
        return preg_match('%(?:
        [\xC2-\xDF][\x80-\xBF]              # non-overlong 2-byte
        |\xE0[\xA0-\xBF][\x80-\xBF]         # excluding overlongs
        |[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}  # straight 3-byte
        |\xED[\x80-\x9F][\x80-\xBF]         # excluding surrogates
        |\xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
        |[\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        |\xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
        )+%xs', $string);
    }

	/**
	 * Log method
	 *
	 * @param string $message
	 * @param bool $resetBuffer [optional][default=FALSE]
  	 * @param bool $wrapMessage [optional][default=TRUE]
     * @param string $fileNamePrefix [optional]
	 * @param int $frequency [optional]
	 * @param int $frequencyRate [optional]
	 * @return bool
  	 */
	public static function log($message, $resetBuffer = FALSE, $wrapMessage = TRUE, $fileNamePrefix = NULL, $frequency = NULL, $frequencyRate = NULL)
	{

		if (empty(self::$log)) {

			if (empty($frequency)) {
	            $frequency = \SmartFocus\Log::FREQUENCY_DAY;
			}

			if (empty($frequencyRate)) {
	            $frequencyRate = 1;
			}

            self::$log = new \SmartFocus\Log(self::$config['logFolder'], self::$config['timeZone'], $frequency, $frequencyRate);

		}

        return self::$log->write($message, $frequency, $frequencyRate, $fileNamePrefix, $wrapMessage, $resetBuffer);

	}

	/**
	 * Notify method
	 *
	 * @param array $nmpParameters
	 * @param string $message
	 * @return bool or throw \Exception
	 */
	public static function notify($nmpParameters, $message) {

		if (
			empty($nmpParameters['encrypt'])
			|| empty($nmpParameters['id'])
			|| empty($nmpParameters['random'])
		) {
        	self::log('[' . LOG_CODE_NMP_ERROR . ']' . ERROR_MESSAGE_NMP_CONFIG);
			return FALSE;
		}

		if (
			empty($nmpParameters['recipients'])
		) {
         	self::log('[' . LOG_CODE_NMP_ERROR . ']' . ERROR_MESSAGE_NMP_RECIPIENTS);
			return FALSE;
		}

		if (!is_array($nmpParameters['recipients'])) {
            $nmpParameters['recipients'] = array($nmpParameters['recipients']);
		}

  		$nmpConfig	= self::$config['nmp'];
		$request 	= array();

		foreach ($nmpParameters['recipients'] as $recepient) {

            $request[] = \SmartFocus\API\NotificationService::buildSendObject(
				$recepient,
				$nmpParameters['encrypt'],
				$nmpParameters['id'],
				$nmpParameters['random'],
				'2012-01-01T01:00:00',
			    array('1' => $message),
			    array(
					'from' 		=> (empty($nmpParameters['from']) ? NULL : $nmpParameters['from']),
					'subject'	=> (empty($nmpParameters['subject']) ? NULL : (self::$config['applicationName'] . ': ' . $nmpParameters['subject'])),
				),
				'NOTHING',
				'EMAIL'
			);

		}

		try {
            self::log('---->Send notification to ' . implode(', ', $nmpParameters['recipients']));
			$notificationService = new \SmartFocus\API\NotificationService();
            $notificationService->sendObjects($request);
		}
		catch (\Exception $exception) {
        	self::log('[' . LOG_CODE_NMP_ERROR . ']' . sprintf(ERROR_MESSAGE_NMP_SEND, $exception->getMessage()));
			trigger_error($exception->getMessage(), E_USER_NOTICE);
   			return FALSE;
		}

        self::log('<----Successfully sent notification to ' . implode(', ', $nmpParameters['recipients']));

		return TRUE;

	}

	/**
	 * Set send date
	 *
	 * @param string $date
	 * @param string $timeZone [optional][default=UTC]
	 * @param int $minutes [optional][default=15]
	 * @return string
	 */
	public static function setSendDate($date, $timeZone = 'UTC', $minutes = 15)
	{

		$minutes        = intval($minutes);
		$dateTimeNow	= new \DateTime('now', new \DateTimeZone($timeZone));
	    $dateTime		= new \DateTime($date, new \DateTimeZone($timeZone));
		$dateTimeNow->modify(sprintf('%+d', $minutes) . ' minutes');
		if ($dateTime < $dateTimeNow) {
	        $date = $dateTimeNow->format('c');
		}

		return $date;

	}

	/**
	 * Trigger error method
	 *
	 * @param string $errorMessage
	 * @param string $errorCode [optional]
	 * @param array $nmpParameters [optional]
	 * @param string $action [optional]
	 * @param bool $exit [optional][default=FALSE]
	 * @return bool|array
	 */
	public static function triggerError($errorMessage, $errorCode = NULL, $nmpParameters = array(), $action = NULL, $exit = FALSE)
	{

		if (!empty($errorCode)) {
			$errorMessage = '[' . $errorCode . ']' . $errorMessage;
		}

		self::log($errorMessage);

		$buffer = self::$log->getBuffer();

		if (!empty($action)) {
            $buffer[] = self::$log->wrapMessage('[END]' . $action);
		}

		if (!empty($nmpParameters)) {
            self::notify($nmpParameters, implode(PHP_EOL, $buffer));
		}

		if (!empty($action)) {
            self::log('[END]' . $action);
		}

		if ($exit) {
            exit($errorCode);
		}

		return empty($nmpParameters) ? TRUE : $buffer;

	}

}

?>