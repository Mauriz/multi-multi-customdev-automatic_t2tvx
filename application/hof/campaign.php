<?php
require 'bootstrap.php';

$dateTime	= new \DateTime((empty($_GET['date']) ? 'now' : trim($_GET['date'])), new \DateTimeZone($config['timeZone']));
$fileName			= sprintf($config['filePattern'], $dateTime->format('Ymd'));
$fileNameTreated	= sprintf($config['fileTreatedPattern'], $dateTime->format('YmdHis'));
$file 		= array(
	'name' 				=> $fileName,
	'localFolder' 		=> $config['uploadFolder'],
	'localPath' 		=> $config['uploadFolder'] . '/' . $fileName,
	'remoteFolder'		=> $config['ftp']['folder'],
	'remotePath' 		=> $config['ftp']['folder'] . '/' . $fileName,
	'remoteTreatedPath'	=> $config['ftp']['folder'] . '/' . $fileNameTreated,
);

\SmartFocus\Util::log('[START]' . ACTION_SCRIPT);

// -----====================-----
// START FILE DONWLOAD
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_FILE_DOWNLOAD);
try {
	if (!is_dir($file['localFolder']) && !is_writable($file['localFolder'])) {
	    throw new \Exception(sprintf(ERROR_MESSAGE_DOWNLOAD_FOLDER_INVALID, $file['localFolder']), EXCEPTION_CODE_ERROR);
	}
	$ftp = new \SmartFocus\Ftp($config['ftp']['server'], $config['ftp']['username'], $config['ftp']['password']);
	$ftp->connect();
	$ftp->downloadFile($file['remotePath'], $file['localPath']);
	if (!is_readable($file['localPath'])) {
		throw new \Exception(sprintf(ERROR_MESSAGE_FILE_DOWNLOAD, $file['remotePath']), EXCEPTION_CODE_ERROR);	
	}
	$ftp->renameFile($file['remotePath'], $file['remoteTreatedPath']);
	$ftp->close();	
}
catch (\Exception $exception) {
    \SmartFocus\Util::triggerError(
		$exception->getMessage(),
		\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
		\SmartFocus\Util::exceptionCodeToNmpSubject(\SmartFocus\Hof\Util::getNmpParameters(), $exception->getCode()),
		ACTION_SCRIPT,
  		TRUE
	);
}
\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_FILE_DOWNLOAD, $file['localPath']));
// -----====================-----
// END FILE DOWNLOAD
// -----====================-----

// -----====================-----
// START API INIT / GET MESSAGE / GET BANNER  
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_API_INIT);
try {
	$partnerAuth 					= new \SmartFocus\API\PartnerPrivilegedAuthenticationService($config['api']['partner_users']['A'], $config['api']['partner_key1'], $config['api']['partner_key2']);
	$batchMemberService 			= new \SmartFocus\API\BatchMemberService($partnerAuth);
	$campaignManagementService  	= new \SmartFocus\API\CampaignManagementService($partnerAuth);
	$externalSegmentationService 	= new \SmartFocus\API\ExtSegmentService($partnerAuth);
	
	\SmartFocus\Util::log('---->' . sprintf(ACTION_GET_MESSAGE, $config['messageId']));	
	$message 	= (array) $campaignManagementService->getMessage($config['messageId']);	
	if (empty($message['body'])) {	
		throw new \Exception(sprintf(ERROR_MESSAGE_TEMPLATE_CONTENT_INVALID, $config['messageId']), EXCEPTION_CODE_ERROR);
	}
	
	\SmartFocus\Util::log('---->' . sprintf(ACTION_GET_BANNER, $config['bannerProductsId']));
	$bannerProducts 	= (array) $campaignManagementService->getBanner($config['bannerProductsId']);
	if (empty($bannerProducts['content'])) {
		throw new \Exception(sprintf(ERROR_MESSAGE_PRODUCTS_TEMPLATE_CONTENT_INVALID, $config['bannerProductsId']), EXCEPTION_CODE_ERROR);
	}
	
	\SmartFocus\Util::log('---->' . sprintf(ACTION_GET_BANNER, $config['bannerImageId']));
	$bannerImage 	= (array) $campaignManagementService->getBanner($config['bannerImageId']);
	if (empty($bannerImage['content'])) {
		throw new \Exception(sprintf(ERROR_MESSAGE_IMAGE_TEMPLATE_CONTENT_INVALID, $config['bannerImageId']), EXCEPTION_CODE_ERROR);
	}	
}
catch (\Exception $exception) {
	\SmartFocus\Util::triggerError(
		$exception->getMessage(),
		\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
  		\SmartFocus\Util::exceptionCodeToNmpSubject(\SmartFocus\Hof\Util::getNmpParameters(), $exception->getCode()),
		ACTION_SCRIPT,
		TRUE
	);
}
// -----====================-----
// END API INIT / GET MESSAGE / GET BANNER
// -----====================-----

// -----====================-----
// START FILE PROCESS / GET CAMPAIGNS
// -----====================-----
\SmartFocus\Util::log('---->' . sprintf(ACTION_FILE_PROCESS, $file['localPath']));
$campaigns = \SmartFocus\Hof\Util::processDownloadedFile($file, $bannerProducts['content'], $bannerImage['content']);
// -----====================-----
// END FILE PROCESS / GET CAMPAIGNS
// -----====================-----

// -----====================-----
// START CREATE CAMPAIGNS 
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_CREATE_CAMPAIGNS);
$campaigns = \SmartFocus\Hof\Util::createCampaigns($campaignManagementService, $externalSegmentationService, $campaigns, $message);
// -----====================-----
// END CREATE CAMPAIGNS 
// -----====================-----

$report = \SmartFocus\Util::$log->getHistory();
$report[] = '[END]' . ACTION_SCRIPT;

\SmartFocus\Util::notify(\SmartFocus\Hof\Util::getNmpParameters(), implode(PHP_EOL, $report));

\SmartFocus\Util::log('[END]' . ACTION_SCRIPT);

exit(LOG_CODE_SUCCESS);
?>