<?php namespace SmartFocus\Hof;

/**
 * Utility class
 */
class Util extends \SmartFocus\Util 
{

	/**
	 * Create campaigns method
	 * 	 
	 * @param CampaignManagementService $campaignManagementService
	 * @param ExtSegmentService $externalSegmentationService 	 
	 * @param array $campaigns
	 * @param array $message 	 	  	  	 
	 * @return array 	 
	 */	
	public static function createCampaigns($campaignManagementService, $externalSegmentationService, $campaigns, $message)
	{
	
		foreach ($campaigns as $index => &$campaign) {

			$date = new \DateTime();
			$date->modify('+2 hour');
						 			
			// Prepare message
			$campaign['messageBody'] 	= $message['body']; 
			
			$campaign['messageBody'] 	= str_replace(sprintf(self::$config['bannerReplacePattern'], self::$config['bannerImageId']), utf8_encode($campaign['imageTemplate']), $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace(sprintf(self::$config['bannerReplacePattern'], self::$config['bannerProductsId']), utf8_encode($campaign['productsTemplate']), $campaign['messageBody']);
			
			$campaign['messageBody'] 	= str_replace('{PREVIEW_LINE}', utf8_encode($campaign['PREVIEW_LINE']), $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{HEADLINE}', utf8_encode($campaign['HEADLINE']), $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{MAIN_PARAGRAPH}', utf8_encode($campaign['MAIN_PARAGRAPH']), $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{BUTTON_TEXT}', utf8_encode($campaign['BUTTON_TEXT']), $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{BUTTON_URL}', $campaign['BUTTON_URL'], $campaign['messageBody']);
			
			$campaign['messageBody'] 	= str_replace('{MAIN_IMG_URL}', $campaign['MAIN_IMG_URL'], $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{MAIN_IMG_LINK}', $campaign['MAIN_IMG_LINK'], $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{MAIN_IMG_ALT}', $campaign['MAIN_IMG_ALT'], $campaign['messageBody']);
			
			$campaign['messageBody'] 	= str_replace('{BOX1_IMG_URL}', $campaign['BOX1_IMG_URL'], $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{BOX1_IMG_ALT}', utf8_encode($campaign['BOX1_IMG_ALT']), $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{BOX1_LINK}', $campaign['BOX1_LINK'], $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{BOX1_TEXT}', utf8_encode($campaign['BOX1_TEXT']), $campaign['messageBody']);
			
			$campaign['messageBody'] 	= str_replace('{BOX2_IMG_URL}', $campaign['BOX2_IMG_URL'], $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{BOX2_IMG_ALT}', utf8_encode($campaign['BOX2_IMG_ALT']), $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{BOX2_LINK}', $campaign['BOX2_LINK'], $campaign['messageBody']);
			$campaign['messageBody'] 	= str_replace('{BOX2_TEXT}', utf8_encode($campaign['BOX2_TEXT']), $campaign['messageBody']);
			
			if (preg_match_all('/href=\"(.*?)\"/', $campaign['messageBody'], $matches, PREG_SET_ORDER)) {								
				foreach ($matches as $index => $match) {
					if (count($match == 2)) {					
						if (filter_var($match[1], FILTER_VALIDATE_URL)) {						
							$url = $match[1] . (filter_var($match[1], FILTER_VALIDATE_URL, FILTER_FLAG_QUERY_REQUIRED) ? '&' : '?');
							$url .= sprintf(self::$config['trackingPattern'], 
										$campaign['TRACKING_TSID'],
										$campaign['TRACKING_KW'],
										$campaign['TRACKING_CGN'],
										$campaign['TRACKING_CM_PLA'],
										$campaign['TRACKING_KW']
									); 							
							$url = 'href="' . $url . '"';
							$campaign['messageBody'] = str_replace($match[0], $url, $campaign['messageBody']);							  
						}
					}						
				}
			}
			
			$campaign['message'] 		= array(
				'name' 			=> $campaign['CAMPAIGN_NAME'] . ' ' . $date->format('Y-m-d'),
				'description'	=> $campaign['CAMPAIGN_NAME'] . ' ' . $date->format('Y-m-d'),
				'subject' 		=> $campaign['SUBJECT_LINE'],
				'from'			=> $message['from'],
				'fromEmail'		=> $message['fromEmail'],
				'replyTo' 		=> $message['replyTo'],
				'replyToEmail' 	=> $message['replyToEmail'],
				'body' 			=> $campaign['messageBody'],
			);
			
			try {
						
				if (empty(self::$config['segmentIdList'][$campaign['CAMPAIGN_INDEX']])) {
					throw new \Exception(sprintf(ERROR_MESSAGE_SEGMENT_ID_EMPTY, $campaign['CAMPAIGN_INDEX']), EXCEPTION_CODE_WARNING);	
				}				
				$campaign['segmentId'] 	= self::$config['segmentIdList'][$campaign['CAMPAIGN_INDEX']];
				
				// Check segment
				\SmartFocus\Util::log('---->' . sprintf(ACTION_CHECK_SEGMENT, $campaign['CAMPAIGN_INDEX'], $campaign['segmentId']));																
				$campaign['segment'] 	= (array) $externalSegmentationService->extSegmentationGet($campaign['segmentId']);
				if (empty($campaign['segmentId'])) {
					throw new \Exception(sprintf(ERROR_MESSAGE_SEGMENT_ID_INVALID, $campaign['CAMPAIGN_INDEX'], $campaign['segmentId']), EXCEPTION_CODE_WARNING);
				}
				\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_SEGMENT_CHECK, $campaign['CAMPAIGN_INDEX'], $campaign['segmentId']));				
				
				// Create message
				\SmartFocus\Util::log('---->' . sprintf(ACTION_CREATE_MESSAGE, $campaign['CAMPAIGN_INDEX'], $campaign['message']['name']));				
				$campaign['messageId'] = (int) $campaignManagementService->createEmailMessagebyObj($campaign['message']);
				if (empty($campaign['messageId'])) {
					throw new \Exception(sprintf(ERROR_MESSAGE_MESSAGE_NOT_CREATED, $campaign['CAMPAIGN_INDEX'], $campaign['message']['name']), EXCEPTION_CODE_WARNING);
				}
				
				// Create mirror link
				// $campaignManagementService->createAndAddMirrorUrl($campaign['messageId']); // Removed at the Client's request
				
			}
			catch (\Exception $exception) {				
				unset($campaigns[$index]);
				$exceptionCode 		= $exception->getCode();				
				$exceptionMessage 	= \SmartFocus\Util::exceptionToLogMessage($exception);
				
				switch ($exceptionCode) {
					case EXCEPTION_CODE_WARNING:
						\SmartFocus\Util::log('[' . \SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_WARNING) . ']' . $exceptionMessage);
						break;
					default:
					    \SmartFocus\Util::triggerError(
							$exceptionMessage,
							\SmartFocus\Util::exceptionCodeToLogCode($exceptionCode),
							\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), $exceptionCode),
							ACTION_SCRIPT,
					  		TRUE
						);					
						break;						
				}
				continue;
			}				
	
	        // Track all links
			try {
		        $campaign['trackedLinks'] = $campaignManagementService->trackAllLinks($campaign['messageId']);
			}
		  	catch (\Exception $exception) {
	   			self::log('<----' .  sprintf(LOG_MESSAGE_KO_MESSAGE_LINKS_TRACK, $campaign['message']['name'], $campaign['messageId']));
			}				
				
			\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_MESSAGE_CREATE, $campaign['CAMPAIGN_INDEX'], $campaign['messageId'], $campaign['message']['name']));
			
			try {								
				\SmartFocus\Util::log('---->' . sprintf(ACTION_CREATE_CAMPAIGN, $campaign['CAMPAIGN_INDEX'], $campaign['CAMPAIGN_NAME']));
				// Create campaign
			    $campaign['id'] = (int) $campaignManagementService->createCampaignByObj(array(
					'name' 			=> $campaign['CAMPAIGN_NAME'] . ' ' . $date->format('Y-m-d'),
					'description' 	=> $campaign['CAMPAIGN_NAME'] . ' ' . $date->format('Y-m-d'),
					'sendDate' 		=> $date->format('c'),
					'messageId' 	=> $campaign['messageId'],
					'mailinglistId'	=> self::$config['segmentIdList'][$campaign['CAMPAIGN_INDEX']],
					'emaildedupflg'	=> FALSE,
					'analytics'		=> FALSE,
				));
				unset($date);				
				if (empty($campaign['id'])) {
					throw new \Exception(sprintf(ERROR_MESSAGE_CAMPAIGN_NOT_CREATED, $campaign['CAMPAIGN_INDEX'], $campaign['CAMPAIGN_NAME']), EXCEPTION_CODE_WARNING);
				}				
				\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_CAMPAIGN_CREATE, $campaign['CAMPAIGN_INDEX'], $campaign['id'], $campaign['CAMPAIGN_NAME']));					
				
			}
			catch (\Exception $exception) {
				unset($campaigns[$index]);
				$exceptionCode 		= $exception->getCode();				
				$exceptionMessage 	= \SmartFocus\Util::exceptionToLogMessage($exception);
				
				switch ($exceptionCode) {
					case EXCEPTION_CODE_WARNING:
						\SmartFocus\Util::log('[' . \SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_WARNING) . ']' . $exceptionMessage);
						break;
					default:
					    \SmartFocus\Util::triggerError(
							$exceptionMessage,
							\SmartFocus\Util::exceptionCodeToLogCode($exceptionCode),
							\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), $exceptionCode),
							ACTION_SCRIPT,
					  		TRUE
						);					
						break;						
				}
			}			
					
		}
				
		return $campaigns;
	
	}
	
	/**
	 * Return the NMP parameters array
	 *  
	 * @return array	 	 
	 */	 	
	public static function getNmpParameters()
	{
		return array(
			'encrypt'		=> self::$config['nmp']['encrypt'],
			'id'			=> self::$config['nmp']['id'],
			'random'		=> self::$config['nmp']['random'],
			'recipients'	=> is_array(self::$config['nmp']['recepients']) ? self::$config['nmp']['recepients'] : array(self::$config['nmp']['recepients']),
			'from' 			=> self::$config['applicationName'],
			'subject' 		=> self::$config['nmp']['subjects']['NOTIFY'],
		);	
	}				
	
	/**
	 * Process downloaded file method
	 * 	 
	 * @param array $downloadedFile
	 * @param string $productsTemplate 	 
	 * @return array 	 
	 */	
	public static function processDownloadedFile($downloadedFile, $productsTemplate = NULL, $imageTemplate = NULL)
	{
		$error 				= array();			
		$campaigns			= array();
		$linesRead			= 0;
		$countColumns		= 0;
		$countCampaigns 	= 0;
		$countLineColumns	= 0;
		$fileHandle 		= NULL;
			
		if (!is_readable($downloadedFile['localPath']) || !($fileHandle = fopen($downloadedFile['localPath'], 'r'))) {
			\SmartFocus\Util::triggerError(
				sprintf(ERROR_MESSAGE_FILE_DOWNLOAD_CSV, $downloadedFile['localPath']),
				\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ERROR),
				\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), EXCEPTION_CODE_ERROR),
				ACTION_SCRIPT,
				TRUE
			);		
		}
				
		while (($line = fgetcsv($fileHandle, 8192, self::$config['fileSeparator'])) !== FALSE) {						
			
			$linesRead++;
			$countLineColumns = count($line);
			
			foreach ($line as &$value) {
				trim($value, '"');
			}
			
			if ($linesRead == 1) {
				$countColumns = $countLineColumns;
				continue;				
			}
			
			if ($countLineColumns != $countColumns) {
				\SmartFocus\Util::triggerError(
					sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $downloadedFile['localPath'], $linesRead, 'Wrong number of columns. Found '  . $countLineColumns . ' instead of ' . $countColumns),
					\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ERROR),
					\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), EXCEPTION_CODE_ERROR),
					ACTION_SCRIPT,
					TRUE
				);			
			}
			
			if ($linesRead == 2) {
				if ($line[0] == 'CAMPAIGN_NAME') {
					for ($i = 1; $i < $countColumns; $i++) {
						if (empty($line[$i])) {						
							\SmartFocus\Util::log('[' . \SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_WARNING) . ']' . sprintf(ERROR_MESSAGE_FILE_VALUE_EMPTY, 'CAMPAIGN_NAME', $downloadedFile['localPath'], $linesRead, 'Skipping campaign: ' . $i));
						}
						else {
							$campaigns[$i] = array('CAMPAIGN_NAME' => $line[$i], 'CAMPAIGN_INDEX' => $i);
						}
					}				
				}
				else {
					\SmartFocus\Util::triggerError(
						sprintf(ERROR_MESSAGE_FILE_LINE_FOUND, 'CAMPAIGN_NAME', $downloadedFile['localPath']),
						\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ERROR),
						\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), EXCEPTION_CODE_ERROR),
						ACTION_SCRIPT,
						TRUE
					);					
				}
			}
			
			for ($i = 1; $i < $countColumns; $i++) {
				if (!empty($campaigns[$i])) {
					$campaigns[$i][$line[0]] = $line[$i]; 
				}	
			}
		}
		
		if (!feof($fileHandle)) {
			\SmartFocus\Util::triggerError(
				sprintf(ERROR_MESSAGE_FILE_READ, $downloadedFile['localPath'], $linesRead),
				\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ERROR),
				\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), EXCEPTION_CODE_ERROR),
				ACTION_SCRIPT,
				TRUE
			);		
		}
		fclose($fileHandle);
		
		if (empty($productsTemplate) && (!is_readable(self::$config['productsTemplatePath']) || !($productsTemplate = file_get_contents(self::$config['productsTemplatePath'])))) {
			\SmartFocus\Util::triggerError(
				sprintf(ERROR_MESSAGE_PRODUCTS_TEMPLATE_FILE_INVALID, self::$config['productsTemplatePath']),
				\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ERROR),
				\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), EXCEPTION_CODE_ERROR),
				ACTION_SCRIPT,
				TRUE
			);		
		}
		
		if (empty($imageTemplate) && (!is_readable(self::$config['imageTemplatePath']) || !($imageTemplate = file_get_contents(self::$config['imageTemplatePath'])))) {
			\SmartFocus\Util::triggerError(
				sprintf(ERROR_MESSAGE_IMAGE_TEMPLATE_FILE_INVALID, self::$config['imageTemplatePath']),
				\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ERROR),
				\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), EXCEPTION_CODE_ERROR),
				ACTION_SCRIPT,
				TRUE
			);		
		}		
		
		foreach ($campaigns as $indexCampaign => &$campaign) {
		
			$campaign['products'] 	= array();
			$campaign['image'] 		= array(
				'MAIN_IMG_URL' 	=> $campaign['MAIN_IMG_URL'], 
				'MAIN_IMG_LINK' => $campaign['MAIN_IMG_LINK'],
				'MAIN_IMG_ALT' 	=> $campaign['MAIN_IMG_ALT'],
			);    
			$productRowCount	= 0;
			$currentRow 			= 0;
			$j 								= 0;
			
			for ($i = 1; $i <= 8; $i++) {
							
				$wrongAttributeList = array();
				
				if (empty($campaign['P' . $i . '_IMG_URL'])) {
					$wrongAttributeList[] = 'P' . $i . '_IMG_URL';
				}
				if (empty($campaign['P' . $i . '_IMG_ALT'])) {
					$wrongAttributeList[] = 'P' . $i . '_IMG_ALT';					
				}
				if (empty($campaign['P' . $i . '_LINK'])) {
					$wrongAttributeList[] = 'P' . $i . '_LINK';					
				}					
				if (empty($campaign['P' . $i . '_TITLE'])) {
					$wrongAttributeList[] = 'P' . $i . '_TITLE';									
				}								    	
				if (empty($campaign['P' . $i . '_PRICE_NOW'])) {					
					$wrongAttributeList[] = 'P' . $i . '_PRICE_NOW';					
				}
				if (!array_key_exists('P' . $i . '_PRICE_WAS', $campaign)) {
					$campaign['P' . $i . '_PRICE_WAS'] = NULL;
				}
				
				if (empty($wrongAttributeList)) {
				
					if (!empty($campaign['products'][$currentRow]) && count($campaign['products'][$currentRow]) == 4) {
						$currentRow++;
						$j = 0;
					}
									
					$product = array(
						'P' . ($j + 1) . '_IMG_URL' 	=> $campaign['P' . $i . '_IMG_URL'], 
						'P' . ($j + 1) . '_IMG_ALT' 	=> $campaign['P' . $i . '_IMG_ALT'],
						'P' . ($j + 1) . '_LINK' 		=> $campaign['P' . $i . '_LINK'],
						'P' . ($j + 1) . '_TITLE' 		=> $campaign['P' . $i . '_TITLE'],
						'P' . ($j + 1) . '_PRICE' 		=> NULL,
					);
					
					if (empty($campaign['P' . $i . '_PRICE_WAS'])) {
						$priceWas	= '&nbsp;';
						$priceNow	= $campaign['P' . $i . '_PRICE_NOW'];
						$color 		= self::$config['productPriceColorNow'];
					}
					else {
						$priceWas 	= sprintf(self::$config['productPriceWas'], $campaign['P' . $i . '_PRICE_WAS']);
						$priceNow		= sprintf(self::$config['productPriceNow'], $campaign['P' . $i . '_PRICE_NOW']);
						$color 			= self::$config['productPriceColorWas'];
					}
					
					$product['P' . ($j + 1) . '_PRICE'] = sprintf(self::$config['productPriceTemplate'], $color, $priceNow, $priceWas);
					
					$campaign['products'][$currentRow][] = $product;
					unset($product);
					
					$j++;				
				
				}
				else {				
					\SmartFocus\Util::log(sprintf(LOG_MESSAGE_KO_PRODUCT, $campaign['CAMPAIGN_INDEX'], $i, implode(', ', $wrongAttributeList)));				
				}
			
			}
			
			if (empty($campaign['products'])) {
				\SmartFocus\Util::log('[' . \SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_WARNING) . ']' . sprintf(ERROR_MESSAGE_PRODUCTS_EMPTY, $campaign['CAMPAIGN_INDEX']));
				unset($campaigns[$indexCampaign]);			
			}
			else {
			
				$countProductLine = count($campaign['products'][0]);
								 
			    if ($countProductLine != 4) {
					\SmartFocus\Util::log('[' . \SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_WARNING) . ']' . sprintf(ERROR_MESSAGE_PRODUCTS_INSUFFICIENT, $campaign['CAMPAIGN_INDEX'], $countProductLine));
					unset($campaigns[$indexCampaign]);			    
			    }			    
			    elseif (empty($campaign['products'][1]) || count($campaign['products'][1]) != 4) {
				
					if (!empty($campaign['products'][1])) {
						unset($campaign['products'][1]);
					}							    
					\SmartFocus\Util::log('[' . \SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_NOTICE) . ']' . sprintf(ERROR_MESSAGE_PRODUCTS_LINE, $campaign['CAMPAIGN_INDEX']));			    			    
				}
			    
			}
						
			$campaign['productsTemplate'] = NULL; 
			foreach ($campaign['products'] as $line) {
				$template = NULL;			
				foreach ($line as $product) {				
					foreach ($product as $key => $value) {					
						$subject = empty($template) ? $productsTemplate : $template;
						$template = str_replace(('{' . $key . '}'), $value, $subject);					
					}									
				}
				$campaign['productsTemplate'] .= $template;
			}
			
			$campaign['imageTemplate'] = NULL;			
			if (!empty($campaign['image']['MAIN_IMG_ALT'])) {
				$campaign['imageTemplate'] = $imageTemplate;			
				foreach ($campaign['image'] as $key => $value) {																		
					$campaign['imageTemplate'] = str_replace(('{' . $key . '}'), $value, $campaign['imageTemplate']);
				}				
			}
			
		}
		
		$countCampaigns = count($campaigns);
		
		if ($countCampaigns == 0) {
			\SmartFocus\Util::triggerError(
				sprintf(ERROR_MESSAGE_CAMPAIGNS_NOT_FOUND, $downloadedFile['localPath']),
				\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ERROR),
				\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), EXCEPTION_CODE_ERROR),
				ACTION_SCRIPT,
				TRUE
			);
		}		
		elseif ($countCampaigns == $countColumns - 1) {
			\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_FILE_PROCESS, $downloadedFile['localPath']));
		}
		else {
			\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_FILE_PROCESS_SOME, $downloadedFile['localPath'], $countCampaigns));
		}		 
		
		return $campaigns;
	
	}

}
?>