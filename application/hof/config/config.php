<?php
$config['logFolder'] 					= '/opt/automatic/application/hof/log';
$config['uploadFolder']				= '/opt/automatic/application/hof/upload';
$config['applicationName'] 		= 'HOF';
$config['filePattern'] 				= 'campaign_%s.csv';
$config['fileTreatedPattern']	= 'processed_campaign_%s.csv';
$config['ftp']								= array(
	'server'    => 'webe.emv3.com',
	'username' 	=> 'HOF_AUTOMATION_ftp',
    'password' 	=> 'Emv1234!',
	'folder' 	=> '/persona_campaing_automation',
);
$config['api']['partner_users']	= array('A' => 'jfarinas_hof_api');
$config['bannerImageId']				= '2041';
$config['bannerProductsId']			= '1982';
$config['messageId']						= '345181';
$config['segmentIdList']				= array(
	1 => '320113',
	2 => '319585',
	3 => '319992',
	4 => '321812',
	5 => '319912',
	6 => '319586',
	7 => '320095',
	8 => '319916',
	9 => '320001',
);
$config['nmp']['recepients']		= array('bogdan.sima@smartfocus.com', 'Rahel.Chowdhury@smartfocus.com', 'Rimantas.Sipkus@smartfocus.com', 'PMietkie@hof.co.uk', 'FDSilva@hof.co.uk', 'VHopkins@hof.co.uk');
$config['bannerReplacePattern']	= '[EMV INCLUDE]%s[EMV /INCLUDE]';
$config['fileSeparator']				= ',';
$config['productsTemplatePath']	= 'template/products.html'; // used only if there is no banner
$config['imageTemplatePath']		= 'template/image.html'; // used only if there is no banner
$config['productPriceTemplate']	= '<span style="color: %s;text-decoration:none;font-weight: normal;font-size: 14px;">%s</span><br /><span style="color: #666666;text-decoration:none;font-weight:normal;font-size: 12px;">%s</span>';
$config['productPriceColorNow']	= '#000000';
$config['productPriceColorWas']	= '#da291c';
$config['productPriceNow']			= 'Now %s';
$config['productPriceWas']			= 'Was %s';
$config['trackingPattern']			= '_$ja=tsid:%1$s|kw:%2$s|cgn:%3$s&cm_cat=CAMPAIGN&cm_ite=link&cm_lm=emv:[EMV FIELD]MEMBER_ID[EMV /FIELD]&cm_pla=%4$s&cm_ven=Email&email=[EMV FIELD]EMAIL[EMV /FIELD]&kw=%5$s';
