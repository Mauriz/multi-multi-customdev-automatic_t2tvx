<?php
// Actions
define('ACTION_API_INIT',			'Initialize the API.');
define('ACTION_SCRIPT', 			'Script.');
define('ACTION_FILE_DOWNLOAD',		'Download file.');
define('ACTION_FILE_PROCESS', 		'Process the downloaded file: %s.');
define('ACTION_GET_MESSAGE', 		'Get SmartFocus Email message: %d.');
define('ACTION_GET_BANNER', 		'Get SmartFocus Email banner: %d.');
define('ACTION_CREATE_CAMPAIGNS',	'Create campaigns.');
define('ACTION_CHECK_SEGMENT',		'Check segment (campaign: %d, id: %d).');
define('ACTION_CREATE_MESSAGE',		'Create message (campaign: %d, name: %s).');
define('ACTION_CREATE_CAMPAIGN',	'Create campaign: %s.');
define('ACTION_FILE_TREAT', 		'Treat file: %s.');

// Environment
define ('ENVIRONMENT_TEST', 'TEST');
define ('ENVIRONMENT_LIVE', 'LIVE');

// Errors
define ('ERROR_MESSAGE_DOWNLOAD_FOLDER_INVALID',			'Invalid download folder (folder: %s).');
define ('ERROR_MESSAGE_FILE_DOWNLOAD',						'Campaign file could not be downloaded from the FTP server (file: %s).');

define ('ERROR_MESSAGE_TEMPLATE_CONTENT_INVALID',			'Invalid message template content (id: %d).');
define ('ERROR_MESSAGE_PRODUCTS_TEMPLATE_CONTENT_INVALID',	'Invalid products template content (id: %d).');
define ('ERROR_MESSAGE_PRODUCTS_TEMPLATE_FILE_INVALID',		'Invalid products template file (file: %s).');
define ('ERROR_MESSAGE_IMAGE_TEMPLATE_CONTENT_INVALID',		'Invalid image template content (id: %d).');
define ('ERROR_MESSAGE_IMAGE_TEMPLATE_FILE_INVALID',		'Invalid image template file (file: %s).');
define ('ERROR_MESSAGE_FILE_LINE_INVALID',					'Invalid line found (file: %s, line: %d, reason: %s).');
define ('ERROR_MESSAGE_FILE_LINE_FOUND',					'No "%s" line found (file: %s).');
define ('ERROR_MESSAGE_FILE_VALUE_EMPTY',					'The "%s" value is empty (file: %s, line: %d, resolution: %s).');
define ('ERROR_MESSAGE_CAMPAIGNS_NOT_FOUND',				'No campaigns have been found (file: %s).');
define ('ERROR_MESSAGE_PRODUCTS_EMPTY',						'No valid products found (campaign: %d).');
define ('ERROR_MESSAGE_PRODUCTS_INSUFFICIENT',				'Insuficcient products found (campaign: %d, products %d).');
define ('ERROR_MESSAGE_PRODUCTS_LINE',						'Only 1 line of products found (campaign: %d).');
define ('ERROR_MESSAGE_SEGMENT_ID_EMPTY',					'No segment id found (campaign: %d).');
define ('ERROR_MESSAGE_SEGMENT_ID_INVALID',					'Invalid segment id (campaign: %d, id: %d).');
define ('ERROR_MESSAGE_MESSAGE_NOT_CREATED',				'Failed to create message. Empty message id returned (campaign: %d, name: %s).');
define ('ERROR_MESSAGE_CAMPAIGN_NOT_CREATED',				'Failed to create campaign. Empty campaign id returned (campaign: %d, name: %s).');

define ('ERROR_MESSAGE_FILE_LINES_INVALID',					'Multiple invalid lines found. (file: %s, read: %d, ok: %d, skipped: %d).');
define ('ERROR_MESSAGE_FILE_PROCESS',						'No files were processed from %d downloaded.');
define ('ERROR_MESSAGE_FILE_READ',							'Unexpected error while reading the file (file: %s, line: %d).');
define ('ERROR_MESSAGE_MEMBER_IMPORT',						'No files were imported from %d processed.');
define ('ERROR_MESSAGE_MEMBER_IMPORT_STATUS',				'The members import has failed (status: %s).');
define ('ERROR_MESSAGE_MEMBER_IMPORT_STATUS_GET',			'API error retrieving the members import status. Empty or no object returned. (id: %d).');
define ('ERROR_MESSAGE_MEMBER_IMPORT_STATUS_TIMEOUT',		'Timeout error for the members import (id: %d, status: %s, attempts: %d).');
define ('ERROR_MESSAGE_MEMBER_UPLOAD_SUMMARY_LIST',			'API error retrieving the members upload summary list. Empty or no object returned.');
define ('ERROR_MESSAGE_MEMBER_UPLOAD_SUMMARY_LIST_STATUS',	'Invalid upload summary list status found: %s.');
define ('ERROR_MESSAGE_MEMBER_UPLOAD_SUMMARY_LIST_TIMEOUT',	'Timeout error for the upload permission check (attempts: %d).');
define ('ERROR_MESSAGE_MEMBER_UPLOAD_UNKNOWN',				'Unknown error while uploading members (file: %s).');
define ('ERROR_MESSAGE_MEMBER_IMPORT_LOG_GET',				'API error retrieving the members import log. Empty or no object returned. (id: %d).');
define ('WARNING_MESSAGE_MEMBER_IMPORT_REJECTED',			'All lines in the file were rejected.');
define ('MESSAGE_MEMBER_IMPORT_PARTIALLY_REJECTED',			'%d lines out of %d were rejected in the upload.');


define ('ERROR_MESSAGE_NMP_CONFIG',				'Wrong or missing nmp configuration.');
define ('ERROR_MESSAGE_NMP_RECIPIENTS',			'No recepients found.');
define ('ERROR_MESSAGE_NMP_SEND',				'Could not send notification. %s');

// Log
define ('LOG_MESSAGE_OK_FILE_DOWNLOAD',			'<----Successfully downloaded file (file: %s).');
define ('LOG_MESSAGE_OK_FILE_PROCESS',			'<----Successfully processed the downloaded file (file: %s).');
define ('LOG_MESSAGE_OK_FILE_PROCESS_SOME',		'<----Partially processed the downloaded file (file: %s, campaigns: %d).');
define ('LOG_MESSAGE_KO_PRODUCT',				'<----Skipped product (campaign: %d, product: %d, attributes: %s).');
define ('LOG_MESSAGE_OK_SEGMENT_CHECK',			'<----Segment ok (campaign: %d, id: %d).');
define ('LOG_MESSAGE_OK_MESSAGE_CREATE',		'<----Created message (campaign: %d, id: %d, name: %s).');
define ('LOG_MESSAGE_OK_CAMPAIGN_CREATE',		'<----Created campaign (campaign: %d, id: %d, name: %s).');
define ('LOG_MESSAGE_KO_MESSAGE_LINKS_TRACK',	'<----There were no links to track (name: %s, id: %d).');

// File process status
define ('FILE_PROCESS_STATUS_OK',	'ok');
define ('FILE_PROCESS_STATUS_KO',	'ko');