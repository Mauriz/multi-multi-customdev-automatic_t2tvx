<?php
require 'bootstrap.php';

$dateTime 			= new \DateTime('now');

// Specific script commands
$localCampaign	= empty($_GET['campaign']) ? NULL : trim($_GET['campaign']);
$skipProcess		= empty($_GET['reprocess']) ? FALSE : !filter_var($_GET['reprocess'], FILTER_VALIDATE_BOOLEAN);
$retryDownload	= empty($_GET['download']) || intval($_GET['download']) != 1 ? FALSE : TRUE;

$allCampaigns 			= array();
$dateString					= $dateTime->format('Ymd'); 
$uploadFolder				= $config['uploadFolder'] . '/' . $dateString;
$sourceFilePathList	= array();
$filePathSkipped		= $uploadFolder . '/skipped_' . $dateString . '.txt';
$fileLines					= array();
$nmpParameters 			= array(
	'encrypt'			=> $config['nmp']['encrypt'],
	'id'					=> $config['nmp']['id'],
	'random'			=> $config['nmp']['random'],
	'recipients'	=> $config['nmp']['recepients'],
	'from' 				=> $config['applicationName'],
	'subject' 		=> $config['nmp']['subjects']['NOTIFY'],
);

\SmartFocus\Util::log('[START]' . ACTION_SCRIPT);

// If there is no need to reprocess the files or a specific campaign need to be reloaded (usually when there was an API failure)
if ($skipProcess || !empty($localCampaign)) {
	$allCampaigns = \SmartFocus\Bnpp\Util::getLocalCampaigns($uploadFolder, $config['memberFileSeparator']); 
}
else {
	// When the retry download is need or the script runs normally
	if (empty($localCampaign) || $retryDownload) {
	
		// -----====================-----
		// START FILES DONWLOAD
		// -----====================-----
		\SmartFocus\Util::log('---->' . ACTION_FILE_DOWNLOAD);
		
		// Connect to the FTP server and search for files to download
		try {
		
			if (!is_dir($uploadFolder) && !mkdir($uploadFolder)) {
			    throw new \Exception(sprintf(ERROR_MESSAGE_FOLDER_CREATE, $uploadFolder), EXCEPTION_CODE_ERROR);
			}
		
			$ftp = new \SmartFocus\Ftp(
				$config['ftp']['server'], 
				$config['ftp']['username'], 
				$config['ftp']['password'], 
				$config['ftp']['folder'], 
				NULL, 
				$config['ftp']['ssl'], 
				$config['ftp']['port'],
				NULL,
				$config['ftp']['passiveMode']
			);
			
			$ftp->connect();				
			
			$sourceFilePathList = $ftp->searchFile('deals');	
		}
		catch (\Exception $exception) {
		    \SmartFocus\Util::triggerError(
				$exception->getMessage(),
				\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
				\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
				ACTION_SCRIPT,
				TRUE
			);
		}
		
		// Download the files form the FTP server 		
		$downloadTimeTotal 		= 0;
		$downloadedFilePathList = array();
		foreach ($sourceFilePathList as $sourceFile) {
		
			try {
				$filePath 			= $uploadFolder . '/' . $sourceFile['name'];
				$downloadTimeStart 	= microtime(TRUE);
				
				$ftp->downloadFile($sourceFile['path'], $filePath);
				
				$downloadTime				= microtime(TRUE) - $downloadTimeStart;				
				$downloadTimeTotal 			+= $downloadTime;
				$downloadedFilePathList[] 	= array('name' => $sourceFile['name'], 'path' => $filePath);
				
				\SmartFocus\Util::log(sprintf(LOG_MESSAGE_TIME_DOWNLOAD, $downloadTime, $filePath));
			}
			catch (\Exception $exception) {
				$exceptionCode 		= $exception->getCode();
				$exceptionMessage 	= $exception->getMessage();
				
				if ($exceptionCode == EXCEPTION_CODE_FTP_ERROR) {				
					\SmartFocus\Util::triggerError($exceptionMessage, \SmartFocus\Util::exceptionCodeToLogCode($exceptionCode));				
				}
				else {
				    \SmartFocus\Util::triggerError(
						$exceptionMessage,
						\SmartFocus\Util::exceptionCodeToLogCode($exceptionCode),
						\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exceptionCode),
						ACTION_SCRIPT,
						TRUE
					);			
				}
			}		
		
		}
		\SmartFocus\Util::log(sprintf(LOG_MESSAGE_TIME_DOWNLOAD_TOTAL, $downloadTimeTotal));
		
		// Close the FTP server connection and check the list of downloaded files
		try {
			$ftp->close();
			
			if (empty($downloadedFilePathList)) {
			    throw new \Exception(sprintf(ERROR_MESSAGE_FILE_DOWNLOAD_EMPTY), EXCEPTION_CODE_ERROR);
			}
		}
		catch (\Exception $exception) {
		    \SmartFocus\Util::triggerError(
				$exception->getMessage(),
				\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
				\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
				ACTION_SCRIPT,
				TRUE
			);
		}			
		// -----====================-----
		// END FILES DONWLOAD
		// -----====================-----
	
	}
	
	// -----====================-----
	// START FILES PROCESS
	// -----====================-----
	$processTimeTotal = 0;
	\SmartFocus\Util::log('---->' . ACTION_FILE_PROCESS);
	foreach ($downloadedFilePathList as $indexFile => $downloadedFile) {
	
		try {
		
			$filePath = $downloadedFile['path'];
		
			if (!is_readable($filePath)) {
		    	throw new \Exception(sprintf(ERROR_MESSAGE_FILE_DOWNLOAD, $filePath), EXCEPTION_CODE_WARNING);
			}
			$processTimeStart 	= microtime(TRUE);
					
		    $campaigns 			= \SmartFocus\Bnpp\Util::parseMemberFile($fileLines, $filePath, $uploadFolder, $config['sendDateFormat'], $dateString, $indexFile, $ftpEnvironment);
		    
	    	$processTime		= microtime(TRUE) - $processTimeStart;
	    	$processTimeTotal	+= $processTime;
	    	
	    	\SmartFocus\Util::log(sprintf(LOG_MESSAGE_TIME_PROCESS, $processTime, $filePath));		    
		    
		    // Log skipped lines
			if (!empty($fileLines['log'])) {    	
		    	foreach ($fileLines['log'] as $indexLine => $skippedLine) {    		
		    		$writeMode 	= FILE_APPEND;
		    		$linePrefix = PHP_EOL;    	
		    		if ($indexFile < 1 && $indexLine < 1) {
			    		$writeMode 	= NULL;
			    		$linePrefix = NULL;    		
					}			    	
		    		file_put_contents($filePathSkipped, $linePrefix . '[' . $downloadedFile['name'] . ']' . $skippedLine, $writeMode);    	
				}
			}
		    
			if (empty($campaigns)) {
		        throw new \Exception(sprintf(ERROR_MESSAGE_CAMPAIGN_LIST_EMPTY, $filePath), EXCEPTION_CODE_WARNING);
			}
			if ($fileLines['ok'] >= $fileLines['read'] - 2) {
		    	\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_FILE_PROCESS, $filePath, count($campaigns)));
			}
			else {
		    	\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_FILE_PROCESS_ERROR, $filePath, count($campaigns), $fileLines['read'], $fileLines['skipped'], $fileLines['ok']));
			}
			
			$allCampaigns += $campaigns;
		}
		catch (\Exception $exception) {
		    \SmartFocus\Util::triggerError($exception->getMessage(), \SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()));
		}
	
	}
	\SmartFocus\Util::log(sprintf(LOG_MESSAGE_TIME_PROCESS_TOTAL, $processTimeTotal));
	// -----====================-----
	// END FILES PROCESS
	// -----====================-----	

}

// -----====================-----
// START CHECK CAMPAIGNS LIST
// -----====================-----
if (empty($allCampaigns)) {
    \SmartFocus\Util::triggerError(
		$exception->getMessage(),
		\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
  		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
		ACTION_SCRIPT,
  		TRUE
	);
}

// If a specific campaign was reloaded
if (!empty($localCampaign) && empty($allCampaigns[$localCampaign])) {
    \SmartFocus\Util::triggerError(
		sprintf(ERROR_MESSAGE_CAMPAIGN_EMPTY, $localCampaign),
		\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ERROR),		
		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, EXCEPTION_CODE_ERROR),		
		ACTION_SCRIPT,
  		TRUE
	);
}
elseif (!empty($localCampaign)) {
	$campaign = $allCampaigns[$localCampaign];
	unset($allCampaigns);
	$allCampaigns[$localCampaign] = $campaign;   
}
// -----====================-----
// END CHECK CAMPAIGNS LIST
// -----====================-----

// -----====================-----
// START API INIT
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_API_INIT);
try {
	$partnerAuth 				= new \SmartFocus\API\PartnerPrivilegedAuthenticationService($config['api']['partner_users']['A'], $config['api']['partner_key1'], $config['api']['partner_key2']);
	$batchMemberService 		= new \SmartFocus\API\BatchMemberService($partnerAuth);
	$campaignManagementService  = new \SmartFocus\API\CampaignManagementService($partnerAuth);
}
catch (\Exception $exception) {
	\SmartFocus\Util::triggerError(
		$exception->getMessage(),
		\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
  		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
		ACTION_SCRIPT,
		true
	);
}
// -----====================-----
// END API INIT
// -----====================-----

// -----====================-----
// START SYNCRONIZATION WITH SMART FOCUS EMAIL
// -----====================-----
foreach ($allCampaigns as $campaign) {

	try {

		$action = sprintf(ACTION_CAMPAIGN_PROCESS, $campaign['reference']);

        \SmartFocus\Util::log('[START]' . $action, true);

    	// -----====================-----
		// START IMPORT MEMBERS
     	// -----====================-----
		\SmartFocus\Util::log('---->' . sprintf(ACTION_MEMBER_IMPORT, $campaign['memberFile']));

			// Generate member fields mapping
			$mapping = \SmartFocus\Bnpp\Util::generateMembersMapping($campaign['memberHeader']);
	        // Upload and import members
			//$campaign['importId'] = (int) $batchMemberService->uploadFileInsert(file_get_contents($campaign['memberFile']), pathinfo($campaign['memberFile'], PATHINFO_BASENAME), $config['memberFileSeparator'], $mapping, 'LOWER(EMAIL)', 'UTF-8', true, 'dd/MM/yyyy');
			$campaign['importId'] = (int) $batchMemberService->uploadFileMerge(file_get_contents($campaign['memberFile']), pathinfo($campaign['memberFile'], PATHINFO_BASENAME), $config['memberFileSeparator'], $mapping, 'LOWER(EMAIL), LOWER(REF_MESSAGE), LOWER(ID_EMETTEUR), LOWER(SEGMENT)', 'UTF-8', true, 'dd/MM/yyyy');
			if (empty($campaign['importId'])) {
		        throw new \Exception(sprintf(ERROR_MESSAGE_MEMBER_UPLOAD_UNKNOWN, $campaign['memberFile']), EXCEPTION_CODE_ERROR);
			}
			// Check the import status
		    $campaign['importStatus'] = \SmartFocus\Bnpp\Util::checkMembersImportStatus($batchMemberService, $campaign['importId'], $config['importStatuses'], $config['importStatusGetDelay']);

		\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_MEMBER_IMPORT, $campaign['importId'], $campaign['importStatus']));
    	// -----====================-----
		// END IMPORT MEMBERS
     	// -----====================-----
     	     	
     	// TODO: search segment (if it exists, add version to it)

		// -----====================-----
		// START CREATE SEGMENT
     	// -----====================-----
        \SmartFocus\Util::log('---->' . sprintf(ACTION_SEGMENT_CREATE, $campaign['segmentName']));

			// Create a new segment
			$campaign['segmentId'] = (int) $campaignManagementService->segmentationCreateSegment($campaign['segmentName']);
			if (empty($campaign['segmentId'])) {
		    	throw new \Exception(sprintf(ERROR_MESSAGE_SEGMET_CREATE_UNKNOWN, $campaign['segmentName']), EXCEPTION_CODE_ERROR);
			}
			// Add demohraphic criteria to the new segment
			if (!(bool) $campaignManagementService->segmentationAddStringDemographicCriteriaByObj($campaign['segmentId'], 'SEGMENT', 'EQUALS', $campaign['segmentName'])) {
		    	throw new \Exception(sprintf(ERROR_MESSAGE_SEGMET_ADD_CRITERIA_UNKNOWN, $campaign['segmentId']), EXCEPTION_CODE_ERROR);
			}
		    sleep($config['importStatusCountDelay']);
			// Count segmentation
			$campaign['segmentCount'] = \SmartFocus\Bnpp\Util::segmentCount($campaignManagementService, $campaign['segmentId'], $config['importStatusCountDelay']);

		\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_SEGMENT_CREATE, $campaign['segmentId'], $campaign['segmentCount']));
  		// -----====================-----
		// END CREATE SEGMENT
      	// -----====================-----
      	
      	// TODO: search message (if it exists, add version to it)
      	
    	// -----====================-----
		// START CLONE MESSAGE
     	// -----====================-----
        \SmartFocus\Util::log('---->' . sprintf(ACTION_MESSAGE_CLONE, $campaign['reference']));

	        // Get the message to clone by field
		    $campaign['templateId'] = $campaignManagementService->getEmailMessagesByField('name', $campaign['templateName']);
			if (empty($campaign['templateId'])) {
		    	throw new \Exception(sprintf(ERROR_MESSAGE_MESSAGE_GET_UNKNOWN, $campaign['reference']), EXCEPTION_CODE_ERROR);
			}
	        // Create new message
			$campaign['messageId'] = (int) $campaignManagementService->cloneMessage($campaign['templateId'], $campaign['messageName']);
			if (empty($campaign['messageId'])) {
		    	throw new \Exception(sprintf(ERROR_MESSAGE_MESSAGE_CLONE_UNKNOWN, $campaign['templateId'], $campaign['messageName']), EXCEPTION_CODE_ERROR);
			}

		\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_MESSAGE_CLONE, $campaign['templateId'], $campaign['messageId']));
  		// -----====================-----
		// END CLONE MESSAGE
    	// -----====================-----
		
		// TODO: search campaign (if it exists, add version to it)      	

        // Create the send date right before creating the campaign to avoid imports delays        
        $campaign['sendDate'] = \SmartFocus\Bnpp\Util::setSendDate(NULL, $config['timeZone']);

		// -----====================-----
		// START CREATE CAMPAIGN
  		// -----====================-----
        \SmartFocus\Util::log('---->' . sprintf(ACTION_CAMPAIGN_CREATE, $campaign['name'], $campaign['sendDate'], $campaign['messageId'], $campaign['segmentId']));

	        // Create campaign
		    $campaign['id'] = (int) $campaignManagementService->createCampaignByObj(array(
				'name' 			=> $campaign['name'],
				'description' 	=> $campaign['name'] . ' - ' . $campaign['sendDate'],
				'sendDate' 		=> $campaign['sendDate'],
				'messageId' 	=> $campaign['messageId'],
				'mailinglistId'	=> $campaign['segmentId'],
				'emaildedupflg'	=> FALSE,
			));
			if (empty($campaign['id'])) {
		        throw new \Exception(sprintf(ERROR_MESSAGE_CAMPAIGN_CREATE_UNKNOWN, $campaign['reference']), EXCEPTION_CODE_ERROR);
			}

		\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_CAMPAIGN_CREATE, $campaign['id']));
  		// -----====================-----
		// END CREATE CAMPAIGN
    	// -----====================-----

		// -----====================-----
		// START POST CAMPAIGN
  		// -----====================-----
		\SmartFocus\Util::log('---->' . sprintf(ACTION_CAMPAIGN_POST, $campaign['id']));

			// Post campaign
		    $campaign['postStatus'] = (bool) $campaignManagementService->postCampaign($campaign['id']);
			if (!$campaign['postStatus']) {
		        throw new \Exception(sprintf(ERROR_MESSAGE_CAMPAIGN_POST_UNKNOWN, $campaign['id']), EXCEPTION_CODE_ERROR);
			}

		\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_CAMPAIGN_POST, $campaign['id']));
  		// -----====================-----
		// END CREATE AND POST CAMPAIGN
    	// -----====================-----

        \SmartFocus\Util::log('[END]' . $action);

	}
	catch (\Exception $exception) {		
    	\SmartFocus\Util::triggerError(
			$exception->getMessage(),
			\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
   			\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
			$action
		);
	}

}
// -----====================-----
// END SYNCRONIZATION WITH SMART FOCUS EMAIL
// -----====================-----

$report = \SmartFocus\Util::$log->getHistory();
$report[] = '[END]' . ACTION_SCRIPT;

\SmartFocus\Util::notify($nmpParameters, implode(PHP_EOL, $report));

\SmartFocus\Util::log('[END]' . ACTION_SCRIPT);

exit(LOG_CODE_SUCCESS);
?>