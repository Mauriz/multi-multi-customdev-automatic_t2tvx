<?php
ini_set('max_execution_time', 0);

require '/opt/automatic/core/bootstrap.php';

spl_autoload_register();

require __DIR__ . '/config/constants.php';

//==========-------- Define environment --------==========//
$ftpEnvironment = empty($_GET['environment']) ? NULL : strtoupper(trim($_GET['environment']));
if (!in_array($ftpEnvironment, array(GET_ENVIRONMENT_TEST, GET_ENVIRONMENT_PROD))) {
	$ftpEnvironment = GET_ENVIRONMENT_PROD; 
}
 
switch ($ftpEnvironment) {
	case GET_ENVIRONMENT_TEST:
		define ('BNPP_ENVIRONMENT', 290);
		break;
	case GET_ENVIRONMENT_PROD:
	default:
		define ('BNPP_ENVIRONMENT', 292);
		break;				 
}
// To override the BNPP_ENVIRONMENT to always take the files from the development FTP server, comment the switch blook above and decomment the following line
// define ('BNPP_ENVIRONMENT', 289);

require __DIR__ . '/config/config.php';

new \SmartFocus\Bnpp\Util($config);
