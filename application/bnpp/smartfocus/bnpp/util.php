<?php namespace SmartFocus\Bnpp;

/**
 * Utility class
 */
class Util extends \SmartFocus\Util {

	/**
	 * Check the members import status with delay and timeout
	 *
	 * @param \SmartFocus\API\BatchMemberService $batchMemberService
	 * @param int $id
	 * @param array $statusList
	 * @param int $delay [optional][default=30]
	 * @param int $maxAttempts [optional][default=10]
	 * @return string or throw \Exception
	 */
	public static function checkMembersImportStatus($batchMemberService, $id, $statusList, $delay = 30, $maxAttempts = 10)
	{

		$repeat 	= TRUE;
		$attempts	= 0;
	    $status		= NULL;

		while ($repeat && $attempts < $maxAttempts) {

	        $attempts++;

	    	$response = $batchMemberService->getUploadStatus($id);
			
			// SF API bug (empty response)
			if (empty($response) || empty($response->status)) {				            	        	            
	            self::log('[ERROR]' . sprintf(ERROR_MESSAGE_MEMBER_IMPORT_STATUS_GET, $id));	            	            
	            $debug = $batchMemberService->getDebug();
	            if (!empty($debug->request)) {
	            	self::log($debug->request, FALSE, FALSE);
				}
				if (!empty($debug->response)) {
					self::log($debug->response, FALSE, FALSE);
				}	            
			}
			else {
		        $status = strval($response->status);
	
				if (in_array($status, $statusList['success'])) {
					$repeat = FALSE;
				}
				elseif (in_array($status, $statusList['pending'])) {
					sleep($delay);
				}
				else {
		            $repeat = FALSE;
		            throw new \Exception(sprintf(ERROR_MESSAGE_MEMBER_IMPORT_STATUS, $status));
				}			
			}

			if ($repeat && $attempts == $maxAttempts) {
	        	$repeat = FALSE;
	            throw new \Exception(sprintf(ERROR_MESSAGE_MEMBER_IMPORT_STATUS_TIMEOUT, $id, $status, $attempts));
			}

		}

		return $status;

	}
	
	/**
	 *  Get campaigns from already downloaded and processed files
	 *  
	 * @param string $folder
	 * @param string $fieldSeparator 	 	 	 
	 * @return array 	 
	 */	 	
	public static function getLocalCampaigns($folder, $fieldSeparator)
	{		
		$campaigns	= array(); 
		$entities 	= scandir($folder);		
		foreach ($entities as $entity) {
			$filePath = $folder . '/' . $entity;						
			if (is_file($filePath) && strtolower(pathinfo($filePath, PATHINFO_EXTENSION)) == 'csv') {
				$campaignReference = pathinfo($filePath, PATHINFO_FILENAME);
				
				if (($fileHandle = fopen($filePath, 'r')) !== FALSE && ($line = fgets($fileHandle, 8192)) !== FALSE) {
				
					$campaigns[$campaignReference] = array(								
						'name'			=> $campaignReference . '_' . pathinfo($folder, PATHINFO_BASENAME),
						'reference'		=> $campaignReference,
						'segmentName'	=> $campaignReference . '_' . pathinfo($folder, PATHINFO_BASENAME),
						'messageName'	=> $campaignReference . '_' . pathinfo($folder, PATHINFO_BASENAME),
						'memberHeader'	=> explode($fieldSeparator, $line),
						'memberFile'	=> $filePath,
						'templateName' 	=> sprintf(self::$config['templateNamePattern'], $campaignReference),
					);  				
				}

			}
		}
		
		return $campaigns;
		 
	}

	/**
	 * Generate members mapping
	 *
	 * @param array $fieldList
	 * @return array
	 */
	public static function generateMembersMapping($fieldList)
	{

		$mapping	= array();
		$column 	= 0;

		foreach ($fieldList as $field) {
		    $column++;
		    $mapping['column'][] = array(
				'colNum' 	=> $column,
				'toReplace'	=> $field == 'EMAIL' ? FALSE : TRUE,
				'fieldName' => $field,
			);
		}

		return $mapping;

	}

	/**
	 * Parse member file
	 *
	 * @param string $filePath
	 * @param string $uploadFolder
	 * @param string $sendDateFormat
	 * @param string $segmentSufix
	 * @param int $indexFile [optional][default=0]
	 * @param string $ftpEnvironment [opyional][default=GET_ENVIRONMENT_PROD]	  	  	 
	 * @return bool
	 */
	public static function parseMemberFile(&$lines, $filePath, $uploadFolder, $sendDateFormat, $segmentSufix, $indexFile = 0, $ftpEnvironment = GET_ENVIRONMENT_PROD)
	{

		$lines = array(
			'read' 		=> 0,
			'skipped' 	=> 0,
			'ok' 		=> 0,
			'log' 		=> array(),
		);

		if (!is_readable($filePath) || !($fileHandle = fopen($filePath, 'r'))) {
			throw new \Exception(sprintf(ERROR_MESSAGE_FILE_DOWNLOAD, $filePath));
		}

		$campaigns 	= array();
        $headers 	= array();

		while (($line = fgets($fileHandle, 8192)) !== FALSE) {

			$lines['read']++;

			// Skip first and last line
			if ($lines['read'] == 1 || feof($fileHandle)) {
                $lines['skipped']++;
                $lines['log'][] = sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $lines['read'], 'First line');
				continue;
			}

			if (mb_strlen($line) < 5250) {
                $lines['skipped']++;
                $lines['log'][] = sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $lines['read'], 'Too short');
                continue;
			}

			$data = self::parseMemberFileLine($line, $sendDateFormat, $segmentSufix, $ftpEnvironment);

			if (empty($data['member']['EMAIL']) || !filter_var($data['member']['EMAIL'], FILTER_VALIDATE_EMAIL)) {
            	$lines['skipped']++;
            	$lines['log'][] = sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $lines['read'], 'Email: ' . $data['member']['EMAIL']);
                continue;
			}

			if (empty($data['reference'])) {
            	$lines['skipped']++;
            	$lines['log'][] = sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $lines['read'], 'Reference');
                continue;
			}
			elseif (!in_array($data['reference'], self::$config['acceptReferenceList'])) {
            	$lines['skipped']++;
            	$lines['log'][] = sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $lines['read'], 'Rejected');
                continue;
			}

	        $memberfilePath	= $uploadFolder . '/' . $data['reference'] . '.csv';

			if ($indexFile < 1 && empty($headers[$data['reference']])) {				
                $headers[$data['reference']] = $data['memberHeader'];
                file_put_contents($memberfilePath, implode('|', $headers[$data['reference']]));
			}

	        file_put_contents($memberfilePath, PHP_EOL . implode('|', $data['member']), FILE_APPEND);
			unset ($data['member']);

	        $data['memberFile'] = $memberfilePath;
	        $data['templateName'] = sprintf(self::$config['templateNamePattern'], $data['reference']);

			if (empty($campaigns[$data['reference']])) {
	            $campaigns[$data['reference']] = $data;
			}

            $lines['ok']++;

		}
		if (!feof($fileHandle)) {
	        throw new \Exception(sprintf(ERROR_MESSAGE_FILE_READ, $filePath, $lines['read']));
		}
		fclose($fileHandle);

		return $campaigns;

	}

	/**
	 * Parse member file line
	 *
	 * @param string $line
	 * @param string $sendDateFormat
	 * @param string $segmentSufix
	 * @param string $ftpEnvironment [opyional][default=GET_ENVIRONMENT_PROD] 	  	 
	 * @return bool
	 */
	public static function parseMemberFileLine($line, $sendDateFormat, $segmentSufix, $ftpEnvironment = GET_ENVIRONMENT_PROD)
	{
	    $data['member']['EMAIL'] 			= trim(mb_substr($line, 5090, 80));
	    $data['member']['EMAIL'] 			.= '@' . trim(mb_substr($line, 5170, 80));	    
		$data['member']['RMCT_ID'] 			= trim(mb_substr($line, 19, 10)); //RMCT_ID
		$data['member']['REF_MESSAGE']		= trim(mb_substr($line, 29, 10)); //REF_MESSAGE
		$data['member']['ID_EMETTEUR']		= trim(mb_substr($line, 65, 25)); //ID_EMETTEUR
		$data['member']['DATE_HEURE_CREA']	= trim(mb_substr($line, 39, 26)); //DATE_HEURE_CREA
	    $data['member']['SEGMENT'] 			= $data['member']['REF_MESSAGE'] . '_' . $segmentSufix; //REF_MESSAGE_Ymd
	    
		if ($ftpEnvironment == GET_ENVIRONMENT_PROD) {
			$data['member']['EMVADMIN2'] = $ftpEnvironment;
		}
		else {
			$data['member']['EMVADMIN1'] 	= $ftpEnvironment;
			$data['member']['SEGMENT']		= $ftpEnvironment . '_' . $data['member']['SEGMENT']; // Make sure the test members are marked properly 
		}

		for ($i = 0; $i < 100; $i++) {
			$data['member']['VARIABLE_' . ($i + 1)] = trim(mb_substr($line, (90 + ($i * 50)), 50)); //VARIABLE_i
	    	$data['member']['VARIABLE_' . ($i + 1)] = mb_convert_encoding(trim(mb_substr($line, (90 + ($i * 50)), 50)), 'UTF-8'); //VARIABLE_i
			$data['member']['VARIABLE_' . ($i + 1)] = str_replace('"', '""', $data['member']['VARIABLE_' . ($i + 1)]);			
	    	if (!empty($data['member']['VARIABLE_' . ($i + 1)])) {
				$data['member']['VARIABLE_' . ($i + 1)] = '"' . $data['member']['VARIABLE_' . ($i + 1)] . '"';
			}
		}

		$data['name'] 			= $data['member']['SEGMENT'];
		$data['reference']		= trim(mb_substr($line, 29, 10)); //REF_MESSAGE				
		$data['segmentName']	= $data['member']['SEGMENT'];
		$data['messageName']	= $data['member']['SEGMENT'];
		$data['memberHeader']	= array_keys($data['member']);

		return $data;

	}

	/**
	 * Parse send date
	 *
	 * @param string $date
	 * @param string $format
	 * @return string
	 */
	public static function parseSendDate($date, $format)
	{

		if (!($dateTime = \DateTime::createFromFormat($format, $date))) {
			return NULL;
		}

	 	return $dateTime->format('c');

	}

	/**
	 * Parse soap fault detail object
	 *
	 * @param \SoapFault $soapFault
	 * @return string
	 */
	public static function parseSoapFaultDetail(\SoapFault $soapFault)
	{
		$message = NULL;
		if (
			property_exists($soapFault, 'detail')
			&& property_exists($soapFault->detail, 'CcmdServiceException')
			&& property_exists($soapFault->detail->CcmdServiceException, 'description')
			&& property_exists($soapFault->detail->CcmdServiceException, 'fields')
			&& property_exists($soapFault->detail->CcmdServiceException, 'status')
		) {
            $message .= ' ' . $soapFault->detail->CcmdServiceException->description . ' (value: ' . $soapFault->detail->CcmdServiceException->fields . ', status: ' . $soapFault->detail->CcmdServiceException->status . ')';
		}

		return $message;

	}

	/**
	 * Counts the segmet with delay and max attempts
	 *
	 * @param \SmartFocus\API\CampaignManagementService $campaignManagementService
	 * @param int $id
	 * @param int $delay [optional][default=5]
	 * @param int $maxAttempts [optional][default=3]
	 * @return int or throw \Exception
	 */
	public static function segmentCount($campaignManagementService, $id, $delay = 5, $maxAttempts = 3)
	{
		$attempts	= 0;
	    $count		= 0;

		while ($count == 0 && $attempts < $maxAttempts) {

	        $attempts++;

	        $count = (int) $campaignManagementService->segmentationDistinctCount($id);

	        sleep($delay);

		}

	    return $count;

	}

	/**
	 * Set send date
	 *
	 * @param string $date
	 * @param string $timeZone [optional][default=UTC]
	 * @param int $minutes [optional][default=15]
	 * @return string
	 */
	public static function setSendDate($date, $timeZone = 'UTC', $minutes = 15)
	{

		$minutes        = intval($minutes);
		$dateTimeNow	= new \DateTime('now', new \DateTimeZone($timeZone));
	    $dateTime		= new \DateTime($date, new \DateTimeZone($timeZone));
		$dateTimeNow->modify(sprintf('%+d', $minutes) . ' minutes');
		if ($dateTime < $dateTimeNow) {
	        $date = $dateTimeNow->format('c');
		}

		return $date;

	}

}

?>