<?php
// Actions
define('ACTION_SCRIPT',				'Script');
define('ACTION_FTP_CONNECT',		'Connect to the ftp server %s.');
define('ACTION_FILE_DOWNLOAD',		'Download the members files.');
define('ACTION_FILE_PROCESS',		'Process the members files.');
define('ACTION_FILE_UPDATE',		'Delete the membes file and upload the log file.');
define('ACTION_API_INIT',			'Initialize the API.');
define('ACTION_CAMPAIGN_PROCESS',	'Process campaign %s.');
define('ACTION_MEMBER_IMPORT',		'Import members (file: %s).');
define('ACTION_SEGMENT_CREATE',		'Create segment (segment: %s).');
define('ACTION_MESSAGE_CLONE',		'Clone message (name: %s).');
define('ACTION_CAMPAIGN_CREATE',	'Create campaign (name: %s, sendDate: %s, messageId: %d, segmentId: %d).');
define('ACTION_CAMPAIGN_POST',		'Post campaign (id: %d).');
define('ACTION_RELOAD_CAMPAIGN',	'Reload campaign (name: %s).');
define('ACTION_REPROCESS_FILES',	'Reprocess downloaded files.');

define ('ACTIVATE_DEVEL_FTP',	1);
define ('ACTIVATE_TEST_FTP',	2);
define ('ACTIVATE_LIVE_FTP',	4);
define ('ACTIVATE_DEVEL_API',	8);
define ('ACTIVATE_TEST_API', 	16);
define ('ACTIVATE_LIVE_API', 	32);
define ('ACTIVATE_DEVEL_NMP', 	64);
define ('ACTIVATE_TEST_NMP', 	128);
define ('ACTIVATE_LIVE_NMP', 	256);

define('GET_ENVIRONMENT_TEST',	'TEST');
define('GET_ENVIRONMENT_PROD',	'PROD');

// Errors
define ('ERROR_MESSAGE_FOLDER_CREATE',					'Could not create folder (folder: %s).');
define ('ERROR_MESSAGE_FILE_DOWNLOAD', 					'Invalid downloaded members file (file: %s).');
define ('ERROR_MESSAGE_FILE_DOWNLOAD_EMPTY',			'No file was downloaded.');
define ('ERROR_MESSAGE_FILE_READ', 						'Unexpected error while reading the members file (file: %s, line: %d).');
define ('ERROR_MESSAGE_FILE_LINE_INVALID',				'Invalid line found (line: %d, reason: %s).');
define ('ERROR_MESSAGE_MEMBER_UPLOAD_UNKNOWN',			'Unknown error while uploading members (file: %s).');
define ('ERROR_MESSAGE_MEMBER_IMPORT_STATUS_GET',		'API error retrieving the members import status. Empty or no object returned. (id: %d).');
define ('ERROR_MESSAGE_MEMBER_IMPORT_STATUS',			'The members import has failed (status: %s).');
define ('ERROR_MESSAGE_MEMBER_IMPORT_STATUS_TIMEOUT',	'Timeout error for the members import (id: %d, status: %s, attempts: %d).');
define ('ERROR_MESSAGE_SEGMET_CREATE_UNKNOWN',			'Unknown error while creating new segment (segment: %s).');
define ('ERROR_MESSAGE_SEGMET_ADD_CRITERIA_UNKNOWN',	'Unknown error while adding demgraphic criteria to segment (id: %d).');
define ('ERROR_MESSAGE_MESSAGE_GET_UNKNOWN',			'Unknown error while retrieving the message (name: %s).');
define ('ERROR_MESSAGE_MESSAGE_CLONE_UNKNOWN',			'Unknown error while cloning the message (id: %d, name: %s).');
define ('ERROR_MESSAGE_CAMPAIGN_LIST_EMPTY',			'No campaigns were found (file: %s).');
define ('ERROR_MESSAGE_CAMPAIGN_EMPTY',					'Campaign does not exist in local files (campaign: %s).');
define ('ERROR_MESSAGE_CAMPAIGN_CREATE_UNKNOWN',		'Unknown error while creating campaign (reference: %s).');
define ('ERROR_MESSAGE_CAMPAIGN_POST_UNKNOWN',			'Unknown error while posting campaign (id: %d).');
define ('ERROR_MESSAGE_NMP_CONFIG',						'Wrong or missing nmp configuration.');
define ('ERROR_MESSAGE_NMP_RECIPIENTS',					'No recepients found.');
define ('ERROR_MESSAGE_NMP_SEND',						'Could not send notification. %s');

// Log
define ('LOG_MESSAGE_ACTIONS_SYNCRONIZE',		'---->Synchronize with SmartFocus.');
define ('LOG_MESSAGE_ACTIONS_REPROCESS',		'---->Reprocess files and synchronize with SmartFocus.');
define ('LOG_MESSAGE_ACTIONS_FULL',				'---->Download and process files and synchronize with SmartFocus.');

define ('LOG_MESSAGE_TIME_DOWNLOAD',			'<----Download file time: %s seconds (file: %s).');
define ('LOG_MESSAGE_TIME_DOWNLOAD_TOTAL',		'<----Download files total time: %s seconds.');
define ('LOG_MESSAGE_TIME_PROCESS',				'<----Process file time: %s seconds (file: %s).');
define ('LOG_MESSAGE_TIME_PROCESS_TOTAL',		'<----Process files total time: %s seconds.');

define ('LOG_MESSAGE_OK_FILE_PROCESS',			'<----Successfully processed the members file (file: %s, campaigns: %d).');
define ('LOG_MESSAGE_OK_FILE_PROCESS_ERROR',	'<----Processed the members file with wrong lines (file: %s, campaigns: %d, read: %d, skipped: %d, ok: %d).');
define ('LOG_MESSAGE_OK_MEMBER_IMPORT',			'<----Successfully imported the members file (id: %d, status: %s).');
define ('LOG_MESSAGE_OK_SEGMENT_CREATE',		'<----Successfully created the segment (id: %d, count: %d).');
define ('LOG_MESSAGE_OK_MESSAGE_CLONE',			'<----Successfully cloned the message (oldId: %d, newId: %d).');
define ('LOG_MESSAGE_OK_CAMPAIGN_CREATE',		'<----Successfully created the campaign (id: %d).');
define ('LOG_MESSAGE_OK_CAMPAIGN_POST',			'<----Successfully post the campaign (id: %d).');
?>