<?php
/*
 * Used combinations:
 * LIVE NMP + LIVE API + LIVE FTP: 292  
 * LIVE NMP + LIVE API + TEST FTP: 290
 * LIVE NMP + LIVE API + DEVEL FTP: 289
 */
 
if (!defined('BNPP_ENVIRONMENT')) {
	define ('BNPP_ENVIRONMENT', 292);
}

$config['applicationName'] = 'BNPP Shopping Deals'; 

//==========-------- FTP configuration --------==========//
$config['ftp'] = array(
	'ssl' 						=> TRUE,
	'server' 					=> 'xfer.bnpparibas.com',    
	'port' 						=> '2121',
	'username'					=> 'G04917CW',
	'password' 					=> 'oUtph0Tq',
	'folder'					=> '/OUT',
	'passiveMode'				=> TRUE,
);
// Override with TEST FTP configuration
/* NO LONGER TO BE USED
if ((BNPP_ENVIRONMENT & ACTIVATE_TEST_FTP) == ACTIVATE_TEST_FTP) {
	$config['ftp']	= array(
		'ssl' 						=> TRUE,    
		'server' 					=> 'xfer-qualif.bnpparibas.com',
		'port' 						=> '2121',
		'username'					=> 'G04917CW',
		'password' 					=> 'oUtph0Tq',
		'folder'					=> '/OUT',
		'passiveMode'				=> TRUE,		
	);
}
*/
// Override with DEVEL FTP configuration
if ((BNPP_ENVIRONMENT & ACTIVATE_DEVEL_FTP) == ACTIVATE_DEVEL_FTP) {
	$config['ftp']	= array(
		'ssl' 						=> FALSE,
		'server' 					=> 'webe.emv3.com',    
		'port' 						=> '21',
		'username'					=> 'bsima_valley_ftp_web',
		'password' 					=> 'bsima_valley2014!',
		'folder'					=> '/bnpp',
		'passiveMode'				=> FALSE,		
	);
}

$config['ftp'] += array (
	'connectionAttempts'		=> 3,
	'connectionDelay'			=> 1,
	'authenticationAttempts'	=> 3,
	'authenticationDelay'		=> 1,
	'downloadAttempts'			=> 3,
	'downloadDelay'				=> 1,
	'timeout'					=> 90,
	'timeoutIncrease'			=> 90,
);
 
//==========-------- API configuration --------==========//
$config['api']['partner_users'] = array('A' => 'emvvalley_bnppdeals_api');
$config['importStatusCountDelay']	= 5;
$config['importStatusGetDelay']		= 30;
$config['memberFileSeparator']		= '|';
$config['sendDateFormat']			= 'Y-m-d-H.i.s.u';
//$config['acceptReferenceList']		= array('MEMDSD05', 'MEMDSD06', 'MEMDSD07', 'MEMDSD08', 'BDMDSD05', 'BDMDSD06', 'BDMDSD07', 'BDMDSD08');
//$config['acceptReferenceList']		= array('MEMDSD05', 'MEMDSD06', 'MEMDSD07', 'MEMDSD08', 'BDMDSD05', 'BDMDSD06', 'BDMDSD07', 'BDMDSD08', 'BDOC0079', 'BDOC0080', 'BDOC0081', 'BDOC0082', 'BDOC0083', 'BDOC0084', 'MMASR001', 'MMASR002', 'MMASR003');
$config['acceptReferenceList']		= array(
	'MEMDSD05',
	'MEMDSD06',
	'MEMDSD07',
	'MEMDSD08',
	'BDMDSD05',
	'BDMDSD06',
	'BDMDSD07',
	'BDMDSD08',
	'BDOC0079',
	'BDOC0080',
	'BDOC0081',
	'BDOC0082',
	'BDOC0083',
	'BDOC0084',
	'MMASR001',
	'MMASR002',
	'MMASR003',
	'MMRDVR01',
	'MMRDVR02',
	'MMRDVR03',
	'MMRDVR04',
	'MMRDVR05',
	'MMRDVM01',
	'MMRDVM02',
	'MMRDVM03',
	'MMRDVM04',
	'MMRDVM05',
	'MMRDVP01',
	'MMRDVP02',
	'MMRDVP03',
	'MMRDVP04',
	'MMRDVP05',
	'BDOC0099',
	'BDOC0100',
	'NET11811',
	'NET11819',
);
$config['templateNamePattern']		= 'Template %s';
?>