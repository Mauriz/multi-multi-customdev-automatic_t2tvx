<?php
require 'bootstrap.php';

$foundFiles				= array();
$downloadedFiles		= array();
$processedFiles			= array();
$createdMessages		= array();
$foundFilesCount		= 0;
$downloadedFilesCount	= 0;
$processedFilesCount	= 0;
$createdMessagesCount	= 0;
$nmpParameters		= array(
	'encrypt'		=> $config['nmp']['encrypt'],
	'id'			=> $config['nmp']['id'],
	'random'		=> $config['nmp']['random'],
	'recipients'	=> $config['nmp']['recepients'],
	'from' 			=> $config['applicationName'],
	'subject' 		=> $config['nmp']['subjects']['NOTIFY'],
);

\SmartFocus\Util::log('[START]' . ACTION_SCRIPT);


// -----====================-----
// START FILE XML DOWNLOAD
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_FILE_XML_DOWNLOAD);
try {

	$sftp = new \SmartFocus\Sftp($config['sftp']['server'], $config['sftp']['username'], $config['sftp']['key']);	
	$sftp->connect();
	$foundFiles 		= (array) $sftp->searchFiles(NULL, $config['sftp']['folder']);
	$foundFilesCount 	= count($foundFiles);
	
	if ($foundFilesCount < 1) {
		$sftp->close();
		throw new \Exception(ERROR_MESSAGE_FILE_FOUND, EXCEPTION_CODE_ABORD);
	} 
	
	foreach ($foundFiles as $file) {
		try {
			$downloadedFilePath = $config['uploadFolder'] . '/' . $file['name'];			
			$sftp->downloadFile($file['path'], $downloadedFilePath);			
		    $downloadedFiles[] = array(				
				'localPath' 	=> $downloadedFilePath,
				'remoteFolder' 	=> $file['folder'],
				'remotePath'	=> $file['path'],
				'name' 			=> $file['name'],				
			);
		}
		catch (\Exception $exception) {
			\SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exception->getMessage());
		}		
	}
	$sftp->close();
	
}
catch (\Exception $exception) {
    \SmartFocus\Util::triggerError(
		$exception->getMessage(),
		\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
		ACTION_SCRIPT,
  		TRUE
	);
}      

$downloadedFilesCount = count($downloadedFiles);

if ($downloadedFilesCount < 1) {
	\SmartFocus\Util::triggerError(
		sprintf(ERROR_MESSAGE_FILE_DOWNLOAD, $foundFilesCount),
		\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ABORD),
		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, EXCEPTION_CODE_ABORD),
		ACTION_SCRIPT,
		TRUE
	);
}
// -----====================-----
// END FILE XML DOWNLOAD
// -----====================-----

// -----====================-----
// START FILE XML PROCESS
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_FILE_XML_PROCESS);

foreach ($downloadedFiles as $downloadedFile) {

	try {
		if (!is_readable($downloadedFile['localPath'])) {
	    	throw new \Exception(sprintf(ERROR_MESSAGE_FILE_XML_DOWNLOAD, $downloadedFile['localPath']), EXCEPTION_CODE_WARNING);
		}
		$downloadedFile['templateName'] = \SmartFocus\ABConcerts\Util::getTemplateName($downloadedFile['localPath']);
		$downloadedFile['templatePath'] = $config['templateFolder'] . '/' . $downloadedFile['templateName'];
		if (!is_readable($downloadedFile['templatePath'])) {
	    	throw new \Exception(sprintf(ERROR_MESSAGE_TEMPLATE, $downloadedFile['templatePath']), EXCEPTION_CODE_WARNING);
		}
		
	    $downloadedFile['message'] 	= strtolower(pathinfo($downloadedFile['name'], PATHINFO_FILENAME));	    
		$downloadedFile	= array_merge($downloadedFile, (array) \SmartFocus\ABConcerts\Util::parseXmlFile($config, $downloadedFile['localPath'], $downloadedFile['templateName']));
		
		$processedFiles[] = $downloadedFile;	    
	}
	catch (\Exception $exception) {
		$exceptionCode 		= $exception->getCode();
		$exceptionMessage 	= $exception->getMessage();
		if ($exceptionCode == EXCEPTION_CODE_WARNING) {
			\SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exceptionMessage);
		}
		else {
		    \SmartFocus\Util::triggerError(
				$exceptionMessage,
				\SmartFocus\Util::exceptionCodeToLogCode($exceptionCode),
				\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exceptionCode),
				ACTION_SCRIPT,
		  		TRUE
			);
		}
	}
		
}

$processedFilesCount = count($processedFiles);

if ($processedFilesCount < 1) {
	\SmartFocus\Util::triggerError(
		sprintf(ERROR_MESSAGE_FILE_PROCESS, $downloadedFilesCount),
		\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ERROR),
		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, EXCEPTION_CODE_ERROR),
		ACTION_SCRIPT,
		TRUE
	);
}

if ($processedFilesCount < $downloadedFilesCount) {
	\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_FILE_PROCESS_SOME, $processedFilesCount, $downloadedFilesCount));
}
else {
	\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_FILE_PROCESS_ALL, $downloadedFilesCount));	
}
// -----====================-----
// END FILE XML PROCESS
// -----====================-----

// -----====================-----
// START API INIT
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_API_INIT);
try {
	$partnerAuth 				= new \SmartFocus\API\PartnerPrivilegedAuthenticationService($config['api']['partner_users']['A'], $config['api']['partner_key1'], $config['api']['partner_key2']);
	$campaignManagementService  = new \SmartFocus\API\CampaignManagementService($partnerAuth);
}
catch (\Exception $exception) {
	\SmartFocus\Util::triggerError(
		$exception->getMessage(),
		\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
  		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
		ACTION_SCRIPT,
		TRUE
	);
}
// -----====================-----
// END API INIT
// -----====================-----

// -----====================-----
// START MESSAGE CREATE
// -----====================-----
foreach ($processedFiles as $processedFile) {
	try {

        \SmartFocus\Util::log('---->' . sprintf(ACTION_MESSAGE_CREATE, $processedFile['message']));

			// Create message
			$messageId = (int) $campaignManagementService->createEmailMessagebyObj(array(
				'name' 				=> $processedFile['message'],				
				'body' 				=> '[EMV HTMLPART]' . $processedFile['template'],
				'replyTo'			=> $processedFile['replyTo'],
				'replyToEmail'		=> $processedFile['replyToEmail'],
				'description' 		=> $processedFile['message'],
				'subject' 			=> $processedFile['subject'],
				'from' 				=> $processedFile['from'],
				'fromEmail' 		=> $config['sender'],
			));

			if (empty($messageId)) {
	            throw new \Exception(sprintf(ERROR_MESSAGE_MESSAGE_CREATE_UNKNOWN, $processedFile['message']), EXCEPTION_CODE_WARNING);
			}

			// Create mirror link
			$campaignManagementService->createAndAddMirrorUrl($messageId);

	        // Track all links
			try {
		        $campaignManagementService->trackAllLinks($messageId);
			}
		  	catch (\Exception $exception) {
	   			\SmartFocus\Util::log('<----' .  sprintf(LOG_MESSAGE_KO_MESSAGE_LINKS_TRACK, $messageId));
			}
			
			$processedFile['id'] = $messageId; 
			
			$createdMessages[] = $processedFile;

        \SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_MESSAGE_CREATE, $messageId, $processedFile['message']));
			    
	}
	catch (\SoapFault $soapFault) {
		\SmartFocus\Util::log('[' . LOG_CODE_API_ERROR . ']' . \SmartFocus\Util::exceptionToLogMessage($soapFault));
	}	
	catch (\Exception $exception) {
		\SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exception->getMessage());
	}	

}

$createdMessagesCount = count($createdMessages);

if ($createdMessagesCount < 1) {
	\SmartFocus\Util::triggerError(
		sprintf(ERROR_MESSAGE_MESSAGE_CREATE, $processedFilesCount),
		\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ERROR),
		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, EXCEPTION_CODE_ERROR),
		ACTION_SCRIPT,
		TRUE
	);
}

if ($createdMessagesCount < $processedFilesCount) {
	\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_MESSAGE_CREATE_SOME, $createdMessagesCount, $processedFilesCount));
}
else {
	\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_MESSAGE_CREATE_ALL, $processedFilesCount));	
}
// -----====================-----
// END MESSAGE CREATE
// -----====================-----

// -----====================-----
// START TREAT FILES
// -----====================-----
try {
	$sftp = new \SmartFocus\Sftp($config['sftp']['server'], $config['sftp']['username'], $config['sftp']['key']);	
	$sftp->connect();
	
	foreach ($createdMessages as $message) {		
		try {		
			//$sftp->renameFile($message['remotePath'], ($config['sftp']['folderTreated'] . '/ok_' . $message['name']));
			$sftp->renameFile($message['remotePath'], ($config['sftp']['folderTreated'] . '/' . $message['name']));
		}
		catch (\Exception $exception) {
			\SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exception->getMessage());
		}		
	}
	$sftp->close();	
}
catch (\Exception $exception) {
    \SmartFocus\Util::triggerError(
		$exception->getMessage(),
		\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
		ACTION_SCRIPT,
  		TRUE
	);
}
// -----====================-----
// END TREAT FILES
// -----====================-----

$report = \SmartFocus\Util::$log->getHistory();
$report[] = '[END]' . ACTION_SCRIPT;

\SmartFocus\Util::notify($nmpParameters, implode(PHP_EOL, $report));

\SmartFocus\Util::log('[END]' . ACTION_SCRIPT);

exit(LOG_CODE_SUCCESS);
?>