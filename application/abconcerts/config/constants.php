<?php
// Actions
define('ACTION_SCRIPT',				'Script');
define('ACTION_FILE_XML_DOWNLOAD',	'Download the xml files.');
define('ACTION_FILE_XML_PROCESS',	'Process the xml files.');
define('ACTION_API_INIT',			'Initialize the API.');
define('ACTION_MESSAGE_CREATE',		'Creating new message (name: %s).');

// Errors
define ('ERROR_MESSAGE_NMP_CONFIG',				'Wrong or missing nmp configuration.');
define ('ERROR_MESSAGE_NMP_RECIPIENTS',			'No recepients found.');
define ('ERROR_MESSAGE_NMP_SEND',				'Could not send notification. %s');
define ('ERROR_MESSAGE_FILE_DOWNLOAD',			'No files were downloaded from %d found on the SFTP server.');
define ('ERROR_MESSAGE_FILE_FOUND',				'No files found on the SFTP server.');
define ('ERROR_MESSAGE_FILE_XML_DOWNLOAD',		'Invalid downloaded xml file (file: %s).');
define ('ERROR_MESSAGE_XML_SUBJECT',			'No "subject" found (file: %s).');
define ('ERROR_MESSAGE_XML_FROM',				'No "from" found (file: %s).');
define ('ERROR_MESSAGE_XML_REPLY_TO_EMAIL',		'No "reply_to_email" found (file: %s).');
define ('ERROR_MESSAGE_TEMPLATE',				'Invalid template file (file: %s).');
define ('ERROR_MESSAGE_FILE_PROCESS',			'Could not process any file from %d donwloaded.');
define ('ERROR_MESSAGE_MESSAGE_CREATE_UNKNOWN',	'Could not create message (name: %s).');
define ('ERROR_MESSAGE_MESSAGE_CREATE',			'Could not create any message from the %d processed files.');

// Log
define ('LOG_MESSAGE_OK_FILE_PROCESS_ALL',		'<----Successfully processed all %d downloaded files.');
define ('LOG_MESSAGE_OK_FILE_PROCESS_SOME',		'<----Processed only %d files out of %d downloaded.');
define ('LOG_MESSAGE_OK_MESSAGE_CREATE',		'<----Successfully created a new message (id: %d, name: %s).');
define ('LOG_MESSAGE_OK_MESSAGE_CREATE_ALL',	'<----Successfully created all the messages from the %d processed files.');
define ('LOG_MESSAGE_OK_MESSAGE_CREATE_SOME',	'<----Created only %d messages from the %d processed files.');
define ('LOG_MESSAGE_KO_MESSAGE_LINKS_TRACK',	'<----There were no links to track (messageId: %d).');
?>