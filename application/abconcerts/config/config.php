<?php
$config['logFolder'] 			= '/opt/automatic/application/abconcerts/log';
$config['uploadFolder'] 		= '/opt/automatic/application/abconcerts/upload';
$config['templateFolder'] 		= '/opt/automatic/application/abconcerts/template';
$config['templateBlockFolder'] 	= '/opt/automatic/application/abconcerts/template/block';

$config['api']['partner_users'] 	= array('A' => 'ABApi'); //ABCONCERTS
$config['sender'] 					= 'email@abconcerts.cccampaigns.com';
$config['applicationName'] 			= 'AB Concerts';
$config['sftp'] = array(
    'server' 		=> 'sftpr.emv2.com',
    'port' 			=> '22',
    'username'		=> 'abconcerts_sftp',
    'folder'		=> 'xml/production',
    'folderTreated'	=> 'xml/production/treated',
);

$config['sftp']['key'] = <<<KEY
PuTTY-User-Key-File-2: ssh-rsa
Encryption: none
Comment: rsa-key-20141014
Public-Lines: 4
AAAAB3NzaC1yc2EAAAABJQAAAIEAxY5K4+yuZrtketT2WzjvN/vJFNFffSFQ+XyL
5vP+tCrYAT5o5NYtv5VfDZV8UAnbMZKBlEtYOkAJU62N97KBD7f5rIiEUQKywxHV
tsuw5saf6nCJv1B9ZLoJhNv+/z72+yUoiq3fKdqimSl7Yg8wKVhyx+dxJ0z5G+vd
SbtbCFk=
Private-Lines: 8
AAAAgQC64IwICWa0NLj+khl44tRzOkiXJuuf4T7CfLuw9KTT8S0xm+au9B1wCdZ0
oil8JP/HG+Jb2JG6j5oeuO4T0l5nJNSMn3B73/YO1tv8gHz9bLpmkMuXKVl/WcvF
7LofXmsHbcRd/sdcp0HEzfwrjaVLdQ05++TXjuvbK2epsSaSmQAAAEEA+bN9Dezu
o0Bc8kRVN+peadQWPaWvTeEw2Q7QS5nItIGLVL0Rnf9fpd7rv4Af8i3WORMJ/I1h
LW1llxGcdV3HjwAAAEEAyooP2Vr8rTPEJnzwPyUkr1c74vblPUw4OOjGEnviLfWp
yhg6RF1+M34mgZjoL394Yc4aOF+Rykk8WhxDBw79lwAAAEBH7pWp5D2Dbq647RfS
XyOjXOS7y5Gb/rqnnbi1o+2LB7Uh8TtH453RY6vF2t7hOdSSG2716UQpcG7Eiy5s
6Qc5
Private-MAC: 270b1278c78158f05cc847a573d29bde15430565
KEY;

$config['replyTo'] 				= 'Kevin Mc Mullan';
$config['persmailTemplateName'] = 'persmail';
$config['supportArtistLabel']	= array(
	'en' => '<br /><b>Support act: </b>',
	'fr' => '<br /><b>Première partie : </b>',
	'nl' => '<br /><b>Voorprogramma: </b>',
);