<?php namespace SmartFocus\ABConcerts;

/**
 * Utility class
 */
class Util extends \SmartFocus\Util {

	/**
	 * Get the html template name from xml path
	 *	 
	 * @param string $filePath
	 * @return string
	 */		
	public static function getTemplateName($filePath)
	{
		$templateName = strtolower(pathinfo($filePath, PATHINFO_FILENAME));
		
		preg_match('/_\d+/', $templateName, $matches);
		
		if (!empty($matches[0])) {
			$templateName = str_replace($matches[0], NULL, $templateName);
		}
		
		return $templateName . '.html';	
	}

	/**
	 * Parse xml file
	 *	 
	 * @param array $config
	 * @param string $filePath
	 * @param string $templateName	  	  	  	 
	 * @return bool
	 */	
	public static function parseXmlFile($config, $filePath, $templateName) 
	{
			    
        $xmlObject				= NULL;
        $xmlToJson				= NULL;
		$xmlToArray 			= array();
		$replacements 			= array();
		$templateFolder			= $config['templateFolder']; 
		$templateBlockFolder	= $config['templateBlockFolder'];
		
		try {			
			$xmlObject	= simplexml_load_file($filePath, 'SimpleXMLElement', LIBXML_NOCDATA);
			$xmlToArray	= self::xml2array($xmlObject, NULL, $xmlToArray);					
			if (!array_key_exists('concert.support.artist', $xmlToArray)) {
				$xmlToArray['concert.support.artist'] = NULL;
			}
			elseif (!empty($xmlToArray['concert.support.artist'])) {			
				if (substr($templateName, 0, strlen($config['persmailTemplateName'])) != $config['persmailTemplateName']) {
					$xmlToArray['concert.support.artist'] = '<br />' . $xmlToArray['concert.support.artist'];
				}
			}			
			if (!array_key_exists('concert.project.alt', $xmlToArray)) {
				$xmlToArray['concert.project.alt'] = NULL;
			}
			if (!array_key_exists('concert.project.image', $xmlToArray)) {
				$xmlToArray['concert.project.image'] = NULL;
			}
			else {
				$xmlToArray['concert.project.image'] = '<img alt="' . $xmlToArray['concert.project.alt'] . '" src="' . $xmlToArray['concert.project.image'] . '" width="70" style="max-width: 70px;padding-top: 10px;padding-bottom: 0;display: inline !important;vertical-align: baseline;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;height: auto !important;" class="mcnImage" />';
			}
		}
		catch (\Exception $exception) {			
			throw $exception;
		}
		
		if (empty($xmlToArray['headers.subject'])) {
			throw new \Exception(sprintf(ERROR_MESSAGE_XML_SUBJECT, $filePath), EXCEPTION_CODE_WARNING);
		}
		if (empty($xmlToArray['headers.from'])) {
			throw new \Exception(sprintf(ERROR_MESSAGE_XML_FROM, $filePath), EXCEPTION_CODE_WARNING);
		}
		if (empty($xmlToArray['headers.reply_to_email'])) {
			throw new \Exception(sprintf(ERROR_MESSAGE_XML_REPLY_TO_EMAIL, $filePath), EXCEPTION_CODE_WARNING);
		}
		// Set start time
		if (!empty($xmlToArray['concert.start_date']) && empty($xmlToArray['concert.start_time'])) {
			try {
				$dateTime = new \DateTime($xmlToArray['concert.start_date']);
				$xmlToArray['concert.start_time'] = $dateTime->format('H:i'); 
			}
			catch (\Exception $exception) {
				$xmlToArray['concert.start_time'] = '00:00';
			}			
		}
		
		$templatePath = $templateFolder . '/' . $templateName;
		
		$result = array(			
			'subject' 		=> $xmlToArray['headers.subject'],
			'from' 			=> $xmlToArray['headers.from'],
			'replyToEmail'	=> $xmlToArray['headers.reply_to_email'],
			'replyTo'		=> empty($xmlToArray['headers.reply_to_label']) ? $config['replyTo'] : $xmlToArray['headers.reply_to_label'],
			'template' 		=> file_get_contents($templatePath),
		);

		preg_match_all('/\*\|([^\*\|]*)\|\*/', $result['template'], $matches);
		
		if (!empty($matches[0]) && !empty($matches[1])) {
		
			$language = empty($xmlToArray['language']) ? 'en' : $xmlToArray['language'];								
			self::setLocale($language);
					
			foreach (array_unique($matches[1]) as $index => $tag) {
			
				if (array_key_exists($index, $matches[0])) {
				
					if (array_key_exists($tag, $xmlToArray)) {
					
						switch ($tag) {
						
							case 'ab_tips':
							case 'news':
							case 'announced':						
							case 'this_week':							
							case 'poll':
							
								$templateBlockWrapper		= NULL;
								$templateBlock				= NULL;
								$templateBlockList			= NULL;
								$templateBlockPath 			= $templateBlockFolder . '/' . $tag . '-' . $templateName;								
								$templateBlockWrapperPath	= $templateBlockFolder . '/' . $tag . '-wrapper-' . $templateName;

								if (
									is_array($xmlToArray[$tag])
									&& !empty($xmlToArray[$tag])
									&& is_readable($templateBlockWrapperPath)
									&& is_readable($templateBlockPath)
								) {
																								
									$templateBlockWrapper	= file_get_contents($templateBlockWrapperPath);									
									$templateBlockWrapper 	= str_replace('*|concert.title|*', (empty($xmlToArray['concert.title']) ? NULL : strtoupper($xmlToArray['concert.title'])), $templateBlockWrapper);
																		
									// Replace all template blocks									
									foreach ($xmlToArray[$tag] as $element) {										
										$templateBlockList .= self::replaceTemplateBlock($templateBlockPath, $element);									
									}
									
									$templateBlockWrapper = str_replace($matches[0][$index], $templateBlockList, $templateBlockWrapper);
									$templateBlockWrapper = self::replaceTemplateBlockWrapper($templateBlockWrapper, $xmlToArray);
									
								}
								
								$result['template'] = str_replace($matches[0][$index], $templateBlockWrapper, $result['template']);
								
								break;
								
							case 'recent_news':
															
								$templateBlockWrapper		= NULL;
								$templateBlock				= NULL;
								$templateBlockList			= NULL;								
								$templateBlockWrapperPath	= $templateBlockFolder . '/' . $tag . '-wrapper-' . $templateName;
								
								if (
									is_array($xmlToArray[$tag])
									&& !empty($xmlToArray[$tag])
									&& is_readable($templateBlockWrapperPath)									
								) {
									
									$templateBlockWrapper = file_get_contents($templateBlockWrapperPath);

									// Replace all template blocks																		
									foreach ($xmlToArray[$tag] as $group) {									
										foreach ($group as $type => $element) {										
											$templateBlockPath = $templateBlockFolder . '/' . $tag . '-' . $type . '-' . $templateName;											
											if (is_readable($templateBlockPath)) {										
												$templateBlockList .= self::replaceTemplateBlock($templateBlockPath, $element);
											}																				
										}									
									}
									
									$templateBlockWrapper = str_replace($matches[0][$index], $templateBlockList, $templateBlockWrapper);									
									$templateBlockWrapper = self::replaceTemplateBlockWrapper($templateBlockWrapper, $xmlToArray);
									
								}
															
								$result['template'] = str_replace($matches[0][$index], $templateBlockWrapper, $result['template']);								
								
								break;							
								
							case 'concert.start_date':
							case 'concert.presale':																												
								try {								
									$dateTime = new \DateTime($xmlToArray[$tag]);
									if ($tag == 'concert.start_date') {									
										$replacement = utf8_encode(ucwords(strtolower(strftime("%a %d %b %Y", $dateTime->getTimestamp()))));										
									}
									else {
										$replacement = utf8_encode(ucwords(strtolower(strftime("%a %d %b %Y %H:%M", $dateTime->getTimestamp()))));
									}
									$result['template'] = str_replace($matches[0][$index], $replacement, $result['template']);									
								}
								catch (\Exception $exception) {
									$result['template'] = str_replace($matches[0][$index], $xmlToArray[$tag], $result['template']);									
								}
								break;
																
							case 'concert.title':
								$result['template'] = str_replace($matches[0][$index], strtoupper($xmlToArray[$tag]), $result['template']);
								break;
								
							case 'concert.support.artist':
								if (substr($templateName, 0, strlen($config['persmailTemplateName'])) == $config['persmailTemplateName']) {
									$replacement = empty($xmlToArray[$tag]) ? NULL : ($config['supportArtistLabel'][$language] . $xmlToArray[$tag]);
								}
								else {
									$replacement = $xmlToArray[$tag]; 
								}
								$result['template'] = str_replace($matches[0][$index], $replacement, $result['template']);								
								break;
																
							default:
								$result['template'] = str_replace($matches[0][$index], $xmlToArray[$tag], $result['template']);						
								break;
																				
						}					
					
					}
					else {											
						$result['template'] = str_replace($matches[0][$index], NULL, $result['template']);
					}
				
				}
								
			}
			
		}		
		
		return $result;					
	
	}
	
	public static function replateProjectTag($projectTag, &$container)
	{
		$projectTagAlt 		= $projectTag. '.alt';
		$projectTagSrc 		= $projectTag. '.logo';
		
		if (!array_key_exists($projectTagAlt, $container)) {
			$container[$projectTagAlt] = NULL;
		}		
		if (!array_key_exists($projectTagSrc, $container)) {
			$container[$projectTagSrc] = NULL;
		}
		else {
			$container[$projectTagSrc] = '<br /><img alt="' . $container[$projectTagAlt] . '" src="' . $container[$projectTagSrc] . '" width="53" style="max-width: 53px;padding-top: 10px;padding-bottom: 0;display: inline !important;margin-top: -10px;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;vertical-align: bottom;height: auto !important;" class="mcnImage" />';
		}
		
		return TRUE;	
	}	
	
	public static function replaceTemplateBlock($templateBlockPath, $tagContainer)
	{	
		$templateBlock = file_get_contents($templateBlockPath);												
		preg_match_all('/\*\|([^\*\|]*)\|\*/', $templateBlock, $matchesBlock);												
		if (!empty($matchesBlock[0]) && !empty($matchesBlock[1])) {
			foreach (array_unique($matchesBlock[1]) as $indexBlock => $tagBlock) {
				if (array_key_exists($indexBlock, $matchesBlock[0])) {														
					if (in_array(substr($tagBlock, -5), array('.date', '_date'))) {
						try {								
							$dateTime 			= new \DateTime($tagContainer[$tagBlock]);														
							$tagContainer[$tagBlock] = utf8_encode(ucwords(strtolower(strftime("%d %b %Y", $dateTime->getTimestamp()))));
						}
						catch (\Exception $exception) {																							
						}															
					}																													
					$replacementBlock 	= array_key_exists($tagBlock, $tagContainer) ? $tagContainer[$tagBlock] : NULL;
					$templateBlock 		= str_replace($matchesBlock[0][$indexBlock], $replacementBlock, $templateBlock);
				}			
			}										
		}
		return $templateBlock;		
	}
	
	public static function replaceTemplateBlockWrapper(&$templateBlockWrapper, $tagContainer, $skipTag = NULL)
	{
		preg_match_all('/\*\|([^\*\|]*)\|\*/', $templateBlockWrapper, $matchesBlockWrapper);
		if (!empty($matchesBlockWrapper[0]) && !empty($matchesBlockWrapper[1])) {
			foreach (array_unique($matchesBlockWrapper[1]) as $indexBlockWrapper => $tagBlockWrapper) {				
				if (self::skipTag($tagBlockWrapper, $skipTag) && array_key_exists($indexBlockWrapper, $matchesBlockWrapper[0])) {
					$replacementBlockWrapper	= array_key_exists($tagBlockWrapper, $tagContainer) ? $tagContainer[$tagBlockWrapper] : NULL;
					$templateBlockWrapper		= str_replace($matchesBlockWrapper[0][$indexBlockWrapper], $replacementBlockWrapper, $templateBlockWrapper);												
				}
			}
		}		
		return $templateBlockWrapper;	
	}
	
	public static function setLocale($language)
	{			
		$localeUnix		= 'en_US';
		$localeWindows	= 'ENGLISH';
		switch ($language) {
			case 'fr':
				$localeUnix		= 'fr_BE';
				$localeWindows 	= 'FRENCH';
				break;
			case 'nl':
				$localeUnix		= 'nl_BE';
				$localeWindows 	= 'nld_bel';
				break;
			default:
				break;																				
		}
		
		setlocale(LC_ALL, $localeUnix, $localeWindows);
		
		return TRUE;
	}
	
	public static function skipTag($tag, $skipTag = NULL) {

		$skip = TRUE;
		
		if (!empty($skipTag)) {
			if (is_array($skipTag) && in_array($tag, $skipTag)) {
				$skip = FALSE;				
			}
			elseif (is_string($skipTag) && $tag == $skipTag) {
				$skip = FALSE;
			}
		}
		
		return $skip;	
	}

	/**
	 * Xml to array method
	 *	 
	 * @param SimpleXMLElement $xmlObject	 
	 * @param string $indexTrail [optional]
	 * @param array &$array [optional] 	  	 
	 * @return array
	 */	
	public static function xml2array($xmlObject, $indexTrail = NULL, &$array = array())
	{

	    foreach ((array) $xmlObject as $index => $node) {
	    	
			if (!empty($indexTrail)) {
				$index = $indexTrail . '.' . $index;				
			}
			
			if (is_object($node)) {
				if (empty($node)) {
					$array[$index] = NULL;
				}
				else {
					switch ($index) {
						case 'ab_tips.tip':
						case 'news.news_item':
						case 'announced.concert':
						case 'this_week.concert':						
						case 'poll.news_item':						
							$subArray = array();
							$array[$indexTrail][] = self::xml2array($node, $index, $subArray);
							break;
						case 'recent_news.news_item':
							$subArray = array();							
							$array[$indexTrail][] = array('one' => self::xml2array($node, ($index . '.center'), $subArray));
							break;
						default:
							self::xml2array($node, $index, $array);
							break;												
					}
				}
			}
			elseif (is_array($node)) {				
				switch ($index) {
					case 'concert.support.artist':
						$array[$index] = implode(' + ', $node);						
						break;				
					case 'concert.genres.genre':
						$array[$index] = implode(', ', $node);						
						break;						
					case 'announced.concert':
					case 'this_week.concert':						
					case 'ab_tips.tip':
						$subArray = array();						
						foreach ($node as $element) {
							$array[$indexTrail][] = self::xml2array($element, $index, $subArray);
							$subArray = array(); 
						}
						foreach ($array[$indexTrail] as &$element) {
							self::replateProjectTag($index . '.project', $element);							
						}												
						break;					
					case 'news.news_item':					
					case 'poll.news_item':
						$subArray = array();
						foreach ($node as $element) {
							$array[$indexTrail][] = self::xml2array($element, $index, $subArray);
						}
						break;
					case 'recent_news.news_item':
						$subArrayLeft 	= array();
						$subArrayRight 	= array();
						$subArrayCenter	= array();												
						for ($i = 0; $i < count($node); $i+=2) {							
							if (array_key_exists(($i), $node)) {							
								if (array_key_exists(($i + 1), $node)) {								
									$array[$indexTrail][] = array('two' =>  
										self::xml2array($node[$i], ($index . '.left'), $subArrayLeft) +
										self::xml2array($node[$i + 1], ($index . '.right'), $subArrayRight));							
								}
								else {
									$array[$indexTrail][] = array('one' => self::xml2array($node[$i], ($index . '.center'), $subArrayCenter));								
								}							
							}
						}					
						break;												
					default:
						continue;					
						break;						
				
				}
			}
			else {
				$array[$index] = $node;
				$array[$index] = str_replace('& ', '&amp; ', $array[$index]);
			}
	    	
		}
		
	    return $array;
	    
	}

}

?>