<?php namespace SmartFocus\Alphab;

/**
 * Utility class
 */
class Util extends \SmartFocus\Util 
{

	/**
	 * Check the members import status with delay and timeout
	 *
	 * @param \SmartFocus\API\BatchMemberService $batchMemberService
	 * @param int $id
	 * @param array $statusList
	 * @param int $delay [optional][default=30]
	 * @param int $maxAttempts [optional][default=10]
	 * @return string or throw \Exception
	 */
	public static function checkMembersImportStatus($batchMemberService, $id, $statusList, $delay = 30, $maxAttempts = 10)
	{

		$repeat 	= TRUE;
		$attempts	= 0;
	    $status		= NULL;

		while ($repeat && $attempts < $maxAttempts) {

	        $attempts++;

	    	$response = $batchMemberService->getUploadStatus($id);
			
			// SF API bug (empty response)
			if (empty($response) || empty($response->status)) {
	            self::log('[ERROR]' . sprintf(ERROR_MESSAGE_MEMBER_IMPORT_STATUS_GET, $id));	            	            
	            $debug = $batchMemberService->getDebug();
	            if (!empty($debug->request)) {
	            	self::log($debug->request, FALSE, FALSE);
				}
				if (!empty($debug->response)) {
					self::log($debug->response, FALSE, FALSE);
				}	            
			}
			else {
		        $status = strval($response->status);
	
				if (in_array($status, $statusList['success'])) {
					$repeat = FALSE;
					if (in_array($status, $statusList['partial_success'])) {
						try {
							$response = $batchMemberService->getLogFile($id);
						}
						catch (\Exception $e){
							self::log('[ERROR]' . sprintf(ERROR_MESSAGE_MEMBER_IMPORT_LOG_GET, $id));
						}
						preg_match_all('/Total logical records (?:read|rejected|discarded)\:\s+(\d{1,})/', $response, $matches);
						if (count($matches[1]) >= 3) {
							if ($matches[1][0] <= $matches[1][1] + $matches[1][2]) {
								self::log('<----' . WARNING_MESSAGE_MEMBER_IMPORT_REJECTED);
								throw new \Exception(sprintf(ERROR_MESSAGE_MEMBER_IMPORT_STATUS, $status));
							}
							else {
								self::log('<----' . sprintf(MESSAGE_MEMBER_IMPORT_PARTIALLY_REJECTED, $matches[1][1] + $matches[1][2], $matches[1][0]));
							}
						}
					}
				}
				elseif (in_array($status, $statusList['pending'])) {
					sleep($delay);
				}
				else {
		            $repeat = FALSE;
		            throw new \Exception(sprintf(ERROR_MESSAGE_MEMBER_IMPORT_STATUS, $status));
				}			
			}

			if ($repeat && $attempts == $maxAttempts) {
	        	$repeat = FALSE;
	            throw new \Exception(sprintf(ERROR_MESSAGE_MEMBER_IMPORT_STATUS_TIMEOUT, $id, $status, $attempts));
			}

		}

		return $status;

	}
	
	/**
	 * Download method
	 * 
	 * @return array	 	 
	 */	 	
	public static function download($subFolder = NULL)
	{
	
		$result = array(
			'foundFiles' 			=> array(),
			'foundFilesCount' 		=> 0,
			'downloadedFiles' 		=> array(),
			'downloadedFilesCount'	=> 0,
		);

		try {
			$downloadedFolderPath = self::$config['uploadFolder'] . $subFolder;			
      
			if (!is_dir($downloadedFolderPath) && !@mkdir($downloadedFolderPath)) {
				throw new \Exception(sprintf(ERROR_MESSAGE_FILE_DOWNLOAD_FOLDER_CREATE, $downloadedFolderPath), EXCEPTION_CODE_ABORD);
			}

			$sftp = new \SmartFocus\Sftp(self::$config['sftp']['server'], self::$config['sftp']['username'], self::$config['sftp']['key']);	
			$sftp->connect();

			//$result['foundFiles']		= (array) $sftp->searchFiles(NULL, self::$config['sftp']['folder']);
			$result['foundFiles']		= (array) $sftp->searchFiles('.csv.zip', self::$config['sftp']['folder']);						
      
			$result['foundFilesCount'] 	= count($result['foundFiles']);

			if ($result['foundFilesCount'] < 1) {
				$sftp->close();
				throw new \Exception(ERROR_MESSAGE_FILE_FOUND, EXCEPTION_CODE_ABORD);
			} 

      
			foreach ($result['foundFiles'] as $k => $file) {
        
				try {
					$downloadedFilePath = $downloadedFolderPath . '/' . $file['name'];
					$sftp->downloadFile($file['path'], $downloadedFilePath);			
				    $result['downloadedFiles'][substr($file['name'],0,4)] = array(				
						'localPath' 	=> $downloadedFilePath,
						'localFolder' 	=> $downloadedFolderPath,
						'remoteFolder' 	=> $file['folder'],
						'remotePath'	=> $file['path'],
						'name' 			=> $file['name'],
						'realName'		=> $file['name'],
						'realLocalPath'	=> $downloadedFilePath,
						'unzip'			=> (strtolower(pathinfo($file['name'], PATHINFO_EXTENSION)) == 'zip' ? TRUE : FALSE),
					);
				}
				catch (\Exception $exception) {
					\SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exception->getMessage());
				}		
			}
			$sftp->close();
		}
		catch (\Exception $exception) {
		    \SmartFocus\Util::triggerError(
				$exception->getMessage(),
				\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
				\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), $exception->getCode()),
				ACTION_SCRIPT,
		  		TRUE
			);
		}      
		
		$result['downloadedFilesCount'] = count($result['downloadedFiles']);
    
		if ($result['downloadedFilesCount'] < 1) {
			\SmartFocus\Util::triggerError(
				sprintf(ERROR_MESSAGE_FILE_DOWNLOAD, $result['foundFilesCount']),
				\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ABORD),
				\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), EXCEPTION_CODE_ABORD),
				ACTION_SCRIPT,
				TRUE
			);
		}

		return $result;
	
	}
	
	/**
	 * Builds a full file path
	 * 
	 * @param string $folder	 	 
	 * @param string $fileName 	 
	 * @return string 	 
	 */	 	
	public static function filePath($folder, $fileName)
	{
		$folder = $folder == '/' ? $folder : $folder . '/';
		return $folder . $fileName;  
	}
	
	/**
	 * Generate members mapping
	 *
	 * @param array $fieldList
	 * @return array
	 */
	public static function generateMembersMapping($fieldList)
	{

		$mapping	= array();
		$column 	= 0;
		$foundEmail = FALSE;

		foreach ($fieldList as $field) {
			
			$fieldName = self::$config['memberImportKeyMap'][strtoupper($field)][0];
			$fieldType = self::$config['memberImportKeyMap'][strtoupper($field)][1];

			if ($fieldName == 'EMAIL') {
            	$foundEmail = TRUE;
			}
			
		    $column++;
		    
			$mappingField = array(
				'colNum' 	=> $column,
				'toReplace'	=> $fieldName == self::$config['memberImportMergeKey'] ? FALSE : TRUE,
				'fieldName' => $fieldName,
			);

			switch ($fieldType) {
				case 'DATE':
					if (!empty(self::$config['memberImportKeyMap'][strtoupper($field)][2])) {
						$mappingField['dateFormat'] = self::$config['memberImportKeyMap'][strtoupper($field)][2];	
					}										
					break;
				default:
					break;
			
			}
			
			$mapping['column'][] = $mappingField;
			
		}

		if (!$foundEmail) {
			$mapping['column'][] = array(
				'colNum' 		=> 0,
				'toReplace'		=> FALSE,
				'fieldName' 	=> 'EMAIL',
				'defaultValue'	=> 'dummy.email@example.com',
			);
		}

		return $mapping;

	}
	
	public static function getNmpParameters()
	{
		return array(
			'encrypt'		=> self::$config['nmp']['encrypt'],
			'id'			=> self::$config['nmp']['id'],
			'random'		=> self::$config['nmp']['random'],
			'recipients'	=> is_array(self::$config['nmp']['recepients']) ? self::$config['nmp']['recepients'] : array(self::$config['nmp']['recepients']),
			'from' 			=> self::$config['applicationName'],
			'subject' 		=> self::$config['nmp']['subjects']['NOTIFY'],
		);	
	}				
	
	/**
	 * Members import method
	 *  
	 * @param \SmartFocus\API\BatchMemberService $batchMemberService
	 * @param array $processedFiles 	 	 	 
	 * @return array 	 
	 */	 	
	public static function import($batchMemberService, $processedFiles)
	{
  
		$result = array(
			'importedFiles' 		=> array(),
			'importedFilesCount'	=> 0,
		);
			
		foreach ($processedFiles as $processedFile) {		
			try {				
				$result['importedFiles'][] = self::importFile($batchMemberService, $processedFile);								
			}			
			catch (\Exception $exception) {
				\SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exception->getMessage());				
			}
		}
		
		$result['importedFilesCount'] = count($result['importedFiles']);
		
		if ($result['importedFilesCount'] < 1) {
			\SmartFocus\Util::triggerError(
				sprintf(ERROR_MESSAGE_FILE_PROCESS, count($processedFiles)),
				\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ABORD),
				\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), EXCEPTION_CODE_ABORD),
				ACTION_SCRIPT,
				TRUE
			);
		}		
		
		return $result;		
	}


	/**
	 * File import method
	 * 
	 * @param \SmartFocus\API\BatchMemberService $batchMemberService
	 * @param array $processedFiles 	 	 	 
	 * @return array 	 
	 */	 	
	public static function importFile($batchMemberService, $processedFile)
	{
		$result = array('mapping' => array(), 'importId' => NULL, 'importStatus' => NULL,) + $processedFile;
		
		self::log('---->' . sprintf(ACTION_MEMBER_IMPORT, $processedFile['name']));
		
		$result['mapping'] = self::generateMembersMapping($processedFile['header']); 		
		$result['importId'] = (int) $batchMemberService->uploadFileMerge(file_get_contents($processedFile['localPath']), 'SmartFocus API Test ' . $processedFile['name'], self::$config['memberFileSeparator'], $result['mapping'], 'LOWER(' . self::$config['memberImportMergeKey'] . ')', 'UTF-8', TRUE, 'yyyy-MM-dd');
		//$result['importId'] = (int) $batchMemberService->uploadFileMerge(file_get_contents($processedFile['localPath']), 'SmartFocus API Test', self::$config['memberFileSeparator'], $result['mapping'], 'LOWER(' . self::$config['memberImportMergeKey'] . ')', 'UTF-8', TRUE, 'yyyy-MM-dd');

		if (empty($result['importId'])) {
	        throw new \Exception(sprintf(ERROR_MESSAGE_MEMBER_UPLOAD_UNKNOWN, $processedFile['name']), EXCEPTION_CODE_ERROR);
		}
		
		self::log('---->' . sprintf(ACTION_MEMBER_IMPORT_STATUS, $processedFile['name']));
					
		$result['importStatus'] = self::checkMembersImportStatus($batchMemberService, $result['importId'], self::$config['importStatuses']);
		
		self::log('<----' . sprintf(ACTION_MEMBER_IMPORT_STATUS_RESULT, $result['importStatus']));
		
		return $result;
	}
		
	/**
	 * Process downloaded file method
	 * 	 
	 * @param array $downloadedFiles
	 * @return array 	 
	 */	
	public static function processDownload($downloadedFiles)
	{                                                       
  
		$result = array(
			'processedFiles' 		=> array(),
			'processedFilesCount'	=> 0,
		);

		$zip = new \ZipArchive;
			
    /*
    Files must be elaborated in this order:
    */
    
    $statusArr[] = 1001;
    $statusArr[] = 1002;
    $statusArr[] = 1003;
    $statusArr[] = 1000;

		foreach ($statusArr as $status) {		
        
        if(!isset($downloadedFiles[$status])) continue;

        try {				
          $result['processedFiles'][] = self::processDownloadedFile($downloadedFiles[$status], $zip);								
        }			
        catch (\Exception $exception) {
          \SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exception->getMessage());				
        }
          
     }
        
		$result['processedFilesCount'] = count($result['processedFiles']);
    
		if ($result['processedFilesCount'] < 1) {
			\SmartFocus\Util::triggerError(
				sprintf(ERROR_MESSAGE_FILE_PROCESS, count($downloadedFiles)),
				\SmartFocus\Util::exceptionCodeToLogCode(EXCEPTION_CODE_ABORD),
				\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), EXCEPTION_CODE_ABORD),
				ACTION_SCRIPT,
				TRUE
			);
		}		
		
		return $result;		
	}
	
	/**
	 * Process downloaded file method
	 * 	 
	 * @param array $downloadedFile 	 	 	 
	 * @param ZipArchive $zip 	 
	 * @param array $header [optional] 	  	  	  	 
	 * @return array 	 
	 */	
	public static function processDownloadedFile($downloadedFile, $zip, $header = array())
	{
    
		$error 			= array();			
		$result 		= array('lines' => 0, 'header' => $header,) + $downloadedFile;		
		$fileHandle 	= NULL;		
		$mergeKeyIndex	= NULL;
		
		self::log('---->' . sprintf(ACTION_FILE_PROCESS, $downloadedFile['name']));

		if (!empty($result['header'])) {			
			self::processHeader($result['header'], $downloadedFile, $mergeKeyIndex);
		}
		
		if (!is_readable($downloadedFile['localPath'])) {			
			throw new \Exception(sprintf(ERROR_MESSAGE_FILE_DOWNLOAD_CSV, $downloadedFile['localPath']), EXCEPTION_CODE_WARNING);
		}
				
		if ($downloadedFile['unzip']) {
		
			self::log('---->' . sprintf(ACTION_FILE_UNZIP, $downloadedFile['name']));
		
			if ($zip->open($downloadedFile['localPath']) !== TRUE) {
				throw new \Exception(sprintf(ERROR_MESSAGE_FILE_DOWNLOAD_ZIP, $downloadedFile['localPath']), EXCEPTION_CODE_WARNING);
			}			
			$extractedFileName = pathinfo(pathinfo($downloadedFile['name'], PATHINFO_FILENAME), PATHINFO_FILENAME) . '.csv';
			if (!@$zip->extractTo($downloadedFile['localFolder'])) {
				throw new \Exception(sprintf(ERROR_MESSAGE_FILE_DOWNLOAD_ZIP_EXTRACT, $downloadedFile['localPath']), EXCEPTION_CODE_WARNING);
			}
  			$zip->close();
  			
  			$result['realName'] 		= $result['name'];
  			$result['realLocalPath'] 	= $result['localPath'];
  			$result['name'] 			= $extractedFileName;
  			$result['localPath'] 		= $result['localFolder'] . '/' . $result['name'];
  			
		}
			
		if (!is_readable($result['localPath']) || !($fileHandle = fopen($result['localPath'], 'r'))) {			
			throw new \Exception(sprintf(ERROR_MESSAGE_FILE_DOWNLOAD_CSV, $result['localPath']), EXCEPTION_CODE_WARNING);
		}

    // Added 18/08/2016 by customer's request 
    // For files named 1000_Player_Master_Data_PLC_ only we need to add two fields

    if( strpos( $result['name'] , '1000_Player_Master_Data_PLC_' ) === 0  ){
        self::$config['memberImportKeyMap']['SECOND_LAST_NAME'][] = 'SECOND_LAST_NAME';
        self::$config['memberImportKeyMap']['SECOND_LAST_NAME'][] = 'TEXT';
        self::$config['memberImportKeyMap']['SECOND_LAST_NAME'][] = '255';
        self::$config['memberImportKeyMap']['REGISTRATION_IP'][] = 'REGISTRATION_IP';
        self::$config['memberImportKeyMap']['REGISTRATION_IP'][] = 'TEXT';
        self::$config['memberImportKeyMap']['REGISTRATION_IP'][] = '255';
    }
    
		while (($line = fgetcsv($fileHandle, 8192, self::$config['memberFileSeparator'])) !== FALSE) {
			
			$result['lines']++;

			if (empty($result['header'])) {
				$result['header'] = $line;
				self::processHeader($result['header'], $downloadedFile, $mergeKeyIndex);
				continue;
			}
			
			self::processLine($line, $result['lines'], $result['header'], $mergeKeyIndex, $downloadedFile);							
			
		}
		
		if (!feof($fileHandle)) {			
	        throw new \Exception(sprintf(ERROR_MESSAGE_FILE_READ, $result['localPath'], $result['lines']['read']), EXCEPTION_CODE_WARNING);
		}
		fclose($fileHandle);
			
		$result['lines'] = max($result['lines'] - 1, 0);
		
		return $result;
	
	}

	/**
	 * Process header method
	 * 	 
	 * @param array $header 	 	 	 
	 * @param array $downloadedFile 	 
	 * @param int &$mergeKeyIndex 	  	  	  	 
	 * @return bool 	 
	 */	
	public static function processHeader($header, $downloadedFile, &$mergeKeyIndex)
	{
    
		foreach ($header as $columnHeader) {				
			if (!array_key_exists(strtoupper($columnHeader), self::$config['memberImportKeyMap'])) {
				throw new \Exception(sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $downloadedFile['localPath'], 0, 'The column header "' . $columnHeader . '" is not configured for the import'), EXCEPTION_CODE_WARNING);
			}				
		}
		
		if (($mergeKeyIndex = array_search(self::$config['memberImportMergeKey'], $header)) === FALSE) {			
			throw new \Exception(sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $downloadedFile['localPath'], 0, 'No "' . self::$config['memberImportMergeKey'] . '" (merge key) column header found'), EXCEPTION_CODE_WARNING);
		}
		
		return TRUE;	
	}

	/**
	 * Process line method
	 * 	 
	 * @param array $line 	 	 	 
	 * @param int $lineCount
	 * @param array $header
	 * @param int $mergeKeyIndex
	 * @param array $downloadedFile 	  	  	  	 
	 * @return bool 	 
	 */
	public static function processLine($line, $lineCount, $header, $mergeKeyIndex, $downloadedFile) 
	{
		$expectedFieldsCount	= count($header);
		$fieldsCount 			= count($line);
		
		if ($expectedFieldsCount != $fieldsCount) {
			throw new \Exception(sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $downloadedFile['localPath'], $lineCount, 'Invalid fields found: ' . $fieldsCount . '/' . $expectedFieldsCount), EXCEPTION_CODE_WARNING);                				
		}
		
		if (empty($line[$mergeKeyIndex])) {
			throw new \Exception(sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $downloadedFile['localPath'], $lineCount, 'Empty merge key found'), EXCEPTION_CODE_WARNING);                				
		}
		
		foreach ($line as $index => $value) {
		
			$rule = self::$config['memberImportKeyMap'][strtoupper($header[$index])];
			
			switch ($rule[1]) {
				case 'TEXT':
					if (mb_strlen($value) > $rule[2]) {
						throw new \Exception(sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $downloadedFile['localPath'], $lineCount, 'The maximum length of "' . $rule[2] . '" characters for the field "' . $header[$index] . '" is exceeded'), EXCEPTION_CODE_WARNING);
					}
					break;
				case 'NUMBER':
					if (!empty($value) && !is_numeric($value)) {
						throw new \Exception(sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $downloadedFile['localPath'], $lineCount, 'Invalid number found in the field "' . $header[$index] . '"'), EXCEPTION_CODE_WARNING);
					}
					break;
				case 'DATE':				
					if (!empty($value)) {
						try {
							$dateTime = new \DateTime($value);
						}
						catch (\Exception $exception) {
							throw new \Exception(sprintf(ERROR_MESSAGE_FILE_LINE_INVALID, $downloadedFile['localPath'], $lineCount, 'Invalid date found in the field "' . $header[$index] . '"'), EXCEPTION_CODE_WARNING);
						}
					}
					break;
				default:
					break;
			}
			
		}
		
		return TRUE;
				
	}	

	/**
	 * Treat files method
	 *  	 
	 * @param array $importedFiles 	 	 	 
	 * @return array 	 
	 */	 	
	public static function treat($importedFiles)
	{

		$result = array(
			'treatedFiles' 		=> array(),
			'treatedFilesCount'	=> 0,
		);
		
		try {
		
			$sftp = new \SmartFocus\Sftp(self::$config['sftp']['server'], self::$config['sftp']['username'], self::$config['sftp']['key']);	
			$sftp->connect();
			
			foreach ($importedFiles as $importedFile) {		
				try {				
					$result['treatedFiles'][] = self::treatFile($sftp, $importedFile);								
				}			
				catch (\Exception $exception) {
					\SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exception->getMessage());				
				}
			}
			
			$sftp->close();
			
			$result['treatedFilesCount'] = count($result['treatedFiles']);
			
			if ($result['treatedFilesCount'] < 1) {
				throw new \Exception(sprintf(ERROR_MESSAGE_FILE_TREAT, count($importedFiles)), EXCEPTION_CODE_ABORD);
			}
						
		}
		catch (\Exception $exception) {
		    \SmartFocus\Util::triggerError(
				$exception->getMessage(),
				\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
				\SmartFocus\Util::exceptionCodeToNmpSubject(self::getNmpParameters(), $exception->getCode()),
				ACTION_SCRIPT,
		  		TRUE
			);
		}
					
		return $result;		
	}
	
	/**
	 * Treat file method
	 * 	 
	 * @param \SmartFocus\Sftp $sftp
	 * @param array $importedFile 	  	 	 	 
	 * @return array 	 
	 */	 	
	public static function treatFile($sftp, $importedFile)
	{
		self::log('---->' . sprintf(ACTION_FILE_TREAT, $importedFile['name']));
		
		return $sftp->renameFile($importedFile['remotePath'], self::$config['sftp']['folderTreated'] . '/' . $importedFile['realName']);
	}
	
	/**
	 * Check upload slots
	 * 
	 * @param \SmartFocus\API\BatchMemberService $batchMemberService	 	 
	 * @param int $delay [optional][default=5]
	 * @param int $maxAttempts [optional][default=5]
	 * @return int 	  	  	 
	 */	 	
	public static function uploadSlots($batchMemberService, $delay = 5, $maxAttempts = 5)
	{
		$repeat 	= TRUE;
		$attempts	= 0;
		$slots 		= MAX_CONCURRENT_UPLOADS_PER_ACCOUNT;	    

		while ($repeat && $attempts < $maxAttempts) {

	        $attempts++;

	    	$response = $batchMemberService->getUploadSummaryList();
			
			// SF API bug (empty response)
			if (empty($response) || empty($response->uploadSummaries->uploadSummaryEntity)) {				            	        	            
	            self::log('[ERROR]' . ERROR_MESSAGE_MEMBER_UPLOAD_SUMMARY_LIST);	            	            
	            $debug = $batchMemberService->getDebug();
	            if (!empty($debug->request)) {
	            	self::log($debug->request, FALSE, FALSE);
				}
				if (!empty($debug->response)) {
					self::log($debug->response, FALSE, FALSE);
				}	            
			}
			else {
				
				foreach ($response->uploadSummaries->uploadSummaryEntity as $upload) {
					$upload = (array) $upload;
					if (!empty($upload['status']) && in_array($upload['status'], self::$config['importStatusesSummary']['finished'])) {
						continue;					
					}
					$slots--;
					if (empty($upload['status']) || !in_array($upload['status'], self::$config['importStatusesSummary']['finished'])) {
						$status = empty($upload['status']) ? NULL : empty($upload['status']); 
						self::log('[WARNING]' . sprintf(ERROR_MESSAGE_MEMBER_UPLOAD_SUMMARY_LIST_STATUS, $status));						
					}
				}
				if ($slots > 1) {
					$repeat = FALSE;
				}
				else {
					sleep($delay);
				}			
			}

			if ($repeat && $attempts == $maxAttempts) {
	        	$repeat = FALSE;
	            throw new \Exception(sprintf(ERROR_MESSAGE_MEMBER_UPLOAD_SUMMARY_LIST_TIMEOUT, $attempts));
	            return 0;
			}

		}

		return $slots;	
	}	

}
?>