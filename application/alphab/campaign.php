<?php
require 'bootstrap.php';

\SmartFocus\Util::log('[START]' . ACTION_SCRIPT);

// START FILES DONWLOAD
\SmartFocus\Util::log('---->' . ACTION_FILES_DOWNLOAD);
$downloadedResult = \SmartFocus\Alphab\Util::download('/' . date('Ymd'));

// START FILES PROCESS
\SmartFocus\Util::log('---->' . ACTION_FILES_PROCESS);
$processedResult = \SmartFocus\Alphab\Util::processDownload($downloadedResult['downloadedFiles']);

// START API INIT
\SmartFocus\Util::log('---->' . ACTION_API_INIT);


try {
	$partnerAuth 				= new \SmartFocus\API\PartnerPrivilegedAuthenticationService($config['api']['partner_users']['A'], $config['api']['partner_key1'], $config['api']['partner_key2']);
	$batchMemberService 		= new \SmartFocus\API\BatchMemberService($partnerAuth);
	$campaignManagementService  = new \SmartFocus\API\CampaignManagementService($partnerAuth);
}
catch (\Exception $exception) {
  
	\SmartFocus\Util::triggerError(
		$exception->getMessage(),
		\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
  		\SmartFocus\Util::exceptionCodeToNmpSubject(\SmartFocus\Alphab\Util::getNmpParameters(), $exception->getCode()),
		ACTION_SCRIPT,
		TRUE
	);
  
}

$uploadSlots = \SmartFocus\Alphab\Util::uploadSlots($batchMemberService);

// -----====================-----
// START MEMBERS IMPORT
// -----====================-----

$importResult = \SmartFocus\Alphab\Util::import($batchMemberService, $processedResult['processedFiles']);

// -----====================-----
// END MEMBERS IMPORT
// -----====================-----

// -----====================-----
// START FILES TREAT
// -----====================-----
$treatResult = \SmartFocus\Alphab\Util::treat($importResult['importedFiles']);
// -----====================-----
// END FILES TREAT
// -----====================-----

$report = \SmartFocus\Util::$log->getHistory();
$report[] = '[END]' . ACTION_SCRIPT;

\SmartFocus\Util::notify(\SmartFocus\Alphab\Util::getNmpParameters(), implode(PHP_EOL, $report));

\SmartFocus\Util::log('[END]' . ACTION_SCRIPT);

exit(LOG_CODE_SUCCESS);
?>