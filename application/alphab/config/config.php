<?php
$config['logFolder']        = '/opt/automatic/application/alphab/log';
$config['uploadFolder']       = '/opt/automatic/application/alphab/upload';
$config['api']['partner_users']   = array('A' => 'jfarinas_alphab_api');
$config['applicationName']      = 'Alpha Bet';
//$config['nmp']['recepients']    = 'marcello.stillitano@smartfocus.com';
$config['nmp']['recepients'] = 'rahel.chowdhury@smartfocus.com';
$config['memberFileSeparator']    = ',';
$config['memberImportMergeKey']   = 'PLAYER_ID';
$config['memberImportKeyMap']   = array(
  'ADDRESS'           => array('ADDRESS', 'TEXT', 255),
  'AFFILIATE_NAME'      => array('AFFILIATE_NAME', 'TEXT', 255),
  'AMOUNT'          => array('AMOUNT', 'NUMBER', 19, 2),
  'AVERAGE_STAKE'       => array('AVERAGE_STAKE', 'NUMBER', 19, 2),
  'BALANCE_BONUS_AMOUNT'    => array('BALANCE_BONUS_AMOUNT', 'NUMBER', 19, 2),
  'BALANCE_CPOINTS_NUMBER'  => array('BALANCE_CPOINTS_NUMBER', 'NUMBER', 19, 2), // MUST BE NUMBER(19,3) 
  'BALANCE_REAL_AMOUNT'   => array('BALANCE_REAL_AMOUNT', 'NUMBER', 19, 2),
  'BLOCK_BAN'         => array('BLOCK_BAN', 'TEXT', 30),  
  'BONUS_CONVERTED_AMOUNT'  => array('BONUS_CONVERTED_AMOUNT', 'NUMBER', 19, 2),
  'BONUS_CONVERTED_TOTAL'   => array('BONUS_CONVERTED_TOTAL', 'NUMBER', 19, 2),     
  'BONUS_END'         => array('BONUS_END', 'DATE', 'yyyy-MM-dd HH24:mi:ss'),   
  'BONUS_ID'          => array('BONUS_ID', 'NUMBER', 19),
  'BONUS_OFFER_CODE'      => array('BONUS_OFFER_CODE', 'TEXT', 50),
  'BONUS_PROGRESS_PERCENTAGE' => array('BONUS_PROGRESS_PERCENTAGE', 'NUMBER', 5, 2), // MUST BE NUMBER(3)     
  'BONUS_RECEIVED'      => array('BONUS_RECEIVED', 'DATE', 'yyyy-MM-dd HH24:mi:ss'),    
  'BONUS_RECEIVED_AMOUNT'   => array('BONUS_RECEIVED_AMOUNT', 'NUMBER', 19, 2),
  'BONUS_RECEIVED_TOTAL'    => array('BONUS_RECEIVED_TOTAL', 'NUMBER', 19, 2),
  'BONUS_TARGET_AMOUNT'     => array('BONUS_TARGET_AMOUNT', 'NUMBER', 19, 2),
  'BONUS_VALID_UNTIL'     => array('BONUS_VALID_UNTIL', 'DATE', 'yyyy-MM-dd HH24:mi:ss'), 
  'BOOKED'          => array('BOOKED', 'DATE', 'yyyy-MM-dd HH24:mi:ss'),    
  'CASINO'          => array('CASINO', 'TEXT', 10),
  'CASINO_BAN'        => array('CASINO_BAN', 'TEXT', 30),
  'CHARGEBACK'        => array('CHARGEBACK', 'DATE', 'yyyy-MM-dd HH24:mi:ss'),
  'CHARGEBACK_REVERSE'    => array('CHARGEBACK_REVERSE', 'DATE', 'yyyy-MM-dd HH24:mi:ss'),    
  'CITY'            => array('CITY', 'TEXT', 255),
  'CONFIRMED_IDENTITY'    => array('CONFIRMED_IDENTITY', 'TEXT', 20),
  'COUNTRY'           => array('COUNTRY', 'TEXT', 50),
  'CUR_TC_ACCEPTED'     => array('CUR_TC_ACCEPTED', 'TEXT', 20),
  'CURRENCY'          => array('CURRENCY', 'TEXT', 3),
  'DATE_OF_BIRTH'       => array('DATEOFBIRTH', 'DATE'),
  'DOUBLE_OPT_IN'       => array('DOUBLE_OPT_IN', 'TEXT', 30),
  'EMAIL_ADDRESS'       => array('EMAIL', 'TEXT', 255),
  'EXCLUDED_FROM_BONUS'   => array('EXCLUDED_FROM_BONUS', 'TEXT', 30),
  'FIRST_LOGIN_IP'      => array('FIRST_LOGIN_IP', 'TEXT', 40),
  'FIRST_NAME'        => array('FIRSTNAME', 'TEXT', 64),
  'FIRST_PAYIN'       => array('FIRST_PAYIN', 'DATE'),  
  'FRAUD_BAN'         => array('FRAUD_BAN', 'TEXT', 30),
  'GAME_DATE_HOUR'      => array('GAME_DATE_HOUR', 'TEXT', 30),
  'GAME_IS_FAVORITE'      => array('GAME_IS_FAVOURITE', 'TEXT', 25),
  'GAME_NAME'         => array('GAME_NAME', 'TEXT', 250),
  'GAME_TYPE'         => array('GAME_TYPE', 'TEXT', 255),
  'GAMES_STARTED'       => array('GAMES_STARTED', 'NUMBER', 19),
  'GENDER'          => array('GENDER', 'TEXT', 15),
  'GROSS_WINNINGS_BONUS'    => array('GROSS_WINNINGS_BONUS', 'NUMBER', 19, 2),
  'GROSS_WINNINGS_REAL'   => array('GROSS_WINNINGS_REAL', 'NUMBER', 19, 2),
  'GROSS_WINNINGS_REAL_TOTAL' => array('GROSS_WINNINGS_REAL_TOTAL', 'NUMBER', 19, 2),
  'HAS_ACTIVE_BONUS'      => array('HAS_ACTIVE_BONUS', 'TEXT', 20),
  'INTERNAL_REFERENCE'    => array('INTERNAL_REFERENCE', 'TEXT', 255),
  'IS_CLOSED'         => array('IS_CLOSED', 'TEXT', 30),  
  'LAST_DESKTOP_CHANNEL'    => array('LAST_DESKTOP_CHANNEL', 'DATE', 'yyyy-MM-dd HH24:mi:ss'),  
  'LAST_GAME_PLAYED'      => array('LAST_GAME', 'DATE'),    
  'LAST_LOGIN_DATE'       => array('LAST_LOGIN', 'DATE', 'yyyy-MM-dd HH24:mi:ss'),      
  'LAST_LOGIN_IP'       => array('LAST_LOGIN_IP', 'TEXT', 40),  
  'LAST_MOBILE_CHANNEL'     => array('LAST_MOBILE_CHANNEL', 'DATE', 'yyyy-MM-dd HH24:mi:ss'),       
  'LAST_TERMINAL_CHANNEL'   => array('LAST_TERMINAL_CHANNEL', 'DATE', 'yyyy-MM-dd HH24:mi:ss'), 
  'LAST_NAME'         => array('LASTNAME', 'TEXT', 64),
  'LAST_PAYIN'        => array('LATEST_PAYIN', 'DATE'),
  'LATEST_PAYOUT'       => array('LATEST_PAYOUT', 'DATE'),
  'LATEST_PLAYER_ACTIVITY'  => array('LATEST_PLAYER_ACTIVITY', 'DATE', 'yyyy-MM-dd HH24:mi:ss'),      
  'LOGIN_ATTEMPTS_EXPIRED'  => array('LOGIN_ATTEMPTS_EXPIRED', 'TEXT', 30), 
  'LOSS_LIMIT'        => array('LOSS_LIMIT', 'TEXT', 255),  
  'MAX_STAKE_BONUS'       => array('MAX_STAKE_BONUS', 'NUMBER', 19, 2),
  'MAX_STAKE_REAL'      => array('MAX_STAKE_REAL', 'NUMBER', 19, 2),
  'MAX_WINNINGS_BONUS'    => array('MAX_WINNINGS_BONUS', 'NUMBER', 19, 2),
  'MAX_WINNINGS_REAL'     => array('MAX_WINNINGS_REAL', 'NUMBER', 19, 2),
  'MIN_STAKE_BONUS'       => array('MIN_STAKE_BONUS', 'NUMBER', 19, 2),
  'MIN_STAKE_REAL'      => array('MIN_STAKE_REAL', 'NUMBER', 19, 2),
  'MIN_WINNINGS_BONUS'    => array('MIN_WINNINGS_BONUS', 'NUMBER', 19, 2),
  'MIN_WINNINGS_REAL'     => array('MIN_WINNINGS_REAL', 'NUMBER', 19, 2),
  'MOBILE_NUMBER'       => array('MOBILE_NUMBER', 'TEXT', 255), 
  'NGR_24_HOURS'        => array('NGR_24_HOURS', 'NUMBER', 19), // MUST BE NUMBER (19,2)    
  'NGR_30_DAYS'         => array('NGR_30_DAYS', 'NUMBER', 19, 2),
  'NGR_TOTAL'         => array('NGR_TOTAL', 'NUMBER', 19, 2), 
  'PAID_IN_TOTAL'       => array('PAID_IN_TOTAL', 'NUMBER', 19, 2),
  'PAID_OUT_TOTAL'      => array('PAID_OUT_TOTAL', 'NUMBER', 19, 2),  
  'PAYMENT_DIRECTION'     => array('PAYMENT_DIRECTION', 'TEXT', 15),  
  'PAYMENT_METHOD'      => array('PAYMENT_METHOD', 'TEXT', 255),  
  'PAYOUT_HOLD'         => array('PAYOUT_HOLD', 'NUMBER', 19, 2), 
  'PERMANENT_BAN'       => array('PERMANENT_BAN', 'TEXT', 30),
  'PHONE_NUMBER'        => array('PHONE_NUMBER', 'TEXT', 255),    
  'PLANNED_PAYOUTS'       => array('PLANNED_PAYOUTS', 'NUMBER', 19),
  'PLAYER_AGE_CAT'      => array('PLAYER_AGE_CAT', 'TEXT', 50), 
  'PLAYER_ID'         => array('PLAYER_ID', 'NUMBER', 19, 2), // MUST BE NUMBER(19) 
  'PLAYER_PAYIN_CAT'      => array('PLAYER_PAYIN_CAT', 'TEXT', 50),
  'PLAYER_VALUE_CAT'      => array('PLAYER_VALUE_CAT', 'TEXT', 50),
  'PSP_REFERENCE'       => array('PSP_REFERENCE', 'TEXT', 255), 
  'RECEIVES_NEWSLETTER'     => array('RECEIVES_NEWSLETTER', 'TEXT', 3),
  'RECEIVES_PROMOTION'    => array('RECEIVES_PROMOTION', 'TEXT', 3),      
  'REGISTRATION_DATE'     => array('REGISTRATION_DATE', 'DATE', 'yyyy-MM-dd HH24:mi:ss'), 
  'REQUESTED'         => array('REQUESTED', 'TEXT', 19), // MUST BE DATETIME    
  'SELECTED_LANGUAGE'     => array('SELECTED_LANGUAGE', 'TEXT', 20),  
  'SELF_BAN'          => array('SELF_BAN', 'TEXT', 30), 
  'STAKE_REAL_AMOUNT'     => array('STAKE_REAL_AMOUNT', 'NUMBER', 19, 2),
  'STAKES_BONUS'        => array('STAKES_BONUS', 'NUMBER', 19, 2),
  'STAKES_REAL'         => array('STAKES_REAL', 'NUMBER', 19, 2), 
  'STATUS'          => array('STATUS', 'TEXT', 50), 
  'TEMP_PASSWORD'       => array('TEMP_PASSWORD', 'TEXT', 30),  
  'TIME_OUT'          => array('TIME_OUT', 'TEXT', 30), 
  'TITLE'           => array('TITLE', 'TEXT', 24),
  'USERNAME'          => array('USERNAME', 'TEXT', 255),  
  'ZIP'             => array('ZIP', 'TEXT', 255),   
);
$config['sftp'] = array(
    'server'    => 'ftpe.emv3.com',
    'port'      => '22',
    'username'    => 'ftpuser',
    'folder'    => '/local00/ftproot/alphab_sftp/synchro/incoming',
    'folderTreated' => '/local00/ftproot/alphab_sftp/synchro/incoming/treated',
);
$config['sftp']['key'] = <<<KEY
PuTTY-User-Key-File-2: ssh-rsa
Encryption: none
Comment: imported-openssh-key
Public-Lines: 6
AAAAB3NzaC1yc2EAAAABIwAAAQEAzUqN9qZGI1oRItXDwVP+Nw3IGLPe9YZASMZG
c/tB+ig3x8wUV2KnLhcPEcAGVGkfaxA6Qpt8p6aEw++1AsIrDmGKqg8wZDxs90uS
lyFF8IjyD9rXtZcfT1REzrDzCpvhKGLGYvvaFBJ0AOLhb7h59tHsW1O3f12le2KU
6DXRoP1cA8blbPQOpZbtKwu4sKnWH11z0oe1ZqQ6P+OQprvbjzPlVQmiKQb1C3ID
EokXYAR+v5wLPylqGITXuQl55RIxVsuJeeKZblPTB/f7b5qxLcYGjURVkHJ5YH+m
4xiGErg5bbX9xuUkJ3JvF8I9NGquHDQ6W0+CsjqJX2cb3BCjyw==
Private-Lines: 14
AAABAECFJU2EtvUq73FZIEQTFWHCgLdOeUXZvG6ktw6CKq203lS8e2vrziRtpaZ+
LeADyAu0pJiXRG80VZyqbBbPT1xKiqp5zWFU0coQb+Za6hhljee5zsQK7JVGXsSe
Az3ZOCKis2DwEVbD+JKQcry2UjehD8TubN7i6t2io9PzqEiJk/MLRQ9NPf1PYvMc
f+tgwLAJ3MLCW0ssU12hVVYtCjzEfVqMgUYftH1MJLFEXIczzzS3haXcXsIWh27i
mtM8SCv5e8aYHJb3RYCFyCDYNwN3oGTibAvzvVh9wPeymxYPaBMYuChVGkOdS8gq
jsHmkizXGMp8hginJI6c7csQ9AMAAACBAPErQH0bcUCTt4qV31y1I+tpRJ4B0YPe
N9VvBYn2BxtY5ST/j8kJL2Y4urTI5nfh8BNnEFvd0g6gTbFabpVXxzgnghlg2muA
QBeCf740LSmf2iYmnIpZseT6KnXA3mvDnqa4VyX94pW/moGuk7PaS8Ts2XTZf65e
9KWDRbPX4AmPAAAAgQDZ6nnSZqROphFWSdGtsYCc4xC6TcAmU2r+eGhUQWtCjDql
2EZsV/5QidxN5w7tcsypgkPAUZot92PUiWzJYYsq0enO/jt13y+BSaAGR0k1milt
sUu5P0SuTMUQ9tNEIC5e1bMZdzmX7cwq1QQ3fMzpiWiN5kOeN721XC4WT4ZMBQAA
AIEAkWHSAgQVQtpFeiY0H1hPO23XbFq8eCrLOjuRLrAmY7YTgNjD+aAVWkskcfP5
CYlta5YhGXMUPfDrV3QE74MnKdMPHWSNOrllM/zXwH4SXR+oJGJhyVLpgr/aM1Q8
3GIK1ekQNEdhSJB4DYJUWfJ6MfFpVXVByXP5l2xv8B+fAgI=
Private-MAC: d45820867f200f3dbb833ac8b97b2d9dd485b006
KEY;
// Members import statuses
$config['importStatuses']['total_success']    = array('DONE');
$config['importStatuses']['partial_success']  = array('DONE WITH ERROR(S)', 'DONE WITH ERRORS');
