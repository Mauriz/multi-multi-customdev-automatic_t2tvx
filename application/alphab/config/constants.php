<?php
// Actions
define('ACTION_API_INIT',						'Initialize the API.');
define('ACTION_SCRIPT', 						'Script');
define('ACTION_FILES_DOWNLOAD', 				'Download files');
define('ACTION_FILES_PROCESS', 					'Process files');
define('ACTION_FILE_UNZIP', 					'Unzip file: %s');
define('ACTION_FILE_PROCESS', 					'Process file: %s');
define('ACTION_FILE_TREAT', 					'Treat file: %s');
define('ACTION_MEMBER_IMPORT',					'Import members (file: %s).');
define('ACTION_MEMBER_IMPORT_STATUS',			'Check the import status (file: %s).');
define('ACTION_MEMBER_IMPORT_STATUS_RESULT',	'Import status: %s.');

// Environment
define ('ENVIRONMENT_TEST', 'TEST');
define ('ENVIRONMENT_LIVE', 'LIVE');

// Errors
define ('ERROR_MESSAGE_FILE_DOWNLOAD',						'No files were downloaded from %d found on the SFTP server.');
define ('ERROR_MESSAGE_FILE_DOWNLOAD_CSV',					'Invalid downloaded csv file (file: %s).');
define ('ERROR_MESSAGE_FILE_DOWNLOAD_ZIP',					'Invalid downloaded zip file (file: %s).');
define ('ERROR_MESSAGE_FILE_DOWNLOAD_ZIP_EXTRACT',			'Could not unzip download file: $s.');
define ('ERROR_MESSAGE_FILE_DOWNLOAD_FOLDER_CREATE',		'Could not create download folder: $s.');
define ('ERROR_MESSAGE_FILE_FOUND',							'No files found on the SFTP server.');
define ('ERROR_MESSAGE_FILE_LINE_INVALID',					'Invalid line found (file: %s, line: %d, reason: %s).');
define ('ERROR_MESSAGE_FILE_LINES_INVALID',					'Multiple invalid lines found. (file: %s, read: %d, ok: %d, skipped: %d).');
define ('ERROR_MESSAGE_FILE_PROCESS',						'No files were processed from %d downloaded.');
define ('ERROR_MESSAGE_FILE_READ',							'Unexpected error while reading the file (file: %s, line: %d).');
define ('ERROR_MESSAGE_MEMBER_IMPORT',						'No files were imported from %d processed.');
define ('ERROR_MESSAGE_MEMBER_IMPORT_STATUS',				'The members import has failed (status: %s).');
define ('ERROR_MESSAGE_MEMBER_IMPORT_STATUS_GET',			'API error retrieving the members import status. Empty or no object returned. (id: %d).');
define ('ERROR_MESSAGE_MEMBER_IMPORT_STATUS_TIMEOUT',		'Timeout error for the members import (id: %d, status: %s, attempts: %d).');
define ('ERROR_MESSAGE_MEMBER_UPLOAD_SUMMARY_LIST',			'API error retrieving the members upload summary list. Empty or no object returned.');
define ('ERROR_MESSAGE_MEMBER_UPLOAD_SUMMARY_LIST_STATUS',	'Invalid upload summary list status found: %s.');
define ('ERROR_MESSAGE_MEMBER_UPLOAD_SUMMARY_LIST_TIMEOUT',	'Timeout error for the upload permission check (attempts: %d).');
define ('ERROR_MESSAGE_MEMBER_UPLOAD_UNKNOWN',				'Unknown error while uploading members (file: %s).');
define ('ERROR_MESSAGE_MEMBER_IMPORT_LOG_GET',				'API error retrieving the members import log. Empty or no object returned. (id: %d).');
define ('WARNING_MESSAGE_MEMBER_IMPORT_REJECTED',			'All lines in the file were rejected.');
define ('MESSAGE_MEMBER_IMPORT_PARTIALLY_REJECTED',			'%d lines out of %d were rejected in the upload.');


define ('ERROR_MESSAGE_NMP_CONFIG',				'Wrong or missing nmp configuration.');
define ('ERROR_MESSAGE_NMP_RECIPIENTS',			'No recepients found.');
define ('ERROR_MESSAGE_NMP_SEND',				'Could not send notification. %s');

// Log
define ('LOG_MESSAGE_OK_FILE_PROCESS_ALL',		'<----Successfully processed all %d downloaded files.');
define ('LOG_MESSAGE_OK_FILE_PROCESS_SOME',		'<----Processed only %d files out of %d downloaded.');
define ('LOG_MESSAGE_OK_MEMBER_IMPORT',			'<----Successfully imported the members file (id: %d, status: %s).');

// File process status
define ('FILE_PROCESS_STATUS_OK',	'ok');
define ('FILE_PROCESS_STATUS_KO',	'ko');
