<?php namespace SmartFocus\Spir;

/**
 * Ftp class
 *
 * @dependencies: \SmartFocus\Ftp
 */
class Ftp extends \SmartFocus\Ftp
{

	/**
	 * Download folder method
	 *
	 * @param string $destinationFolderParent
	 * @param string $sourceFolder
	 * @param array $sourceFileList
	 * @return array
	 */
    public function downloadFolder($destinationFolderParent, $sourceFolder, $sourceFileList)
	{

		// Reconnect if necessary.
		if (empty($this->connection) || !$this->authenticated) {
			$this->connect(true);
		}

        // -----===== START Check and/or create destication folder tree =====-----
        \SmartFocus\Util::log('---->Check and/or create the destination folder tree under folder ' . $destinationFolderParent. '.');

        $destinationFolderTree	= array($destinationFolderParent);
		$destinationFolderTree	= array_merge($destinationFolderTree, explode('/', substr($sourceFolder, strlen($this->root))));
        $destinationFolder 		= NULL;

		foreach ($destinationFolderTree as $folder) {

            $destinationFolder .= (empty($destinationFolder) ? NULL : '/') . $folder;

			if (!is_dir($destinationFolder) && !mkdir($destinationFolder)){
                $this->throwException('Failed to create the destination folder ' . $destinationFolder . '.', EXCEPTION_CODE_ERROR, FALSE);
			}
			elseif (!is_writable($destinationFolder)) {
                $this->throwException('The destination folder ' . $destinationFolder . ' is not writable.', EXCEPTION_CODE_ERROR, FALSE);
			}

		}
        \SmartFocus\Util::log('<----Successfully checked and/or created the destination folder tree ' . $destinationFolder. '.');
        // -----===== END Check and/or create destication folder tree =====-----

        // -----===== START Donwload files =====-----
		\SmartFocus\Util::log('---->Download files from the campaign folder ' . $sourceFolder . ' to destination folder ' . $destinationFolder .'.');

		$campaign = array(
			'localFolder' 		=> $destinationFolder,
			'localFileList'		=> array(),
			'remoteFolder' 		=> $sourceFolder,
			'remoteFileList'	=> $sourceFileList,
		);

		foreach ($sourceFileList as $sourceFileType => $sourceFilePath) {

			$destinationFileName	= pathinfo($sourceFilePath, PATHINFO_BASENAME);
			$destinationFilePath	= $destinationFolder . '/' . $destinationFileName;

			$downloaded			= FALSE;
			$downloadAttempts	= 0;

			while (!$downloaded && $downloadAttempts < $this->maxAttempts) {

		        $downloadAttempts++;

				if ($downloadAttempts > 1) {
					\SmartFocus\Util::log('---->Retrying to donwload the file ' . $sourceFilePath. ' (attempt: ' . $downloadAttempts . ').');
	             	sleep($this->config['downloadDelay']);
				}

		        $downloaded = @ftp_get($this->connection, $destinationFilePath, $sourceFilePath, FTP_BINARY);

				if (!$downloaded) {
		            \SmartFocus\Util::log('<----Failed to donwload the file ' . $sourceFilePath. ' (attempt: ' . $downloadAttempts . ').');
				}

			}

			if (!$downloaded) {
                $this->throwException('Failed to donwload the file: ' . $sourceFilePath. ' (attempts: ' . $downloadAttempts . ').', EXCEPTION_CODE_ERROR, FALSE);
			}
			elseif ($sourceFileType == 'message') {
                file_put_contents($destinationFilePath, str_replace('&amp;&amp;&amp;','&&&', file_get_contents($destinationFilePath)));
			}

            $campaign['localFileList'][$sourceFileType] = $destinationFilePath;

            \SmartFocus\Util::log('<----Successfully donwloaded the file: ' . $sourceFilePath. ' (attempts: ' . $downloadAttempts . ').');

		}
        // -----===== END Donwload files =====-----

		return $campaign;

	}

	/**
	 * Scan daily folder method
	 *
	 * @param string $dailyFolder
	 * @return array or throw \Exception
	 */
	public function scanDailyFolder($dailyFolder)
	{

		// Reconnect if necessary.
		if (empty($this->connection) || !$this->authenticated) {
			$this->connect(TRUE);
		}

        \SmartFocus\Util::log('---->Scan the ftp server for new campaign folders within the daily folder ' . $dailyFolder . '.');

		// Fix for the ftp_nlist function (returns always FALSE if the server is behind a firewall)
		ftp_pasv($this->connection, TRUE);

		// Check if folder exists.
		if (!in_array($dailyFolder, ftp_nlist($this->connection, $this->root))) {
			$this->throwException('Could not find the daily folder ' . $dailyFolder . '.', EXCEPTION_CODE_ABORD);
		}

		// Set working folder.
		if (!@ftp_chdir($this->connection, $dailyFolder)) {
            $this->throwException('Failed to set the working daily folder ' . $dailyFolder . '.', EXCEPTION_CODE_FTP_ERROR, TRUE);
		}

		// Scan for items.
		$itemList = (array) ftp_nlist($this->connection, $dailyFolder);
		if (empty($itemList)) {
            $this->throwException('Empty daily folder ' . $dailyFolder . '.', EXCEPTION_CODE_WARNING);
		}

        // Scan for folders.
		$folderList = array();
		foreach ($itemList as $item) {
			if (@ftp_chdir($this->connection, $item)) {
		        $folderList[] = $item;
				@ftp_chdir($this->connection, $this->config['folder']);
			}
		}
		if (empty($folderList)) {
            $this->throwException('No folders were found in the daily folder ' . $dailyFolder . '.', EXCEPTION_CODE_WARNING);
		}

        // Scan for all campaign folders.
		$campaignFolderList = array();

		foreach ($folderList as $folder) {

			// Scan for items. If none found, skip folder.
			$itemList = ftp_nlist($this->connection, $folder);
			if (empty($itemList)) {
                \SmartFocus\Util::log('<----Empty folder ' . $folder . '.');
				continue;
			}

            $campaignFolderList[$folder] = array();

			// Scan for files.
			$logFileFound = FALSE;

			foreach ($itemList as $item) {

				// Test if the item is a folder. If so, skip it.
				if (@ftp_chdir($this->connection, $item)) {
                    @ftp_chdir($this->connection, $this->config['folder']);
					continue;
				}

				// Check file extension.
				$fileExtension 	= strtolower(pathinfo($item, PATHINFO_EXTENSION));
				$fileName 		= pathinfo($item, PATHINFO_BASENAME);

				if (strcmp($fileExtension, 'ack1') == 0) {
                    \SmartFocus\Util::log('<----Skip already processed folder ' . $folder . '. Found log file ' . $fileName);
                    $logFileFound = TRUE;
					unset ($campaignFolderList[$folder]);
					break;
				}

			    if (strcmp($fileExtension, 'html') == 0) {
                    $campaignFolderList[$folder]['message'] = $item;
				}

			    if (strcmp($fileExtension, 'csv') == 0) {
                    switch (substr(strtolower($fileName), 0, 2)) {
						case 'c_':
	                        $campaignFolderList[$folder]['campaign'] = $item;
							break;
						case 'm_':
	                    	$campaignFolderList[$folder]['member'] = $item;
							break;
						default:
	                    	break;
					}
				}

			}

			if (!$logFileFound && empty($campaignFolderList[$folder]['campaign'])) {
                \SmartFocus\Util::log('<----Skip folder: ' . $folder . '. No campaign file found.');
				unset ($campaignFolderList[$folder]);
			}

		}

		if (empty($campaignFolderList)) {
            $this->throwException('No new campaign folders were found in the daily folder ' . $dailyFolder . '.', EXCEPTION_CODE_NOTICE);
		}

		$countCampaignFolders = count($campaignFolderList);

        \SmartFocus\Util::log('<----Found a total of ' . $countCampaignFolders . ' new campaign folder' . ($countCampaignFolders == 1 ? NULL : 's') . ' in the daily folder ' . $dailyFolder . '.');

		return $campaignFolderList;

	}

	/**
	 * Throws an exception
	 *
	 * @param string $message
	 * @param int $code [optional]
	 * @param bool $disconnect [optional][default=TRUE]
	 * @return throw \Exception
	 */
	public function throwException($message, $code = NULL, $disconnect = TRUE) {

		\SmartFocus\Util::log('<----' . $message);
		if ($disconnect) {
        	$this->close();
		}
		throw new \Exception($message, $code);

	}

}
?>