<?php namespace SmartFocus\Spir;

/**
 * Utility class
 */
class Util extends \SmartFocus\Util {

	/**
	 * Campaign NOT OPENED flow
	 *
	 * @param array &$campaign
	 * @param SmartFocus\API\CampaignManagementService $campaignManagementService
	 * @return bool
	 */
	public static function campaignNotOpened(&$campaign, $campaignManagementService)
	{

        // Set the segment name
		$campaign['info']['segment'] = pathinfo($campaign['files']['remoteFileList']['campaign'], PATHINFO_FILENAME);

		// Set the message name
		$campaign['info']['message'] = 'SmartFocus Test ' . pathinfo($campaign['files']['remoteFileList']['campaign'], PATHINFO_FILENAME);

		// -----====================-----
		// START GET CAMPAIGN
     	// -----====================-----
		self::log('---->' . sprintf(ACTION_CAMPAIGN_GET, CAMPAIGN_GET_BY_FIELD, $campaign['info']['referenceMd5']));

			$sfCampaignId = $campaignManagementService->getCampaignsByField(CAMPAIGN_GET_BY_FIELD, $campaign['info']['referenceMd5'], 1);
			if (empty($sfCampaignId)) {
				throw new \Exception (sprintf(ERROR_MESSAGE_CAMPAIGN_FIND, CAMPAIGN_GET_BY_FIELD, $campaign['info']['referenceMd5']), EXCEPTION_CODE_ERROR);
			}
	    	$sfCampaign = (array) $campaignManagementService->getCampaign($sfCampaignId);
			if (empty($sfCampaign)) {
	            throw new \Exception (sprintf(ERROR_MESSAGE_CAMPAIGN_GET_UNKNOWN, $sfCampaignId), EXCEPTION_CODE_ERROR);
			}

        self::log(sprintf(LOG_MESSAGE_OK_CAMPAIGN_GET, CAMPAIGN_GET_BY_FIELD, $campaign['info']['referenceMd5'], $sfCampaign['id']));
		// -----====================-----
		// END GET CAMPAIGN
     	// -----====================-----

		// -----====================-----
		// START CREATE SEGMENT, ADD CRITERIA TO SEGMENT, COUNT SEGMENT
     	// -----====================-----
        self::log('---->' . sprintf(ACTION_SEGMENT_CREATE, $campaign['info']['segment']));

			// Create segment
			$campaign['info']['segmentId'] = (int) $campaignManagementService->segmentationCreateSegment($campaign['info']['segment'], $campaign['info']['segment']);
			if (empty($campaign['info']['segmentId'])) {
	            throw new \Exception(sprintf(ERROR_MESSAGE_SEGMENT_CREATE_UNKNOWN, $campaign['info']['segment']), EXCEPTION_CODE_ERROR);
			}
			// Add "NOT OPENED" campaign action criteria to segment
	        if (!(bool) $campaignManagementService->segmentationAddCampaignActionCriteriaByObj($campaign['info']['segmentId'], $sfCampaign['id'], OPERATOR_DID_NOT_OPEN)) {
				throw new \Exception (sprintf(ERROR_MESSAGE_SEGMENT_ADD_CAMPAIGN_ACTION_CRITERIA_UNKNOWN, $campaign['info']['segmentId'], OPERATOR_DID_NOT_OPEN), EXCEPTION_CODE_ERROR);
			}
	        sleep(5);
	        // Count segment
			$campaign['info']['segmentCount'] = (int) self::segmentCount($campaignManagementService, $campaign['info']['referenceMd5'], $campaign['info']['segmentId']);
			if (empty($campaign['info']['segmentCount'])) {
	            throw new \Exception(sprintf(ERROR_MESSAGE_SEGMENT_EMPTY, $campaign['info']['segmentId']), EXCEPTION_CODE_ERROR);
			}

        self::log(sprintf(LOG_MESSAGE_OK_SEGMENT_CREATE, $campaign['info']['segmentId'], $campaign['info']['segmentCount']));
    	// -----====================-----
		// END CREATE SEGMENT, ADD CRITERIA TO SEGMENT, COUNT SEGMENT
     	// -----====================-----

    	// -----====================-----
		// START CLONE MESSAGE
     	// -----====================-----
        self::log('---->' . sprintf(ACTION_MESSAGE_CLONE, $sfCampaign['messageId']));

			$campaign['info']['messageId'] = (int) $campaignManagementService->cloneMessage($sfCampaign['messageId'], $campaign['info']['message']);
			if (empty($campaign['info']['messageId'])) {
	            throw new \Exception(sprintf(ERROR_MESSAGE_MESSAGE_CLONE_UNKNOWN, $sfCampaign['messageId'], $campaign['info']['message']), EXCEPTION_CODE_ERROR);
			}

        self::log(sprintf(LOG_MESSAGE_OK_MESSAGE_CLONE, $sfCampaign['messageId'], $campaign['info']['messageId']));
    	// -----====================-----
		// END CLONE MESSAGE
     	// -----====================-----

		// -----====================-----
		// START CREATE CAMPAIGN
  		// -----====================-----
        self::log('---->' . sprintf(ACTION_CAMPAIGN_CREATE, $campaign['info']['name'], $campaign['info']['sendDate'], $campaign['info']['messageId'], $campaign['info']['segmentId']));

	        $campaign['info']['id'] = (int) $campaignManagementService->createCampaignByObj(array(
				'name' 			=> $campaign['info']['name'],
				'description' 	=> $campaign['info']['referenceMd5'] . ' ' . $campaign['info']['companyId'],
				'sendDate' 		=> $campaign['info']['sendDate'],
				'messageId' 	=> $campaign['info']['messageId'],
				'mailinglistId'	=> $campaign['info']['segmentId'],
			));
			if (empty($campaign['info']['messageId'])) {
	            throw new \Exception(sprintf(ERROR_MESSAGE_CAMPAIGN_CREATE_UNKNOWN, $campaign['info']['name'], $campaign['info']['sendDate'], $campaign['info']['messageId'], $campaign['info']['segmentId']), EXCEPTION_CODE_ERROR);
			}

        self::log(sprintf(LOG_MESSAGE_OK_CAMPAIGN_CREATE, $campaign['info']['id']));
		// -----====================-----
		// END CREATE CAMPAIGN
  		// -----====================-----

		// -----====================-----
		// START POST CAMPAIGN
  		// -----====================-----
        self::log('---->Post campaign.');

			$campaign['info']['status'] = (bool) $campaignManagementService->postCampaign($campaign['info']['id']);
			if (!$campaign['info']['status']) {
	        	throw new \Exception(sprintf(ERROR_MESSAGE_CAMPAIGN_POST_UNKNOWN, $campaign['info']['id']), EXCEPTION_CODE_ERROR);
			}

        self::log(sprintf(LOG_MESSAGE_OK_CAMPAIGN_POST, $campaign['info']['id']));
		// -----====================-----
		// END POST CAMPAIGN
  		// -----====================-----

// 		$campaign['info']['segmentId']		= 2346184;
// 		$campaign['info']['segmentCount']	= 0;
// 		$campaign['info']['messageId']		= 2290567;
// 		$campaign['info']['id']				= 2415585;
// 		$campaign['info']['status']			= TRUE;

        return $campaign['info']['status'];

	}

	/**
	 * Campaign STANDARD flow
	 *
	 * @param array &$campaign
	 * @param SmartFocus\API\BatchMemberService $batchMemberService
	 * @param SmartFocus\API\CampaignManagementService $campaignManagementService
	 * @param string $uploadFolder
	 * @return bool
	 */
	public static function campaignStandard(&$campaign, $batchMemberService, $campaignManagementService, $uploadFolder)
	{

		$fileSeparatorConfig	= self::$config['fileSeparator'];
        $fieldsMatchConfig		= self::$config['fieldsMatch'];
        $importStatuses			= self::$config['importStatuses'];

        // Set the segment name
		$campaign['info']['segment'] = pathinfo($campaign['files']['remoteFileList']['member'], PATHINFO_FILENAME);

		// Set the message name
		$campaign['info']['message'] = 'SmartFocus Test ' . pathinfo($campaign['files']['remoteFileList']['message'], PATHINFO_FILENAME);

    	// -----====================-----
		// START CHECK FILES
     	// -----====================-----
			self::log('---->Check the member file.');
			if (empty($campaign['files']['localFileList']['member']) || !is_readable($campaign['files']['localFileList']['member'])) {
				throw new \Exception('Wrong or missing member file for the ' . CAMPAIGN_MODE_STANDARD . ' campaign ' . $campaign['info']['referenceMd5'] . '.', EXCEPTION_CODE_ERROR);
			}

			self::log('---->Check the message file.');
			if (empty($campaign['files']['localFileList']['message']) || !is_readable($campaign['files']['localFileList']['message'])) {
				throw new \Exception('Wrong or missing message file for the ' . CAMPAIGN_MODE_STANDARD . ' campaign ' . $campaign['info']['referenceMd5'] . '.', EXCEPTION_CODE_ERROR);
			}
    	// -----====================-----
		// END CHECK FILES
     	// -----====================-----

    	// -----====================-----
		// START PROCESS THE MEMBERS FILE
     	// -----====================-----
        self::log('---->' . sprintf(ACTION_FILE_MEMBERS_PROCESS, $campaign['files']['localFileList']['member']));

			$campaign['info']['lines'] = array();

			$campaign['files']['localFileList']['import'] = self::parseMembersFile(
                $campaign['info']['lines'],
				$campaign['files']['localFolder'] . '/' . self::generateFileName('csv', 'I_', $uploadFolder, $campaign['files']['localFolder']),
				$campaign['files']['localFileList']['member'],
				$fileSeparatorConfig['member'],
				$fieldsMatchConfig['member']
			);

		self::log(sprintf(LOG_MESSAGE_OK_FILE_MEMBERS_PROCESS, $campaign['files']['localFileList']['member']));
    	// -----====================-----
		// END PROCESS THE MEMBERS FILE
     	// -----====================-----

    	// -----====================-----
		// START IMPORT MEMBERS
     	// -----====================-----
        self::log('---->' . sprintf(ACTION_MEMBER_IMPORT, $campaign['files']['localFileList']['import']));

			// Upload members
			$campaign['info']['importId'] = (int) $batchMemberService->uploadFileInsert(
				file_get_contents($campaign['files']['localFileList']['import']),
				pathinfo($campaign['files']['remoteFileList']['member'], PATHINFO_BASENAME),
				$fileSeparatorConfig['member'],
				self::generateMembersMapping($fieldsMatchConfig['member']),
                self::$config['importMapping'],
				'UTF-8',
				TRUE,
				'dd/MM/yyyy'
			);
			if (empty($campaign['info']['importId'])) {
	   			throw new \Exception(sprintf(ERROR_MESSAGE_MEMBER_UPLOAD_UNKNOWN, $campaign['files']['localFileList']['import']), EXCEPTION_CODE_ERROR);
			}
			sleep(5);
			// Check the members import status
			$campaign['info']['importStatus'] = self::checkMembersImportStatus(
				$batchMemberService,
				$campaign['info']['referenceMd5'],
				$campaign['info']['importId'],
				$importStatuses,
				self::$config['importStatusGetDelay']
			);

		self::log(sprintf(LOG_MESSAGE_OK_MEMBERS_IMPORT, $campaign['info']['importId'], $campaign['info']['importStatus']));
    	// -----====================-----
		// END IMPORT MEMBERS
     	// -----====================-----

		// -----====================-----
		// START CREATE SEGMENT, ADD CRITERIA TO SEGMENT, COUNT SEGMENT
     	// -----====================-----
        self::log('---->' . sprintf(ACTION_SEGMENT_CREATE, $campaign['info']['segment']));

			// Create segment
   			$campaign['info']['segmentId'] = (int) $campaignManagementService->segmentationCreateSegment($campaign['info']['segment'], 'Test SPIR');
			if (empty($campaign['info']['segmentId'])) {
	            throw new \Exception(sprintf(ERROR_MESSAGE_SEGMENT_CREATE_UNKNOWN, $campaign['info']['segment']), EXCEPTION_CODE_ERROR);
			}
   			// Add segment demographic criteria
			if (!(bool) $campaignManagementService->segmentationAddStringDemographicCriteriaByObj($campaign['info']['segmentId'], self::$config['segmentMapping'], 'EQUALS', $campaign['info']['referenceMd5'])) {
            	throw new \Exception (sprintf(ERROR_MESSAGE_SEGMENT_ADD_CAMPAIGN_ACTION_CRITERIA_UNKNOWN, $campaign['info']['segmentId'], self::$config['segmentMapping'], 'EQUALS', $campaign['info']['referenceMd5']), EXCEPTION_CODE_ERROR);
			}

	        sleep(5);
	        // Count segment
			$campaign['info']['segmentCount'] = (int) self::segmentCount($campaignManagementService, $campaign['info']['referenceMd5'], $campaign['info']['segmentId']);
			if (false && empty($campaign['info']['segmentCount'])) {
	            throw new \Exception(sprintf(ERROR_MESSAGE_SEGMENT_EMPTY, $campaign['info']['segmentId']), EXCEPTION_CODE_ERROR);
			}

        self::log(sprintf(LOG_MESSAGE_OK_SEGMENT_CREATE, $campaign['info']['segmentId'], $campaign['info']['segmentCount']));
    	// -----====================-----
		// END CREATE SEGMENT, ADD CRITERIA TO SEGMENT, COUNT SEGMENT
     	// -----====================-----

		// -----====================-----
		// START CREATE MESSAGE
     	// -----====================-----
        self::log('---->' . sprintf(ACTION_MESSAGE_CREATE, $campaign['info']['message']));

			// Create message
			$campaign['info']['messageId'] = (int) $campaignManagementService->createEmailMessagebyObj(array(
				'name' 				=> $campaign['info']['message'],
				'body' 				=> '[EMV HTMLPART]' . utf8_encode(file_get_contents($campaign['files']['localFileList']['message'])),
				'replyToEmail'		=> $campaign['info']['replyTo'],
				'description' 		=> $campaign['info']['subject'],
				'subject' 			=> $campaign['info']['subject'],
				'from' 				=> $campaign['info']['alias'],
				'fromEmail' 		=> $campaign['info']['sender'],
			));

			if (empty($campaign['info']['messageId'])) {
	            throw new \Exception(sprintf(ERROR_MESSAGE_MESSAGE_CREATE_UNKNOWN, $campaign['info']['message']), EXCEPTION_CODE_ERROR);
			}

			// Create mirror link
			$campaignManagementService->createAndAddMirrorUrl($campaign['info']['messageId']);

	        // Track all links
			try {
		        $campaign['trackedLinks'] = $campaignManagementService->trackAllLinks($campaign['info']['messageId']);
			}
		  	catch (\Exception $exception) {
	   			self::log('<----' .  sprintf(LOG_MESSAGE_KO_MESSAGE_LINKS_TRACK, $campaign['info']['messageId']));
			}

        self::log(sprintf(LOG_MESSAGE_OK_MESSAGE_CREATE, $campaign['info']['messageId']));
    	// -----====================-----
		// END CREATE MESSAGE
     	// -----====================-----

		// -----====================-----
		// START CREATE CAMPAIGN
  		// -----====================-----
        self::log('---->' . sprintf(ACTION_CAMPAIGN_CREATE, $campaign['info']['name'], $campaign['info']['sendDate'], $campaign['info']['messageId'], $campaign['info']['segmentId']));

	        $campaign['info']['id'] = (int) $campaignManagementService->createCampaignByObj(array(
				'name' 			=> $campaign['info']['name'],
				'description' 	=> $campaign['info']['referenceMd5'] . ' ' . $campaign['info']['companyId'],
				'sendDate' 		=> $campaign['info']['sendDate'],
				'messageId' 	=> $campaign['info']['messageId'],
				'mailinglistId'	=> $campaign['info']['segmentId'],
			));
			if (empty($campaign['info']['id'])) {
	            throw new \Exception(sprintf(ERROR_MESSAGE_CAMPAIGN_CREATE_UNKNOWN, $campaign['info']['name'], $campaign['info']['sendDate'], $campaign['info']['messageId'], $campaign['info']['segmentId']), EXCEPTION_CODE_ERROR);
			}

        self::log(sprintf(LOG_MESSAGE_OK_CAMPAIGN_CREATE, $campaign['info']['id']));
		// -----====================-----
		// END CREATE CAMPAIGN
  		// -----====================-----

		// -----====================-----
		// START POST CAMPAIGN
  		// -----====================-----
        self::log('---->Post campaign.');

			$campaign['info']['status'] = (bool) $campaignManagementService->postCampaign($campaign['info']['id']);
			if (!$campaign['info']['status']) {
	        	throw new \Exception(sprintf(ERROR_MESSAGE_CAMPAIGN_POST_UNKNOWN, $campaign['info']['id']), EXCEPTION_CODE_ERROR);
			}

        self::log(sprintf(LOG_MESSAGE_OK_CAMPAIGN_POST, $campaign['info']['id']));
		// -----====================-----
		// END POST CAMPAIGN
  		// -----====================-----

		/*
		$campaign['info']['importId'] 		= 869084;
		$campaign['info']['importStatus']	= 'DONE';
		$campaign['info']['segmentId']		= 2346142;
		$campaign['info']['segmentCount']	= 5;
		$campaign['info']['messageId']		= 2290515;
		$campaign['info']['trackedLinks']	= 5;
		$campaign['info']['id']				= 2415538;
		$campaign['info']['status']			= TRUE;
		*/

        return $campaign['info']['status'];

	}

	/**
	 * Check the members import status with delay and timeout
	 *
	 * @param SmartFocus\API\BatchMemberService $batchMemberService
	 * @param string $campaignReference
	 * @param int $sfImportId
	 * @param array $importStatuses
	 * @param array $importStatuses
	 * @param int $delay [optional][default=30]
	 * @param int $maxAttempts [optional][default=10]
	 * @return string or throw \Exception
	 */
	public static function checkMembersImportStatus($batchMemberService, $campaignReference, $sfImportId, $importStatuses, $delay = 30, $maxAttempts = 10)
	{

		$repeat 		= TRUE;
		$attempts 		= 0;
	    $sfImportStatus	= NULL;

		$logMessageParameters = 'campaignReference: ' . $campaignReference . ', sfImportId: ' . $sfImportId;

		while ($repeat && $attempts < $maxAttempts) {

            $attempts++;

			try {

			    self::log('---->Get the members import status (' . $logMessageParameters . ').');
		    	$sfImportStatus = $batchMemberService->getUploadStatus($sfImportId);
				if (empty($sfImportStatus->status)) {
		            throw new \Exception('Error retrieving the members import status. (' . $logMessageParameters . ').', EXCEPTION_CODE_WARNING);
				}
		        $sfImportStatus = strval($sfImportStatus->status);
	            self::log('<----Successfully retrieved the members import status ' . $sfImportStatus . '.');

				if (in_array($sfImportStatus, $importStatuses['success'])) {
					$repeat = FALSE;
                    self::log('<----The members import job has finished with the status ' . $sfImportStatus . '.');
				}
				elseif (in_array($sfImportStatus, $importStatuses['pending'])) {
	                self::log('---->Waiting for ' . $delay . ' seconds before rechecking the members import status.');
					sleep($delay);
				}
				else {
			  		throw new \Exception('The members import has failed. (' . $logMessageParameters . ', sfImportStatus: ' . $sfImportStatus . ')', EXCEPTION_CODE_ERROR);
				}

			}
			catch (\SoapFault $e) {
	            $repeat = FALSE;
				throw($e);
	            return FALSE;
			}
			catch (\Exception $e) {
	        	$repeat = FALSE;
				if ($e->getCode() == EXCEPTION_CODE_ERROR) {
					throw($e);
					return FALSE;
				}
				else {
					self::log('---->' . $e->getMessage());
				}
			}

			if ($repeat && $attempts == $maxAttempts) {
	        	$repeat = FALSE;
       			throw new \Exception('Timeout error for the members import job (' . $logMessageParameters . ', sfImportStatus: ' . $sfImportStatus . ', attempts: ' . $attempts . ').', EXCEPTION_CODE_ERROR);
	            return FALSE;
			}

		}

		return $sfImportStatus;

	}

	/**
	 * Generates a filename
	 *
	 * @param string $extension
	 * @param string $prefix
	 * @param string $rootFolder
	 * @param string $folderPath
	 * @return string
	 */
	public static function generateFileName($extension, $prefix, $rootFolder, $folderPath) {

		if (strcmp($extension, 'html') == 0) {
			$prefix = null;
		}

		return $prefix . implode('_', array_reverse(explode('/', trim(substr($folderPath, strlen($rootFolder)), '/')))) . '.' . $extension;
	}

	/**
	 * Generate members mapping
	 *
	 * @param array $fieldList
	 * @return array
	 */
	public static function generateMembersMapping($fieldList)
	{

		$mapping	= array();
		$column 	= 0;

		foreach ($fieldList as $field) {
		    $column++;
		    $mapping['column'][] = array(
				'colNum' 	=> $column,
				'toReplace'	=> false,
				'fieldName' => $field,
			);
		}

		return $mapping;

	}

	/**
	 * Parse campaign method
	 *
	 * @param array &$campaign
	 * @param string $separator
	 * @param array $fieldsMatch
	 * @return bool or throw \Exception
	 */
	public static function parseCampaignFile(&$campaign, $separator, $fieldsMatch)
	{
		// -----====================-----
		// START PROCESS CAMPAIGN FILE
		// -----====================-----
        self::log('---->' . sprintf(ACTION_FILE_CAMPAIGN_PROCESS, $campaign['files']['localFileList']['campaign']));

			if (!is_readable($campaign['files']['localFileList']['campaign']) || !($fileHandle = fopen($campaign['files']['localFileList']['campaign'], 'r'))) {
	            throw new \Exception('Invalid downloaded campaign file ' . $campaign['files']['localFileList']['campaign'] . '.', EXCEPTION_CODE_ERROR);
			}

	        $data = fgetcsv($fileHandle, 0, $separator);

			if (empty($data)) {
	            throw new \Exception('No valid campaign found in the command file ' . $campaign['files']['localFileList']['campaign'] . '.', EXCEPTION_CODE_ERROR);
			}

	        $data = array_map('trim', $data);

			$data = array_combine($fieldsMatch, $data);

			$dateTime 			= new \DateTime($data['sendDate'], new \DateTimeZone('Europe/Paris'));
			$data['sendDate'] 	=  $dateTime->format('c');

	        $campaign['info'] = $data;
			// Fix a possible bug
	        if (empty($campaign['info']['referenceMd5'])) {
	            $campaign['info']['referenceMd5'] = $campaign['files']['folderName'];
			}
			// Make sure the send date is at least 15 minutes from now
	        $campaign['info']['sendDate'] = self::setSendDate($campaign['info']['sendDate'], 'Europe/Paris');

			// Validate sender
            $campaign['info']['sender'] = (empty($campaign['info']['sender']) || !filter_var($campaign['info']['sender'], FILTER_VALIDATE_EMAIL))
				? self::$config['sender']
				: $campaign['info']['sender'];

	        $campaign['info']['status'] = FALSE;

		self::log(sprintf(LOG_MESSAGE_OK_FILE_CAMPAIGN_PROCESS, $campaign['files']['localFileList']['campaign']));
		// -----====================-----
		// END PROCESS CAMPAIGN FILE
		// -----====================-----

		return TRUE;
	}

	/**
	 * Parse members method
	 *
	 * @param array $lines
	 * @param string $newFilePath
	 * @param string $filePath
	 * @param string $separator
	 * @param array $fieldsMatch
	 * @return string or throw \Exception
	 */
	public static function parseMembersFile(&$lines, $newFilePath, $filePath, $separator, $fieldsMatch)
	{
		$lines = array(
			'found' 	=> 0,
			'skipped' 	=> 0,
			'ok' 		=> 0,
			'log' 		=> array(),
		);

		if (!is_readable($filePath) || !($fileHandle = fopen($filePath, 'r'))) {
            throw new \Exception('Invalid downloaded members file ' . $filePath . '.', EXCEPTION_CODE_ERROR);
		}

		while (($data = fgetcsv($fileHandle, 0, $separator)) !== false) {

			try {

                $lines['found']++;

            	$data = array_combine(array_keys($fieldsMatch), $data);

				foreach ($data as $key => &$value) {
		            $value = trim($value);
					switch ($key) {
						case 'email':
							if (empty($value)) {
		      					throw new \Exception('Empty email found. Skipping line ' . $lines['found'] . '.', EXCEPTION_CODE_WARNING);
							}
							if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
		      					throw new \Exception('Invalid email found: ' . $value . '. Skipping line ' . $lines['found'] . '.', EXCEPTION_CODE_WARNING);
							}
							break;
						case 'partnerHeader':
						case 'partnerFooter':
							$value = str_replace('{EMAIL}', $data['email'], $value);
							break;
					}
				}

                $lines['ok']++;

				if ($lines['ok'] == 1) {
					file_put_contents($newFilePath, implode($separator, array_values($fieldsMatch)));
				}
                file_put_contents($newFilePath, PHP_EOL . implode($separator, $data), FILE_APPEND);

			}
			catch (\Exception $exception) {
                $lines['skipped']++;
                $lines['log'][] = $exception->getMessage();
				self::log('<----' . $exception->getMessage());
			}

		}

		if (empty($lines['ok'])) {
            throw new \Exception('No members were processed out of ' . $lines['found'] . ' found.', EXCEPTION_CODE_ERROR);
		}

		if ($lines['ok'] == $lines['found']) {
        	self::log('<----Successfully processed all members out of ' . $lines['found'] . ' found.');
		}
		else {
        	self::log('<----Successfully processed ' . $lines['ok'] . ' members out of ' . $lines['found'] . ' found.');
		}

		return $newFilePath;

	}

	/**
	 * Parse soap fault detail object
	 *
	 * @param \SoapFault $soapFault
	 * @return string
	 */
	public static function parseSoapFaultDetail(\SoapFault $soapFault)
	{
		$message = null;
		if (
			property_exists($soapFault, 'detail')
			&& property_exists($soapFault->detail, 'CcmdServiceException')
			&& property_exists($soapFault->detail->CcmdServiceException, 'description')
			&& property_exists($soapFault->detail->CcmdServiceException, 'fields')
			&& property_exists($soapFault->detail->CcmdServiceException, 'status')
		) {
            $message .= ' ' . $soapFault->detail->CcmdServiceException->description . ' (value: ' . $soapFault->detail->CcmdServiceException->fields . ', status: ' . $soapFault->detail->CcmdServiceException->status . ')';
		}

		return $message;

	}

	/**
	 * Counts the segmet with delay and max attempts
	 *
	 * @param SmartFocus\API\CampaignManagementService $campaignManagementService
	 * @param string $campaignReference
	 * @param int $sfSegmentId
	 * @param int $delay [optional][default=5]
	 * @param int $maxAttempts [optional][default=3]
	 * @return int  or throw \Exception
	 */
	public static function segmentCount($campaignManagementService, $campaignReference, $sfSegmentId, $delay = 5, $maxAttempts = 3)
	{
		$attempts 		= 0;
	    $sfSegmentCount	= 0;

		while ($sfSegmentCount == 0 && $attempts < $maxAttempts) {

	        $attempts++;

			try {
	    		$sfSegmentCount = (int) $campaignManagementService->segmentationDistinctCount($sfSegmentId);

	    		self::log('<----Successfully counted segment (campaignReference: ' . $campaignReference . ', segmentId: ' . $sfSegmentId . ', sfSegmentCount: ' . $sfSegmentCount . ').');
			}
			catch (\SoapFault $e) {
				if ($attempts == $maxAttempts - 1) {
	                return $sfSegmentCount;
				}
			}
			catch (\Exception $e) {
				if ($attempts == $maxAttempts - 1) {
	                return $sfSegmentCount;
				}
			}

			if ($sfSegmentCount == 0 && $attempts == $maxAttempts) {
				return 0;
				throw new \Exception('Empty segment count after ' . $attempts . ' attempts.', EXCEPTION_CODE_ERROR);
			}

            sleep($delay);

		}

	    return $sfSegmentCount;

	}

}

?>