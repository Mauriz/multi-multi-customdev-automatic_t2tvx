<?php
$config['logFolder'] 			= '/opt/automatic/application/spir/log';
$config['uploadFolder'] 		= '/opt/automatic/application/spir/upload';

$config['api']['partner_users'] 	= array('A' => 'valley_topannonce');
$config['applicationName'] 			= 'SPIR Automatic Emails';
$config['fieldsMatch'] = array(
	'campaign' => array(
		'configMode',
		'name',
		'referenceMd5',
		'sender',
		'replyTo',
		'alias',
		'subject',
		'sendDate',
		'companyId',
	),

	'member' => array(
		'title'			=> 'TITLE',
		'firstName'		=> 'FIRSTNAME',
		'lastName'		=> 'LASTNAME',
		'email'			=> 'EMAIL',
		'zipCode' 		=> 'ZIPCODE',
		'city' 			=> 'CITY',
		'campaignId'	=> 'CAMPAIGN_ID',
		'partnerHeader'	=> 'HEADER',
		'partnerFooter'	=> 'FOOTER',
	),
);
$config['fileSeparator'] = array(
	'member' 	=> '|',
	'campaign' 	=> ';',
	'log' 		=> '|',
);

$config['importMapping'] = 'LOWER(EMAIL),CAMPAIGN_ID';
$config['segmentMapping'] = 'CAMPAIGN_ID';

$config['ftp'] = array(
    'ssl' 						=> FALSE,
    'server' 					=> 'ftp.spir.fr',
    'port' 						=> '21',
    'username'					=> 'smartfocus',
    'password' 					=> 'Fb88jk5*',
	'folder'					=> '/',
	'connectionAttempts'		=> 3,
	'connectionDelay'			=> 1,
	'authenticationAttempts'	=> 3,
    'authenticationDelay'		=> 1,
	'downloadAttempts'			=> 3,
    'downloadDelay'				=> 1,
	'timeout'					=> 90,
	'timeoutIncrease'			=> 90,
);

$config['importStatusCountDelay']	= 10;
$config['importStatusGetDelay']		= 45;
$config['memberFileSeparator']		= '|';
$config['sendDateFormat']			= 'Y-m-d-H.i.s.u';
$config['sender']					= 'digipush@digipush-mail.regicom.fr';
?>