<?php
// Generic
define('CAMPAIGN_MODE_STANDARD', 	'STANDARD');
define('CAMPAIGN_GET_BY_FIELD', 	'description');
define('OPERATOR_DID_NOT_OPEN', 	'DIDNOTOPENEDMESSAGE_CAMP');

// Actions
define('ACTION_SCRIPT',					'Script.');
define('ACTION_API_INIT',				'Initialize the API.');
define('ACTION_CAMPAIGN_CREATE',		'Create campaign (name: %s, sendDate: %s, messageId: %d, segmentId: %d).');
define('ACTION_CAMPAIGN_DOWNLOAD',		'Download the campaigns.');
define('ACTION_CAMPAIGN_GET',			'Get campaign (field: %s, value: %s).');
define('ACTION_CAMPAIGN_POST',			'Post campaign (id: %d).');
define('ACTION_CAMPAIGN_PROCESS',		'Process campaign (reference: %s).');
define('ACTION_FILE_CAMPAIGN_LOG',		'Upload the campaign log file (file: %s).');
define('ACTION_FILE_CAMPAIGN_PROCESS',	'Process the campaign file (file: %s).');
define('ACTION_FILE_MEMBERS_PROCESS',	'Process the members file (file: %s).');
define('ACTION_MEMBER_IMPORT',			'Import members (file: %s).');
define('ACTION_MESSAGE_CLONE',			'Clone message (id: %d).');
define('ACTION_MESSAGE_CREATE',			'Create message (name: %s).');
define('ACTION_SEGMENT_CREATE',			'Create segment (segment: %s).');

// Errors
define ('ERROR_MESSAGE_CAMPAIGN_CREATE_UNKNOWN',						'Could not create campaign (name: %s, sendDate: %s, messageId: %d, segmentId: %d).');
define ('ERROR_MESSAGE_CAMPAIGN_FIND',									'Could not find any campaign (field: %s, value: %s).');
define ('ERROR_MESSAGE_CAMPAIGN_GET_UNKNOWN',							'Could not retrieve campaign (id: %d).');
define ('ERROR_MESSAGE_CAMPAIGN_POST_UNKNOWN',							'Could not post campaign (id: %d).');
define ('ERROR_MESSAGE_FOLDER_CREATE',									'Could not create folder (folder: %s).');
define ('ERROR_MESSAGE_LOG_FILE_CREATE',								'Could not create log file (file: %s).');
define ('ERROR_MESSAGE_MEMBER_IMPORT_STATUS_GET',						'Error retrieving the members import status (id: %d).');
define ('ERROR_MESSAGE_MEMBER_UPLOAD_UNKNOWN',							'Unknown error while uploading members (file: %s).');
define ('ERROR_MESSAGE_MESSAGE_CLONE_UNKNOWN',							'Unknown error while cloning the message (id: %d, newName: %s).');
define ('ERROR_MESSAGE_MESSAGE_CREATE_UNKNOWN',							'Unknown error while creating the message (name: %s).');
define ('ERROR_MESSAGE_SEGMENT_CREATE_UNKNOWN',							'Unknown error while creating new segment (segment: %s).');
define ('ERROR_MESSAGE_SEGMENT_ADD_CAMPAIGN_ACTION_CRITERIA_UNKNOWN',	'Unknown error while adding campaign action criteria to segment (id: %d, criteria: %s).');
define ('ERROR_MESSAGE_SEGMENT_ADD_DEMOGRAPHIC_CRITERIA_UNKNOWN',		'Unknown error while adding demographic criteria to segment (id: %d, field: %s, operator: %s, value: %s).');
define ('ERROR_MESSAGE_SEGMENT_EMPTY',									'Empty segment (id: %d).');

// Log
define ('LOG_MESSAGE_OK_CAMPAIGN_CREATE',		'<----Successfully created the campaign (id: %d).');
define ('LOG_MESSAGE_OK_CAMPAIGN_GET',			'<----Successfully retrieved the campaign (field: %s, value: %s, id: %d).');
define ('LOG_MESSAGE_OK_CAMPAIGN_POST',			'<----Successfully posted the campaign (id: %d).');
define ('LOG_MESSAGE_OK_FILE_CAMPAIGN_PROCESS',	'<----Successfully processed the campaign file (file: %s).');
define ('LOG_MESSAGE_OK_FILE_MEMBERS_PROCESS',	'<----Successfully processed the members file (file: %s).');
define ('LOG_MESSAGE_OK_MEMBERS_IMPORT',		'<----Successfully imported the members file (id: %d, status: %s).');
define ('LOG_MESSAGE_OK_MESSAGE_CLONE',			'<----Successfully cloned the message (oldId: %d, newId: %d).');
define ('LOG_MESSAGE_OK_MESSAGE_CREATE',		'<----Successfully created the message (id: %d).');
define ('LOG_MESSAGE_OK_SEGMENT_CREATE',		'<----Successfully created the segment (id: %d, count: %d).');
define ('LOG_MESSAGE_KO_MESSAGE_LINKS_TRACK',	'<----There were no links to track (messageId: %d).');
?>