<?php
require 'bootstrap.php';

$dateTime       = new \DateTime((empty($_GET['date']) ? 'now' : trim($_GET['date'])), new \DateTimeZone($config['timeZone']));
$uploadFolder   = $config['uploadFolder'];
$dailyFtpFolder = $config['ftp']['folder'] . $dateTime->format('Ymd');
$nmpParameters  = array(
  'encrypt'    => $config['nmp']['encrypt'],
  'id'         => $config['nmp']['id'],
  'random'     => $config['nmp']['random'],
  'recipients' => $config['nmp']['recepients'],
  'from'       => $config['applicationName'],
  'subject'    => $config['nmp']['subjects']['NOTIFY'],
);

$campaignScanList     = array();
$campaignDownloadList = array();

\SmartFocus\Util::log('[START]' . ACTION_SCRIPT, FALSE, TRUE, NULL, \SmartFocus\Log::FREQUENCY_MINUTE, 15);

// -----====================-----
// START CAMPAIGN DONWLOAD
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_CAMPAIGN_DOWNLOAD);

  try{

    if (!is_dir($uploadFolder) && !mkdir($uploadFolder))  throw new \Exception(sprintf(ERROR_MESSAGE_FOLDER_CREATE, $uploadFolder), EXCEPTION_CODE_ERROR);
    $ftp = new \SmartFocus\Spir\Ftp($config['ftp']['server'], $config['ftp']['username'], $config['ftp']['password'], $config['ftp']['folder'], NULL, $config['ftp']['ssl'], $config['ftp']['port']);
    $ftp->connect();
    $campaignScanList = $ftp->scanDailyFolder($dailyFtpFolder);
    
    foreach( $campaignScanList as $remoteFolder => $remoteFileList ){
      $campaignDownloadList[] = $ftp->downloadFolder($uploadFolder, $remoteFolder, $remoteFileList);
    }
    
    $ftp->close();
    
  }catch (\Exception $exception) {
    
    \SmartFocus\Util::triggerError(
      $exception->getMessage(),
      \SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
      \SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
      ACTION_SCRIPT,
      TRUE
    );
    
  }
  
  
// -----====================-----
// END CAMPAIGN DONWLOAD
// -----====================-----

// -----====================-----
// START API INIT
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_API_INIT);

  try{
    
    $partnerAuth               = new \SmartFocus\API\PartnerPrivilegedAuthenticationService($config['api']['partner_users']['A'], $config['api']['partner_key1'], $config['api']['partner_key2']);
    $batchMemberService        = new \SmartFocus\API\BatchMemberService($partnerAuth);
    $campaignManagementService = new \SmartFocus\API\CampaignManagementService($partnerAuth);
  
  }catch (\Exception $exception) {
    
    \SmartFocus\Util::triggerError(
      $exception->getMessage(),
      \SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
      \SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
      ACTION_SCRIPT,
      TRUE
    );
    
  }
  
// -----====================-----
// END API INIT
// -----====================-----

foreach ($campaignDownloadList as &$fileList) {

  $campaign = array(
    'files'   => & $fileList,
    'info'    => array(),
  );

  $campaign['files']['folderName']            = pathinfo($campaign['files']['localFolder'], PATHINFO_BASENAME);
  $action                                     = sprintf(ACTION_CAMPAIGN_PROCESS, $campaign['files']['folderName']);
  $logFileName                                = \SmartFocus\Spir\Util::generateFileName('ack1', 'C_', $uploadFolder, $campaign['files']['localFolder']);
  $campaign['files']['localFileList']['log']  = $campaign['files']['localFolder'] . '/' . $logFileName;
  $campaign['files']['remoteFileList']['log'] = $campaign['files']['remoteFolder'] . '/' . $logFileName;

  try {

    \SmartFocus\Util::log('[START]' . $action, TRUE);
    \SmartFocus\Spir\Util::parseCampaignFile($campaign, $config['fileSeparator']['campaign'], $config['fieldsMatch']['campaign']);

    if ($campaign['info']['configMode'] == CAMPAIGN_MODE_STANDARD) {
      \SmartFocus\Spir\Util::campaignStandard($campaign, $batchMemberService, $campaignManagementService, $uploadFolder);
    }else{
      \SmartFocus\Spir\Util::campaignNotOpened($campaign, $campaignManagementService);
    }

    $campaign['info']['status'] = TRUE;

    \SmartFocus\Util::log('[END]' . $action);

    $result = array(
      'REFERENCE'      => empty($campaign['info']['referenceMd5']) ? '' : $campaign['info']['referenceMd5'],
      'STATUS'         => 'OK',
      'LINES_FOUND'    => empty($campaign['info']['lines']['found']) ? 0 : $campaign['info']['lines']['found'],
      'LINES_INSERTED' => empty($campaign['info']['lines']['ok']) ? 0 : $campaign['info']['lines']['ok'],
      'SEGMENT_COUNT'  => empty($campaign['info']['segmentCount']) ? 0 : $campaign['info']['segmentCount'],
      'SEND_DATE'      => empty($campaign['info']['sendDate']) ? '' : $campaign['info']['sendDate'],
    );

  }catch (\Exception $exception) {

    $errorMessage = $exception->getMessage();

    if($exception instanceof \SoapFault) {
      $errorMessage .= \SmartFocus\Spir\Util::parseSoapFaultDetail($exception);
    }

    $errorCode = $exception->getCode();

    $result = array(
      'REFERENCE' => empty($campaign['info']['referenceMd5']) ? '' : $campaign['info']['referenceMd5'],
      'STATUS'    => 'KO',
      'REASON'    => $errorMessage,
    );

    \SmartFocus\Util::triggerError(
      $errorMessage,
      \SmartFocus\Util::exceptionCodeToLogCode($errorCode),
      \SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $errorCode),
      $action
    );

  }try{

    \SmartFocus\Util::log('---->' . sprintf(ACTION_FILE_CAMPAIGN_LOG, $campaign['files']['remoteFileList']['log']));

    if (!file_put_contents($campaign['files']['localFileList']['log'], implode($config['fileSeparator']['log'], $result))) {
      throw new \Exception (sprintf(ERROR_MESSAGE_LOG_FILE_CREATE, $campaign['files']['localFileList']['log']), EXCEPTION_CODE_ERROR);
    }
    
    $ftp->connect();
    $ftp->uploadFile($campaign['files']['localFileList']['log'], $campaign['files']['remoteFileList']['log']);
    $ftp->close();

  }catch(\Exception $exception){
      \SmartFocus\Util::triggerError(
        $exception->getMessage(),
        \SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
        \SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode())
      );
  }

}

$report   = \SmartFocus\Util::$log->getHistory();
$report[] = '[END]' . ACTION_SCRIPT;

\SmartFocus\Util::notify($nmpParameters, implode(PHP_EOL, $report));
\SmartFocus\Util::log('[END]' . ACTION_SCRIPT);

exit(LOG_CODE_SUCCESS);
?>