<?php
$config['logFolder'] 				= '/opt/automatic/application/bebiwicks/log';
$config['uploadFolder'] 			= '/opt/automatic/application/bebiwicks/upload';
$config['api']['partner_users'] 	= array('A' => 'prima_api');
$config['applicationName'] 			= 'Bebiwicks';
$config['sftp'] = array(
    'server' 	=> 'sftpe.emv3.com',
    'port' 		=> '22',
    'username'	=> 'prima_sftp',
    'folder'	=> '/synchro/incoming',
);

$config['sftp']['key'] = <<<KEY
PuTTY-User-Key-File-2: ssh-rsa
Encryption: none
Comment: rsa-key-20140702
Public-Lines: 4
AAAAB3NzaC1yc2EAAAABJQAAAIB/r/xNJrnv5nNE9eYUqBdQMXrvBM856dQlUZ3G
dGvCqXVl1IMSxuNkZrhCnulZ8/FYdW/SUcgT2VKi+q07ualqlCBgKLB3khcoefjh
SXx6zfuYWPz7BfmliedVBA+3dCHO8Z27ot0OslthYzCyo9lNVHwRGHkuMFRyT1uv
E2EB2w==
Private-Lines: 8
AAAAgGCg2ps/5qe1UE/crh14SP5qojhWqqhd7KaeoOk1gfsYdIRpd/KIrBSgwsqh
xVjUR+/qKxuzgqc8rT0QunJi/L5DfKUEeVDyGicmXCztuFUtFnNhsOCJ7oL02ZD3
7VVbVfvBTX9QQAOU20ZrFfoAQyyCeBmjCzI+RcNa/qly6R7NAAAAQQDfpKUSbww1
ismkm8X1V2laF/tPk4rS3ck+mznd6z+T/BpaIf0VlGNBwZy/pZeRHeo0OvLev+zI
WLC8a6R+iXY1AAAAQQCSKU6+DtKu6c+r0QSHuqDmBfJIUf5YRUGHvuS58/3wmKfq
VVZQ9BRr5bnfLhC29OhPRvxZIVFMjUiRjTzz2lnPAAAAQQC155y3LFLqnbO6oN2T
/Gy9IcbmOkLZWpYX5iViCDmyGVsO/P2WRFFAyDZ1QE0bDTXv+ufrWynk43IuQP3I
GQeg
Private-MAC: 1e7c073fb28a72a1a6a0d7c8cd7c2a5a315366e2
KEY;

$config['placeHolderTags'] = array(
	'start' => array('|||', '#pf:'),
	'end' 	=> array('|||', '#'),
);
$config['templateList'] 	= array(
    'newsletter-samedi'	=> 'template/defaut.html',
	'maman' 			=> 'template/maman.html',
	'enceinte' 			=> 'template/enceinte.html',
);
?>