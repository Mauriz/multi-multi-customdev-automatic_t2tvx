<?php
// Generic
define ('TAG_BANNER_ID',		'NEWSLETTER_ID');
define ('TAG_BANNER_NAME',		'NEWSLETTER_TITRE');
define ('TAG_BANNER_TYPE_1',	'NEWSLETTER_SLUG');
define ('TAG_BANNER_TYPE_2',	'NEWSLETTER_CATEGORIE');
define ('TAG_BANNER_WRAPPER',	'newsletter');

// Actions
define('ACTION_SCRIPT',				'Script');
define('ACTION_FILE_BANNERS_DOWNLOAD',	'Download the banners file.');
define('ACTION_FILE_BANNERS_PROCESS',	'Process the banners file.');
define('ACTION_BANNER_CREATE',			'Creating new banner (name: %s, description: %s).');
define('ACTION_BANNER_UPDATE',			'Updating banner (id: %d, name: %s, description: %s).');
define('ACTION_FILE_MAPPING_UPDATE',	'Update the mapping file.');
define('ACTION_FILE_MAPPING_UPLOAD',	'Upload the mapping file.');

// Errors
define ('ERROR_MESSAGE_FOLDER_CREATE',					'Could not create folder (folder: %s).');
define ('ERROR_MESSAGE_NMP_CONFIG',						'Wrong or missing nmp configuration.');
define ('ERROR_MESSAGE_NMP_RECIPIENTS',					'No recepients found.');
define ('ERROR_MESSAGE_NMP_SEND',						'Could not send notification. %s');
define ('ERROR_MESSAGE_FILE_BANNERS_DOWNLOAD',			'Invalid downloaded banners file (file: %s).');
define ('ERROR_MESSAGE_FILE_BANNERS_DOWNLOAD_EMPTY',	'No banner files found.');
define ('ERROR_MESSAGE_FILE_BANNERS_PROCESS_EMPTY',		'No banner files processed.');
define ('ERROR_MESSAGE_BANNER_PROCESSED_EMPTY',			'No banners were processed (file: %s).');
define ('ERROR_MESSAGE_BANNER_CREATE_UNKNOWN',			'Unknown error while creating new segment (name: %s, description: %s).');
define ('ERROR_MESSAGE_BANNER_ID_EMPTY',				'Empty banner id.');
define ('ERROR_MESSAGE_BANNER_NAME_EMPTY',				'Empty banner name (id: %s).');
define ('ERROR_MESSAGE_BANNER_TEMPLATE_EMPTY',			'Empty banner template (id: %s).');
define ('ERROR_MESSAGE_BANNER_TEMPLATE_FILE',			'Invalid banner template file (file: %s).');
define ('ERROR_MESSAGE_BANNER_TEMPLATE_UNKNOWN',		'Invalid banner template type (id: %s, type: %s).');
define ('ERROR_MESSAGE_BANNER_XML_EMPTY',				'Empty banner xml.');
define ('ERROR_MESSAGE_FILE_MAPPING_EMPTY',				'Invalid mapping file (file: %s).');

// Log
define ('LOG_MESSAGE_OK_BANNERS_FILE_PROCESS',	'<----Successfully processed the banners file (banners: %d).');
define ('LOG_MESSAGE_OK_BANNER_CREATE',			'<----Successfully created a new banner (id: %d).');
define ('LOG_MESSAGE_OK_BANNER_UPDATE',			'<----Successfully updated the banner (id: %d, newsletterId: %d).');
define ('LOG_MESSAGE_OK_BANNER_LINKS_RETRACK',	'<----Successfully retracked the banner links (id: %d, newsletterId: %d).');
define ('LOG_MESSAGE_OK_MAPPING_FILE_UPDATE',	'<----Successfully updated the mapping file (file: %s).');
?>