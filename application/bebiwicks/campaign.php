<?php
require 'bootstrap.php';

$dateTime				= new \DateTime((empty($_GET['date']) ? 'now' : trim($_GET['date'])), new \DateTimeZone($config['timeZone']));
$uploadFolder			= $config['uploadFolder'] . '/' . $dateTime->format('Ymd');
$remoteBannersFilePath1	= $config['sftp']['folder'] . '/bebiwicks_defaut_' . $dateTime->format('Ymd') . '.xml';
$remoteBannersFilePath2	= $config['sftp']['folder'] . '/bebiwicks_enceinte_maman_' . $dateTime->format('Ymd') . '.xml';
$bannersFileName1		= 'bebiwicks_defaut_' . $dateTime->format('Ymd');
$bannersFileName2		= 'bebiwicks_enceinte_maman_' . $dateTime->format('Ymd');
$bannersFilePath1		= $uploadFolder . '/' . $bannersFileName1 . '.xml';
$bannersFilePath2		= $uploadFolder . '/' . $bannersFileName2 . '.xml';
$mappingFilePath		= $config['uploadFolder'] . '/mapping.csv';
$remoteMappingFilePath	= $config['sftp']['folder'] . '/mapping.csv';

$nmpParameters 			= array(
	'encrypt'		=> $config['nmp']['encrypt'],
	'id'			=> $config['nmp']['id'],
	'random'		=> $config['nmp']['random'],
	'recipients'	=> $config['nmp']['recepients'],
	'from' 			=> $config['applicationName'],
	'subject' 		=> $config['nmp']['subjects']['NOTIFY'],
);

$donwloadedBannersFiles	= array();
$processedBannersFiles	= array();
$banners1         		= array();
$banners2         		= array();
$bannersLog1         	= array();
$bannersLog2         	= array();

\SmartFocus\Util::log('[START]' . ACTION_SCRIPT);

// -----====================-----
// START FILE BANNERS DONWLOAD
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_FILE_BANNERS_DOWNLOAD);
// SFTP connect
try {
	if (!is_dir($uploadFolder) && !mkdir($uploadFolder)) {
	    throw new \Exception(sprintf(ERROR_MESSAGE_FOLDER_CREATE, $uploadFolder), EXCEPTION_CODE_ERROR);
	}
	$sftp = new \SmartFocus\Sftp($config['sftp']['server'], $config['sftp']['username'], $config['sftp']['key']);
	$sftp->connect();
}
catch (\Exception $exception) {
    \SmartFocus\Util::triggerError(
		$exception->getMessage(),
		\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
		ACTION_SCRIPT,
  		TRUE
	);
}
// Download defaut banners files
try {
	$sftp->downloadFile($remoteBannersFilePath1, $bannersFilePath1);
    $donwloadedBannersFiles['defaut'] = $bannersFilePath1;
}
catch (\Exception $exception) {
	\SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exception->getMessage());
}
// Download enceinte_maman banners files
try {
	$sftp->downloadFile($remoteBannersFilePath2, $bannersFilePath2);
    $donwloadedBannersFiles['enceinte_maman'] = $bannersFilePath2;
}
catch (\Exception $exception) {
	\SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exception->getMessage());
}
// Download mapping files
try {
	$sftp->downloadFile($remoteMappingFilePath, $mappingFilePath);
}
catch (\Exception $exception) {
	\SmartFocus\Util::log('[' . LOG_CODE_NOTICE . ']' . $exception->getMessage());
}
// SFTP disconnect
try { $sftp->close(); } catch (\Exception $exception) {}
if (empty($donwloadedBannersFiles)) {
	\SmartFocus\Util::triggerError(
		ERROR_MESSAGE_FILE_BANNERS_DOWNLOAD_EMPTY,
		LOG_CODE_ABORD,
		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, EXCEPTION_CODE_ABORD),
		ACTION_SCRIPT,
		TRUE
	);
}
// -----====================-----
// END FILE BANNERS DONWLOAD
// -----====================-----

// -----====================-----
// START FILE BANNERS PROCESS
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_FILE_BANNERS_PROCESS);
// Process defaut banners files
if (!empty($donwloadedBannersFiles['defaut']))
{
	try {
		if (!is_file($bannersFilePath1)) {
	    	throw new \Exception(sprintf(ERROR_MESSAGE_FILE_BANNERS_DOWNLOAD, $bannersFilePath1), EXCEPTION_CODE_WARNING);
		}
	    $banners1 = \SmartFocus\Bebiwicks\Util::parseBannersFile($bannersLog1, $bannersFilePath1, $uploadFolder);
		if (empty($banners1)) {
	        throw new \Exception(sprintf(ERROR_MESSAGE_BANNER_PROCESSED_EMPTY, $bannersFilePath1), EXCEPTION_CODE_WARNING);
		}
	    $processedBannersFiles['defaut'] = $bannersFilePath1;
	    \SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_BANNERS_FILE_PROCESS, count($banners1)));
	}
	catch (\Exception $exception) {
		$exceptionCode 		= $exception->getCode();
		$exceptionMessage 	= $exception->getMessage();
		if ($exceptionCode == EXCEPTION_CODE_WARNING) {
			\SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exceptionMessage);
		}
		else {
		    \SmartFocus\Util::triggerError(
				$exceptionMessage,
				\SmartFocus\Util::exceptionCodeToLogCode($exceptionCode),
				\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exceptionCode),
				ACTION_SCRIPT,
		  		TRUE
			);
		}
	}
}
// Process enceinte_maman banners files
if (!empty($donwloadedBannersFiles['enceinte_maman'])) {
	try {
		if (!is_file($bannersFilePath2)) {
	    	throw new \Exception(sprintf(ERROR_MESSAGE_FILE_BANNERS_DOWNLOAD, $bannersFilePath2), EXCEPTION_CODE_WARNING);
		}
	    $banners2 = \SmartFocus\Bebiwicks\Util::parseBannersFile($bannersLog2, $bannersFilePath2, $uploadFolder);
		if (empty($banners2)) {
	        throw new \Exception(sprintf(ERROR_MESSAGE_BANNER_PROCESSED_EMPTY, $bannersFilePath2), EXCEPTION_CODE_WARNING);
		}
	    $processedBannersFiles['enceinte_maman'] = $bannersFilePath2;
	    \SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_BANNERS_FILE_PROCESS, count($banners2)));
	}
	catch (\Exception $exception) {
		$exceptionCode 		= $exception->getCode();
		$exceptionMessage 	= $exception->getMessage();
		if ($exceptionCode == EXCEPTION_CODE_WARNING) {
			\SmartFocus\Util::log('[' . LOG_CODE_WARNING . ']' . $exceptionMessage);
		}
		else {
		    \SmartFocus\Util::triggerError(
				$exceptionMessage,
				\SmartFocus\Util::exceptionCodeToLogCode($exceptionCode),
				\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exceptionCode),
				ACTION_SCRIPT,
		  		TRUE
			);
		}
	}
}
if (empty($processedBannersFiles)) {
	\SmartFocus\Util::triggerError(
		ERROR_MESSAGE_FILE_BANNERS_PROCESS_EMPTY,
		LOG_CODE_ERROR,
		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, EXCEPTION_CODE_ABORD),
		ACTION_SCRIPT,
		TRUE
	);
}
// -----====================-----
// END FILE BANNERS PROCESS
// -----====================-----

// -----====================-----
// START FILE MAPPING UPDATE
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_FILE_MAPPING_UPDATE);
$banners = array();
$mapping = array();
foreach ($banners1 as $key => $value) {
    $banners[$key] = $value;
}
foreach ($banners2 as $key => $value) {
    $banners[$key] = $value;
}
if (is_readable($mappingFilePath)) {
	foreach (file($mappingFilePath) as $line) {
		$line = explode(',', trim($line));
		if (count($line) == 2) {
            $mapping[$line[0]] = $line[1];
		}
	}
	$banners = array_replace($mapping, $banners);
}
else {
    \SmartFocus\Util::log('[NOTICE]' . sprintf(ERROR_MESSAGE_FILE_MAPPING_EMPTY, $mappingFilePath));
}
$index = 0;
foreach ($banners as $key => $value) {
	$fileWriteMode 	= $index ? FILE_APPEND : NULL;
	$linePrefix 	= $index ? PHP_EOL : NULL;
	file_put_contents($mappingFilePath, $linePrefix . $key . ',' . $value, $fileWriteMode);
    $index++;
}
\SmartFocus\Util::log(sprintf(LOG_MESSAGE_OK_MAPPING_FILE_UPDATE, $mappingFilePath));
// -----====================-----
// END FILE MAPPING UPDATE
// -----====================-----

// -----====================-----
// START FILE MAPPING UPLOAD
// -----====================-----
\SmartFocus\Util::log('---->' . ACTION_FILE_MAPPING_UPLOAD);
// SFTP connect
try {
	if (!is_readable($mappingFilePath)) {
	    throw new \Exception(sprintf(ERROR_MESSAGE_FILE_MAPPING_EMPTY, $mappingFilePath), EXCEPTION_CODE_ERROR);
	}
	$sftp = new \SmartFocus\Sftp($config['sftp']['server'], $config['sftp']['username'], $config['sftp']['key']);
	$sftp->connect();
	$sftp->uploadFile(file_get_contents($mappingFilePath), $remoteMappingFilePath);
	$sftp->close();
}
catch (\Exception $exception) {
    \SmartFocus\Util::triggerError(
		$exception->getMessage(),
		\SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
		\SmartFocus\Util::exceptionCodeToNmpSubject($nmpParameters, $exception->getCode()),
		ACTION_SCRIPT,
  		TRUE
	);
}
// -----====================-----
// END FILE MAPPING UPLOAD
// -----====================-----

$report = \SmartFocus\Util::$log->getHistory();
$report[] = '[END]' . ACTION_SCRIPT;

\SmartFocus\Util::notify($nmpParameters, implode(PHP_EOL, $report));

\SmartFocus\Util::log('[END]' . ACTION_SCRIPT);

exit(LOG_CODE_SUCCESS);
?>