<?php
ini_set('max_execution_time', 0);

require '/opt/automatic/core/bootstrap.php';

spl_autoload_register();

require __DIR__ . '/config/constants.php';
require __DIR__ . '/config/config.php';

new \SmartFocus\Bebiwicks\Util($config);
