<?php namespace SmartFocus\Bebiwicks;

/**
 * Utility class
 */
class Util extends \SmartFocus\Util {

	/**
	 * Deletes a list of banners either from origin or destination account
	 *
	 * @param  array	$banners		Array of banners : each banner must have a type (EMAIL | TRANSACTIONAL) and an array of fields (each field must be name => value)
	 * @param  string	$removeFrom		Account to delete from ('origin' | 'destination')
	 * @return
	 */
	 public static function deleteBannersByField($field, $value)
	 {

        self::log('---->Delete banners by field (field: ' . $field .', value: ' . $value . ').');
		try {

			$partnerAuth 				= new \SmartFocus\API\PartnerPrivilegedAuthenticationService(self::$config['api']['partner_users']['A'], self::$config['api']['partner_key1'], self::$config['api']['partner_key2']);
			$campaignManagementService  = new \SmartFocus\API\CampaignManagementService($partnerAuth);

			$sfBannersId  = $campaignManagementService->getBannersByField($field, $value, 1000);

			if (empty($sfBannersId)) {
				throw new \Exception('Could not find any banners.', EXCEPTION_CODE_SUCCESS);
			}
			if (!is_array($sfBannersId)) {
		        $sfBannersId = array(intval($sfBannersId));
			}

		}
		catch (\Exception $exception) {

            self::triggerError(
				\SmartFocus\Util::exceptionToLogMessage($exception),
                \SmartFocus\Util::exceptionCodeToLogCode($exception->getCode()),
				NULL,
				NULL,
				TRUE
			);

		}

		$banners = array(
			'found' 	=> count($sfBannersId),
			'deleted'	=> 0,
		);

		$apiCalls 	= 0;
		$limit 		= 50;
		foreach ($sfBannersId as $sfBannerId) {

			$apiCalls++;

			try {

				if ($apiCalls >= $limit) {

					if (!empty($campaignManagementService)) {
						$campaignManagementService->disconnect();
						unset($campaignManagementService, $partnerAuth);
					}

					$partnerAuth 				= new \SmartFocus\API\PartnerPrivilegedAuthenticationService(self::$config['api']['partner_users']['A'], self::$config['api']['partner_key1'], self::$config['api']['partner_key2']);
					$campaignManagementService  = new \SmartFocus\API\CampaignManagementService($partnerAuth);

                    $apiCalls = 1;

				}

                self::log('---->Deleting banner (id: ' . $sfBannerId .').');
				$campaignManagementService->deleteBanner($sfBannerId);

                $banners['deleted']++;

			}
			catch (\Exception $exception) {
                $apiCalls = $limit;

	            self::triggerError(
					\SmartFocus\Util::exceptionToLogMessage($exception),
	                \SmartFocus\Util::exceptionCodeToLogCode($exception->getCode())
				);

			}

		}

		if (empty($banners['deleted'])) {
        	self::log('<----Failed to delete banners.');
		}
		else {
        	self::log('<----Successfully deleted banners(found: ' . $banners['found'] .', deleted: ' . $banners['deleted'] . ').');
		}

		return $banners;

	}

	/**
	 * Parse banners file
	 *
	 * @param array &$bannersLog
	 * @param string $filePath
	 * @param string $uploadFolder
	 * @return bool
	 */
	public static function parseBannersFile(&$bannersLog, $filePath, $uploadFolder)
	{
        $campaignManagementService = NULL;

		$xml = new \XMLReader();

		try {
			$xml->open($filePath, 'UTF-8');
		}
		catch (\Exception $exception) {
			throw $exception;
		}

		$banners 			= array();
		$bannersLog 		= array(
			'found' 	=> 0,
			'skipped' 	=> 0,
			'ok' 		=> 0,
			'created'	=> 0,
			'updated'	=> 0,
			'failed'	=> 0,
			'log' 		=> array(),
		);
        $searchDate 		= array();
        $replaceDate		= array();
        $placeHolderPattern =
			'/(' . implode('|', array_map('preg_quote', self::$config['placeHolderTags']['start'])) . ')'
			. '%s'
			. '(' . implode('|', array_map('preg_quote', self::$config['placeHolderTags']['end'])) . ')/';

		$searchDate[]	= '/' . preg_quote('|||JOUR_LETTRES|||') . '/';
		$searchDate[]	= '/' . preg_quote('|||JOUR_CHIFFRES|||') . '/';
		$searchDate[]	= '/' . preg_quote('|||MOIS_LETTRES|||') . '/';

		$replaceDate[]	= '[EMV SYSDATE]EEEE,fr[EMV /SYSDATE]';
		$replaceDate[]	= '[EMV SYSDATE]dd[EMV /SYSDATE]';
		$replaceDate[]	= '[EMV SYSDATE]MMMM,fr[EMV /SYSDATE]';

        while ($xml->read()) {

			if ($xml->name === TAG_BANNER_WRAPPER && $xml->nodeType === \XMLReader::ELEMENT) {

                $bannersLog['found']++;

		    	// -----====================-----
				// START PROCESS BANNER
		     	// -----====================-----
                $continueProcess = TRUE;
				try {

                    // Process banner xml
					$lsfBanner = new \SimpleXMLElement(trim($xml->readOuterXml()));
					if (empty($lsfBanner)) {
                        throw new \Exception(ERROR_MESSAGE_BANNER_XML_EMPTY, EXCEPTION_CODE_WARNING);
					}
	                $lsfBanner = (array) $lsfBanner->children();
					if (empty($lsfBanner)) {
                        throw new \Exception(ERROR_MESSAGE_BANNER_XML_EMPTY, EXCEPTION_CODE_WARNING);
					}
	                $lsfBanner = array_map('strval', $lsfBanner);
	                $lsfBanner = array_map('trim', $lsfBanner);

					// Check banner id
					if (!array_key_exists(TAG_BANNER_ID, $lsfBanner)) {
                    	throw new \Exception(ERROR_MESSAGE_BANNER_ID_EMPTY, EXCEPTION_CODE_WARNING);
					}

					// Check banner name
					if (!array_key_exists(TAG_BANNER_NAME, $lsfBanner)) {
                    	throw new \Exception(sprintf(ERROR_MESSAGE_BANNER_NAME_EMPTY, $lsfBanner[TAG_BANNER_ID]), EXCEPTION_CODE_WARNING);
					}
                    // Check banner type
					if (!array_key_exists(TAG_BANNER_TYPE_1, $lsfBanner) && !array_key_exists(TAG_BANNER_TYPE_2, $lsfBanner)) {
                    	throw new \Exception(sprintf(ERROR_MESSAGE_BANNER_TEMPLATE_EMPTY, $lsfBanner[TAG_BANNER_ID]), EXCEPTION_CODE_WARNING);
					}
                    $tagBannerType 	= (array_key_exists(TAG_BANNER_TYPE_1, $lsfBanner) ? TAG_BANNER_TYPE_1 : TAG_BANNER_TYPE_2);
	                $templateType 	= mb_strtolower($lsfBanner[$tagBannerType]);

					if (!array_key_exists($templateType, self::$config['templateList'])) {
						throw new \Exception(sprintf(ERROR_MESSAGE_BANNER_TEMPLATE_UNKNOWN, $lsfBanner[TAG_BANNER_ID], $templateType), EXCEPTION_CODE_WARNING);
					}
					if (!is_readable(self::$config['templateList'][$templateType])) {
                    	throw new \Exception(sprintf(ERROR_MESSAGE_BANNER_TEMPLATE_FILE, self::$config['templateList'][$templateType]), EXCEPTION_CODE_WARNING);
					}

					$sfBannerName 			= ucfirst(mb_strtolower($lsfBanner[TAG_BANNER_NAME])) . ' ' . ucfirst(mb_strtolower($lsfBanner[$tagBannerType]));
					$sfBannerDescription 	= $sfBannerName . ' ' . $lsfBanner[TAG_BANNER_ID] . ' (' . pathinfo($filePath, PATHINFO_FILENAME) .  ')';
					$sfBannerContent 		= file_get_contents(self::$config['templateList'][$templateType]);

					$search		= array();
					$replace	= array();
					foreach($lsfBanner as $placeholder => $value) {
						$search[]	= sprintf($placeHolderPattern, $placeholder);
						$replace[]	= $value;
					}
					$search 	= array_merge($search, $searchDate);
	                $replace	= array_merge($replace, $replaceDate);

	                $sfBannerContent = preg_replace($search, $replace, $sfBannerContent);
	                //$sfBannerContent = html_entity_decode($sfBannerContent);
	                //$sfBannerContent = utf8_encode($sfBannerContent);

                    $bannersLog['ok']++;

				}
				catch (\Exception $exception) {
                    $continueProcess = FALSE;
					$bannersLog['skipped']++;
					$logMessage 			= $exception->getMessage();
                    $bannersLog['log'][] 	= $logMessage;
                    self::triggerError($logMessage, $exception->getCode());
				}
		    	// -----====================-----
				// END PROCESS BANNER
		     	// -----====================-----

				if (!$continueProcess) {
					continue;
				}

		    	// -----====================-----
				// START MERGE BANNER
		     	// -----====================-----
				$continueProcess 	= TRUE;
				$createBanner 		= TRUE;
				try {

					$partnerAuth 				= new \SmartFocus\API\PartnerPrivilegedAuthenticationService(self::$config['api']['partner_users']['A'], self::$config['api']['partner_key1'], self::$config['api']['partner_key2']);
					$campaignManagementService  = new \SmartFocus\API\CampaignManagementService($partnerAuth);

                    $sfBannerId 	= (int) $campaignManagementService->getBannersByField('name', $sfBannerName, 1);
                    $createBanner 	= empty($sfBannerId);

					if ($createBanner) {
                        self::log('---->' . sprintf(ACTION_BANNER_CREATE, $sfBannerName, $sfBannerDescription));
						$sfBannerId = (int) $campaignManagementService->createBannerByObj($sfBannerName, 'HTML', $sfBannerContent, $sfBannerDescription);
						if (empty($sfBannerId)) {
							throw new \Exception(sprintf(ERROR_MESSAGE_BANNER_CREATE_UNKNOWN, $sfBannerName, $sfBannerDescription), EXCEPTION_CODE_WARNING);
						}
                        $banners[$lsfBanner[TAG_BANNER_ID]] = $sfBannerId;
					}
					else {
                        $banners[$lsfBanner[TAG_BANNER_ID]] = $sfBannerId;
						self::log('---->' . sprintf(ACTION_BANNER_UPDATE, $sfBannerId, $sfBannerName, $sfBannerDescription));
						$campaignManagementService->untrackAllBannerLinks($sfBannerId);
	                    $campaignManagementService->updateBannerByObj($sfBannerId, $sfBannerName, 'HTML', $sfBannerContent, $sfBannerDescription);
					}

				}
				catch (\Exception $exception) {
                    $bannersLog['failed']++;
					$logMessage				= \SmartFocus\Util::exceptionToLogMessage($exception);
					$logCode 				= \SmartFocus\Util::exceptionCodeToLogCode($exception->getCode());
                    $bannersLog['log'][] 	= $logMessage;
                    $continueProcess 		= FALSE;
                    self::triggerError($logMessage, $logCode);
				}

				if ($continueProcess) {

					try {
	                    if ($createBanner) {
							// $campaignManagementService->trackAllBannerLinks($sfBannerId); // THIS WAS COMMENTED AT THE CLIENT'S REQUEST (2014.08.07)
							$bannersLog['created']++;
                        	self::log(sprintf(LOG_MESSAGE_OK_BANNER_CREATE, $sfBannerId));
						}
						else {
	                        // $campaignManagementService->trackAllBannerLinks($sfBannerId); // THIS WAS COMMENTED AT THE CLIENT'S REQUEST (2014.08.07)
	                        $bannersLog['updated']++;
                        	self::log(sprintf(LOG_MESSAGE_OK_BANNER_UPDATE, $sfBannerId, $lsfBanner[TAG_BANNER_ID]));
						}

					}
					catch (\Exception $exception) {
						$logMessage				= \SmartFocus\Util::exceptionToLogMessage($exception);
                        $bannersLog['log'][]	= $logMessage;
                        self::triggerError($logMessage, LOG_CODE_NOTICE);
					}

				}
				// THIS WAS COMMENTED AT THE CLIENT'S REQUEST (2014.08.07)
				/*
				elseif (!$createBanner) { // Retrack all links if the update fails

					try {
	                    $campaignManagementService->trackAllBannerLinks($sfBannerId);
	                	self::log(sprintf(LOG_MESSAGE_OK_BANNER_LINKS_RETRACK, $sfBannerId, $lsfBanner[TAG_BANNER_ID]));
					}
					catch (\Exception $exception) {
						$logMessage				= \SmartFocus\Util::exceptionToLogMessage($exception);
                        $bannersLog['log'][]	= $logMessage;
                        self::triggerError($logMessage, LOG_CODE_NOTICE);
					}

				}
				*/

				$campaignManagementService->disconnect();
				unset($campaignManagementService, $partnerAuth);
		    	// -----====================-----
				// END MERGE BANNER
		     	// -----====================-----

			}

		}

		@$xml->close();

		return $banners;
	}

}

?>