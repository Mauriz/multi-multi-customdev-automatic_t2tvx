<?php
/*
*   @author: Rahel Chowdhury <rahel.chowdhury@smartfocus.com>
*   @copyright: 2018
*   @system: SmartFocus
*
*   Server Utility Functions
*
*/

error_reporting( E_ALL ^ E_NOTICE );

//Some Vital Configurations for the script
$configs = array(
	'nmp' 	=> array(
		'email_addresses' => array( 'rahel.chowdhury@smartfocus.com' ), //array('rahel.chowdhury@smartfocus.com','SChampaneri@smartfocus.com','bruno.oliveira@smartfocus.com','monitoring_valley@smartfocus.com')
		'from_address'	  => 'monitoring_valley@smartfocus.com',
		'subject'	  => 'Disk Utility Check: t2tvx1.emv2.com',
		'body'		  => '',
	),
	'options' => array(
		'chdir' => '/opt/automatic/application/',
		'base_disk_check_path' => '/',
		'trigger_disk_limit' => 4, //checks and triggers when disk space is less then 4 GB
	),
);

$objDiskUtility = new SF_Disk_Utility( $configs );
$objDiskUtility->run_disk_check();

/*
* Object => Performs Disk Utility checks
*   => Also able to check application paths,
*   => Also able to traverse logs in paths
*   =>  and any match on the pattern will trigger alert!
*
*/
class SF_Disk_Utility
{
    //utility variables
    private $chdir_dir;
    private $base_chk_disk_path;
    private $trigger_disk_limit;    
    private $disk_usage;
    private $app_usage;
    private $obj_notification;
    private $obj_var_mapping;
    private $obj_last_run_file = "/opt/automatic/SF_Disk_Utility-last-run";

    public function __construct( $configs = array() )
    {
        if( empty( $configs ) || !isset( $configs['nmp'], $configs['options'] ) )
        {
	    error_log( '[' . date('d/m/Y H:i:s') . '] Missing Configurations for both \'nmp\' and \'options\' -> ' . __FILE__ . 
		PHP_EOL, 3, '/var/log/httpd/error_log' );
            $this->__destruct();
        }
        $this->__initialise( $configs['options'] );
        $this->obj_notification = new SF_NMP_Notification( $configs['nmp'] );
        touch( $this->obj_last_run_file );
    }

    public function __destruct()
    {
	/* -- no need to log here..
        error_log( '[' . date('d/m/Y H:i:s') . '] TERMINATED : ' . __CLASS__ . '()' . __FILE__ .
		PHP_EOL, 3, '/var/log/httpd/error_log' );
	*/
    }

    public function __get( $var )
    {
        if( isset( $this->obj_var_mapping[$var] ) )
        {
            return $this->obj_var_mapping[$var];
        }

        return false;
    }

    private function __initialise( $configs )
    {
            $chdir_dir = isset( $configs['chdir'] ) ? $configs['chdir'] : $this->base_chdir;
            chdir( $chdir_dir );
	    $this->base_chk_disk_path = $configs['base_disk_check_path'];
	    $this->trigger_disk_limit = $configs['trigger_disk_limit'];
            $this->disk_usage = null;
            $this->app_usage = null;
    }

    public function check_disk_space()
    {
        $output = '';
        $command = 'df -H';
        exec( $command, $output );
        $this->disk_usage  = $output;
    }

    public function check_app_disk_space()
    {
        $output = '';
        $command = 'du -sh * | sort';
        exec( $command, $output );
        $this->app_usage = $output;
    }

    public function run_disk_check()
    {
	$this->check_disk_space();
	$this->check_app_disk_space();

	$disk_space_used = disk_total_space( $this->base_chk_disk_path );
	$disk_remaining_space = disk_free_space( $this->base_chk_disk_path );
	
	$space_prefix = array( 'B', 'KB', 'MB', 'GB', 'TB', 'EB', 'ZB', 'YB' );
	$base_byte = 1024;
	
	$class = min((int)log($disk_remaining_space, $base_byte), count($space_prefix) - 1 );
	$disk_running_low = sprintf( '%1.2f', $disk_remaining_space / pow( $base_byte, $class ));
	//$disk_running_low_human = sprintf( '%1.2f', $disk_remaining_space / pow($base_byte, $class)) . ' ' . $space_prefix[$class];

	if( $disk_running_low < $this->trigger_disk_limit )
	{
		//echo "YES RUNNING LOW" . PHP_EOL;
		$content = 'Hello Tech Guru,' . PHP_EOL . PHP_EOL . 'Please check the disk space on the server t2tvx1.emv2.com below are some stats ' . PHP_EOL . PHP_EOL;
		$content .= '========== Disk Usage Stats ===========' . PHP_EOL . PHP_EOL;
		$content .= implode(PHP_EOL, $this->disk_usage) . PHP_EOL . PHP_EOL;
		$content .= '========== App Usage Stats  ===========' . PHP_EOL . PHP_EOL;
		$content .= implode(PHP_EOL, $this->app_usage) . PHP_EOL . PHP_EOL;
		$this->obj_notification->setBody( $content );
		$this->obj_notification->send_NMP_Notifications();	
		echo "\r\n[OK] - Done and Notified Techies\r\n";
	}else
	{
		echo "\r\n[OK] - No Need to Notify\r\n";
	}
    }
}

/*
* Object => Sends Notifications
*       Using the NMP Platform.
*	Options - Required!!!!
*
*	 -- email_addresses
*	 -- from_address
*	 -- subject
*	 -- body
*
*/
class SF_NMP_Notification
{
    private $strAddresses;
    private $strSubject;
    private $strReplyTo;
    private $strFromAddress;
    private $strBody;
    private $objCurl;
    private $strNotification;

    CONST NMP_REST_URL = 'http://api.notificationmessaging.com/NMSXML';
    
    public function __construct( $configs )
    {
        $this->__initialise( $configs );
        $this->objCurl = new SF_SimpleCurlRequest();
        $this->objCurl->setURL( self::NMP_REST_URL );
    }

    private function __initialise( $options )
    {
        //configure all the options in the object
        $this->strAddresses = isset( $options['email_addresses'] ) ? $options['email_addresses'] : 'monitoring_valley@smartfocus.com';
        $this->strFromAddress = isset( $options['from_address'] ) ? $options['from_address'] : 'noreply-ops@smartfocus.com';
        $this->strSubject = isset( $options['subject'] ) ? $options['subject'] : 'Failure Email - Check';
        $this->strBody = isset( $options['body'] ) ? $options['body'] : 'Email has no content provided.';
        $this->strNotification = null;

    }

    public function setAddress( $address )
    {
	if( is_array( $this->strAddresses) )
	{
		$this->strAddresses[] = $address;
		return;
	}
	$this->strAddresses = $address;
    }

    public function setFromAddress( $from_address )
    {
	$this->strFromAddress = $from_address;
    }

    public function setSubject( $subject )
    {
	$this->strSubject = $subject;
    }

    public function setBody( $body )
    {
	$this->strBody = $body;
    }

    public function send_NMP_Notifications()
    {
        //begin the sending of the notification(s)

        $this->strNotification = '<?xml version="1.0" encoding="UTF-8"?>';
        $this->strNotification .= '<MultiSendRequest>';

                if( is_array( $this->strAddresses ) )
                {
                        foreach( $this->strAddresses as $indx => $email_address )
                        {
                                //building the notification for each of the email addresses to be sent to
                                $this->strNotification .= $this->build_NMP_SendXML( $email_address, $this->strFromAddress, $this->strSubject, $this->strBody );
                        }
                }else
                {
                        $this->strNotification .= $this->build_NMP_SendXML( $this->strAddresses, $this->strFromAddress, $this->strSubject, $this->strBody );
                }
        $this->strNotification .= '</MultiSendRequest>';    

        $this->objCurl->setPostData( $this->strNotification );

        //do the request of sending ....
        $this->objCurl->doCurlRequest();
    }

    private function build_NMP_SendXML( $email_address, $from_address, $subject, $body )
    {
        //$arguments = func_get_args();
        //$no_of_args = func_num_args();
	$strXML = '';
        $strXML .= '<sendrequest>';
        $strXML .= '<dyn>';
                $strXML .= '<entry>';
                $strXML .= '<key>from</key><value>' . $from_address . '</value>';
                $strXML .= '</entry>';
                $strXML .= '<entry>';
                $strXML .= '<key>subject</key><value>' . $subject . '</value>';
                $strXML .= '</entry>';
        $strXML .= '</dyn>';
        $strXML .= '<content>';
                $strXML .= '<entry><key>1</key><value><![CDATA[' . htmlentities( $body ) . ']]></value></entry>';
        $strXML .= '</content>';
        $strXML .= '<email>' . $email_address . '</email>';
        $strXML .= '<encrypt>EdX7CqkmmqPm8SA9MOPv1E3WWT4OFKm3izrcc6k-WMGoK7U</encrypt>';
        $strXML .= '<random>1ABD84DA2180801A</random>';
        $strXML .= '<senddate>' . date('Y-m-d\TH:i:s') . '</senddate>';
        $strXML .= '<synchrotype>NOTHING</synchrotype>';
        $strXML .= '<uidkey>EMAIL</uidkey>';                        
        $strXML .= '</sendrequest>';

        return $strXML;
    }

}

/*
* Simple Curl Object
*	Processes a Curl Request
*	Mandatory Requirements Include
*	SetURL() and setPostData()
*/
class SF_SimpleCurlRequest
{
    private $rsCurl;
    private $strURL;
    private $strPostData;
    private $strResponse;
    private $aResponseData;
    private $strRequestData;
    
    public function __construct()
    {
        $this->__initialise();
    }
    
    public function __initialise()
    {
        $this->rsCurl = null;
        $this->strURL = '';
        $this->strPostData = null;
        $this->strResponse = null;
        $this->aResponseData = array();
        $this->strRequestData = null;
    }

    public function setURL( $url )
    {
        $this->strURL = $url;
    }

    public function setPostData( $data )
    {
        $this->strPostData = $data;
    }

    public function getResponseData( $retrunArr = true )
    {
        return ( (bool) $retrunArr ) ?  $this->aResponseData : $strResponse;
    }

    public function getRequestData()
    {
        return $this->strRequestData;
    }

    public function doCurlRequest()
    {
        if( empty( $this->strURL ) )
            return false;

        $this->rsCurl = curl_init();

        curl_setopt($this->rsCurl, CURLOPT_URL, $this->strURL);
        curl_setopt($this->rsCurl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->rsCurl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->rsCurl, CURLOPT_POST, 1);
        curl_setopt($this->rsCurl, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($this->rsCurl, CURLINFO_HEADER_OUT, 1);
        curl_setopt($this->rsCurl, CURLOPT_POSTFIELDS, "{$this->strPostData}");
        curl_setopt($this->rsCurl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->rsCurl, CURLOPT_FOLLOWLOCATION, true);

        $this->strResponse = curl_exec( $this->rsCurl );

        $this->aResponseData['error'] = ( curl_error( $this->rsCurl ) ) ? curl_error( $this->rsCurl ) : false;
        $this->aResponseData['data'] = $this->strResponse;
        $this->aResponseData['http_code'] = curl_getinfo( $this->rsCurl, CURLINFO_HTTP_CODE );
        $this->aResponseData['http_request'] = curl_getinfo( $this->rsCurl, CURLINFO_HEADER_OUT );
        $this->aResponseData['request_url'] = $this->strURL;
        $this->strRequestData = $this->aResponseData['http_request'];

        curl_close( $this->rsCurl );

        return 1;
    }

}

?>
