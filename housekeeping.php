<?php
/*
*   @author: Rahel Chowdhury
*   @copyright: 2018
*
*   SmartFocus Application House Cleaning Utility
*	It works well with Glob so any pattern accepted!
*/

error_reporting( E_ALL ^ E_NOTICE );

$DRY_RUN = true; //Set this to true to just write to log but not remove!!

$housekeeping_log = '/opt/automatic/housekeeping.log';

$applications_conf = array(
    // Specify your housekeeping rules here
    '/opt/automatic/application/spir/upload/' . date("Ym", strtotime("-2 months")) . "*",
    '/opt/automatic/application/alphab/upload/' . date("Ym", strtotime("-2 months")) . "*",

    //'/opt/automatic/application/spir/upload/' . date("Ym", strtotime("-2 months")) . "*",
    //'/opt/automatic/application/spir/upload/' . date("Ym", strtotime("-1 months")) . "*/*/*.csv",
);


$mail_opts = array(
	// Specify your mailing options here
	'email_addresses' => array( 'monitoring_valley@smartfocus.com' ),
	'from_address'	  => 'monitoring_valley@smartfocus.com',
	'subject'	  => 'Log Housekeeping :: t2tvx1.emv2.com',
	'body'		  => ''
);

// Specify the skipping list of ids => index of $applications_conf
$skippers_list = array();

$SF_App_Logger = new SF_App_Logger( $housekeeping_log );
$SF_App_Notify = new SF_App_Notify( $mail_opts );
$SF_App_HouseKeeping = new SF_App_HouseKeeping( $applications_conf, $SF_App_Logger, $SF_App_Notify );
$SF_App_HouseKeeping->set_run_mode( $DRY_RUN ); //setting the run mode

$SF_App_HouseKeeping->do_cleanup( $skippers_list );

exit(0);



/*
* House Clean Object
*   => Dependency Injected SF_APP_Logger
*/
class SF_App_HouseKeeping
{
    protected $housekeeping_conf = null;
    protected $logger = null;
    protected $mailer = null;
    protected $dry_run = false;

    public function __construct( array $config, SF_App_Logger $logger, SF_App_Notify $mailer )
    {
            $this->__initialise( $config, $logger, $mailer );
    }

    private function __initialise( $config, $logger, $mailer )
    {
        $this->housekeeping_conf = $config;
        $this->logger = $logger;
	$this->mailer = $mailer;
    }

    public function set_run_mode( bool $run_mode )
    {
	$this->dry_run = $run_mode;
    }

    public function do_cleanup( $avoid_idx = array() )
    {
        if( isset( $this->housekeeping_conf ) && !empty( $this->housekeeping_conf ) )
        {
	    $log = '';
	    $log .= $this->logger->write_log( 'START: clean process started' );
	    $i_total_removed = 0;

            foreach( $this->housekeeping_conf as $key => $log_clean_rule )
            {
		if( in_array( $key, $avoid_idx ) )
		{
			$log .= $this->logger->write_log( 'Skipping pattern: ' . $log_clean_rule );
			continue; //skipping if you tell me to ..;)
		}

		$pattern = "$log_clean_rule";

		$log .= $this->logger->write_log( 'Searching Pattern:' . $pattern );

                $logs_to_remove = glob( $pattern, GLOB_MARK );

        	if( !empty( $logs_to_remove ) && is_array( $logs_to_remove ) )	
		{
			$lenzer = count( $logs_to_remove );
			$log .= $this->logger->write_log( 'Found pattern data to remove. Total: ' . $lenzer );

			for( $j=0; $j < $lenzer; $j++ )
			{
				$log .= $this->logger->write_log( 'Removing: ' . $logs_to_remove[$j] );
				$command = 'rm -rf ' . $logs_to_remove[$j];
				$output = '';
				
				//run via the shell to remove = faster
				if( $this->dry_run == false )
				{
					exec( $command, $output );
				}
				$i_total_removed ++;
			}
			
		}else
		{
			$log .= $this->logger->write_log( 'Nothing found for pattern.' );
		}


            }
	    $log .= $this->logger->write_log( 'Total Removed: ' . $i_total_removed );
	    $log .= $this->logger->write_log( 'END: clean process finished' );

	    //Send notification if we actually removed logs!!
	    if( $i_total_removed > 0 )
	    {
		$addresses = (array) $this->mailer->get_to_addresses();
		$this->logger->write_log( 'Sending Notification to -> ' . implode( ', ', $addresses ));
		$this->mailer->set_body( $log );
		$this->mailer->sendNotification();
	    }

	    echo PHP_EOL . '[OK] - Done' . PHP_EOL;
        }
    }
}

/*
* Logger Object
* Writes log data to a log file
*/
class SF_App_Logger
{
    protected $logfile;

    public function __construct( $logfile )
    {
        $this->__initialise( $logfile );
    }

    private function __initialise( $logfile )
    {
        $this->logfile = $logfile;
    }

    public function write_log( $log )
    {
        if( !is_file( $this->logfile ) )
        {
            touch( $this->logfile );
        }

        if( file_exists( $this->logfile ) && is_writeable( $this->logfile ) )
        {
            $fh = fopen( $this->logfile, 'a' );
            if( $fh )
            {
                $log = '[' . date('Y-m-d H:i:s') . '] ' . $log . PHP_EOL;
                flock( $fh, LOCK_EX );
                fwrite( $fh, $log);
                flock( $fh, LOCK_UN );
                fclose( $fh );
		return $log;
            }
        }
    }
}

/*
* Object that notifies using the 
*	NMP Platform
*/
class SF_App_Notify
{
    protected $to_addresses;
    protected $subject;
    protected $replyto;
    protected $from_address;
    protected $body;
    protected $rsCurl;
    public $strResponse;
    public $aResponseData;
    public $strRequestData;

    CONST NMP_REST_URL = 'http://api.notificationmessaging.com/NMSXML';

    public function __construct( $options )
    {
	$this->__initialise( $options );
    }

    private function __initialise( $configs )
    {
	$this->to_addresses = isset( $configs['email_addresses'] ) ? $configs['email_addresses'] : 'monitoring_valley@smartfocus.com';
	$this->subject = isset( $configs['subject'] ) ? $configs['subject'] : ' ::Notification:: ';
	$this->replyto = isset( $configs['replyto'] ) ? $configs['replyto'] : 'noreply-ops@smartfocus.com';
	$this->from_address = isset( $configs['from_address'] ) ? $configs['from_address'] : 'noreply-ops@smartfocus.com';
	$this->body = isset( $configs['body'] ) ? $configs['body'] : '';
	$this->rsCurl = null;
    }
	
    public function set_subject( $subject )
    {
	$this->subject = $subject;
    }

    public function set_body( $body )
    {
	$this->body = $body;
    }

    public function set_to_addresses( $address )
    {
	if( !empty( $this->to_addresses ) )
	{
		if( is_array( $this->to_addresses ) )
		{
			$this->to_addresses[] = $address;
			return;
		}
		$this->to_addresses[] = $this->to_addresses;
		$this->to_addresses[] = $address;
	}
	$this->to_addresses = $address;
    }

    public function get_to_addresses()
    {
	return $this->to_addresses;
    }

    public function get_from_address()
    {
	return $this->from_address;
    }

    public function get_response()
    {
	return $this->aResponseData;
    }

    public function get_request()
    {
	return $this->strRequestData;
    }

    public function sendNotification()
    {
	if( empty( $this->to_addresses ) )
	{
		return false;
	}

	$notification = '<?xml version="1.0" encoding="UTF-8"?>';
	$notification .= '<MultiSendRequest>';
	
	if( is_array( $this->to_addresses ) )
	{
		foreach( $this->to_addresses as $idx => $address )
		{
			//start to build the notification for each of the addresses
			$notification .= $this->build_NotificationXML( $address, $this->from_address, $this->subject, $this->body );
		}
	}else
	{
		$notification .= $this->build_NotificationXML( $this->to_addresses, $this->from_address, $this->subject, $this->body );
	}
	$notification .= '</MultiSendRequest>';

	$this->rsCurl = curl_init();
	
         curl_setopt($this->rsCurl, CURLOPT_URL, self::NMP_REST_URL);
         curl_setopt($this->rsCurl, CURLOPT_SSL_VERIFYHOST, 0);
         curl_setopt($this->rsCurl, CURLOPT_SSL_VERIFYPEER, 0);
         curl_setopt($this->rsCurl, CURLOPT_POST, 1);
         curl_setopt($this->rsCurl, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
         curl_setopt($this->rsCurl, CURLINFO_HEADER_OUT, 1);
         curl_setopt($this->rsCurl, CURLOPT_POSTFIELDS, "{$notification}");
         curl_setopt($this->rsCurl, CURLOPT_RETURNTRANSFER, 1);
         curl_setopt($this->rsCurl, CURLOPT_FOLLOWLOCATION, true);

         $this->strResponse = curl_exec( $this->rsCurl );

         $this->aResponseData['error'] = ( curl_error( $this->rsCurl ) ) ? curl_error( $this->rsCurl ) : false;
         $this->aResponseData['data'] = $this->strResponse;
         $this->aResponseData['http_code'] = curl_getinfo( $this->rsCurl, CURLINFO_HTTP_CODE );
         $this->aResponseData['http_request'] = curl_getinfo( $this->rsCurl, CURLINFO_HEADER_OUT );
         $this->aResponseData['request_url'] = self::NMP_REST_URL;
         $this->strRequestData = $this->aResponseData['http_request'];

         curl_close( $this->rsCurl );

	return 1;

    }

    private function build_NotificationXML( $email_address, $from_address, $subject, $body )
    {

         $strXML = '';
         $strXML .= '<sendrequest>';
         $strXML .= '<dyn>';
                 $strXML .= '<entry>';
                 $strXML .= '<key>from</key><value>' . $from_address . '</value>';
                 $strXML .= '</entry>';
                 $strXML .= '<entry>';
                 $strXML .= '<key>subject</key><value>' . $subject . '</value>';
                 $strXML .= '</entry>';
         $strXML .= '</dyn>';
         $strXML .= '<content>';
                 $strXML .= '<entry><key>1</key><value><![CDATA[' . htmlentities( $body ) . ']]></value></entry>';
         $strXML .= '</content>';
         $strXML .= '<email>' . $email_address . '</email>';
         $strXML .= '<encrypt>EdX7CqkmmqPm8SA9MOPv1E3WWT4OFKm3izrcc6k-WMGoK7U</encrypt>';
         $strXML .= '<random>1ABD84DA2180801A</random>';
         $strXML .= '<senddate>' . date('Y-m-d\TH:i:s') . '</senddate>';
         $strXML .= '<synchrotype>NOTHING</synchrotype>';
         $strXML .= '<uidkey>EMAIL</uidkey>';
         $strXML .= '</sendrequest>';

         return $strXML;

    } 

}

?>
